/*
 * RELEVANT LINKS
 * ==============
 *
 * https://reactnavigation.org/docs/en/stack-navigator.html#docsNav
 *
 */
import React from 'react'
import { Platform } from 'react-native'
import { createStackNavigator, createMaterialTopTabNavigator, createBottomTabNavigator } from 'react-navigation'

import HomeScreen from '../screens/HomeScreen'
import InitialAuthScreen from '../screens/InitialAuthScreen'
import EmailCreateUserScreen from '../screens/EmailCreateUserScreen'
import HeaderTitle from '../components/HeaderTitle'
import HeaderLeft from '../components/HeaderLeft'

import { renderHeaderLeft } from '../lib/utils'

const AuthStack = createStackNavigator({
  InitialAuthScreen: {
    screen: InitialAuthScreen,
    navigationOptions: () => ({
      title: 'Auth',
      headerMode: 'none',
      headerBackTitle: null,
      headerBackTitleVisible: false,
      header: null,
    }),
  },
  EmailCreateUserScreen: {
    screen: EmailCreateUserScreen,
    navigationOptions: () => ({
      headerTitle: null,
      headerMode: 'screen',
      headerBackImage: <HeaderLeft />,
      headerBackTitle: null,
    }),
  },
});

export default AuthStack
