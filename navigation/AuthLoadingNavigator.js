/*
 * RELEVANT LINKS
 * ==============
 *
 * https://reactnavigation.org/docs/en/stack-navigator.html#docsNav
 *
 */
import React from 'react'
import { createStackNavigator, createMaterialTopTabNavigator, createBottomTabNavigator } from 'react-navigation'

import AuthLoadingScreen from '../screens/AuthLoadingScreen'

const AuthLoadingStack = createStackNavigator({
  AuthLoading: {
    screen: AuthLoadingScreen,
  },
});

export default AuthLoadingStack
