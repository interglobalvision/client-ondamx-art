/*
 * RELEVANT LINKS
 * ==============
 *
 * https://reactnavigation.org/docs/en/stack-navigator.html#docsNav
 *
 */
import React from 'react'
import { Platform, Image, View } from 'react-native'
import { createStackNavigator, createMaterialTopTabNavigator, createBottomTabNavigator } from 'react-navigation'

import HomeScreen from '../screens/HomeScreen'
import ArticleScreen from '../screens/ArticleScreen'
import ArticleArchiveScreen from '../screens/ArticleArchiveScreen'
import EventScreen from '../screens/EventScreen'
import EventArchiveScreen from '../screens/EventArchiveScreen'
import ExhibitionArchiveScreen from '../screens/ExhibitionArchiveScreen'
import SettingsScreen from '../screens/SettingsScreen'
import CalendarExploreScreen from '../screens/CalendarExploreScreen'
import CalendarExhibitionsScreen from '../screens/CalendarExhibitionsScreen'
import CalendarEventsScreen from '../screens/CalendarEventsScreen'
import CalendarFilterScreen from '../screens/CalendarFilterScreen'
import DirectoryScreen from '../screens/DirectoryScreen'
import MapScreen from '../screens/MapScreen'
import SpaceScreen from '../screens/SpaceScreen'
import SpaceEventArchiveScreen from '../screens/SpaceEventArchiveScreen'
import NotificationsScreen from '../screens/NotificationsScreen'
import StaticScreen from '../screens/StaticScreen'
import LanguageSettingsScreen from '../screens/LanguageSettingsScreen'
import LoginSettingsScreen from '../screens/LoginSettingsScreen'
import NotificationsSettingsScreen from '../screens/NotificationsSettingsScreen'
import NewsletterSettingsScreen from '../screens/NewsletterSettingsScreen'
import BookmarkEventsScreen from '../screens/BookmarkEventsScreen'
import BookmarkArticlesScreen from '../screens/BookmarkArticlesScreen'
import BookmarkSpacesScreen from '../screens/BookmarkSpacesScreen'

import TabBarIcon from '../components/TabBarIcon'
import Layout from '../constants/Layout'
import HeaderTitle from '../components/HeaderTitle'
import TabBarLabel from '../components/TabBarLabel'
import HeaderLeft from '../components/HeaderLeft'

const eventScreenDefinition = {
  screen: EventScreen,
  navigationOptions: () => ({
    headerTitle: <HeaderTitle translateId={'event'} />,
    headerMode: 'screen',
    headerBackImage: <HeaderLeft />,
    headerBackTitle: null,
  }),
}

const spaceScreenDefinition = {
  screen: SpaceScreen,
  navigationOptions: () => ({
    headerTitle: <HeaderTitle translateId={'space'} />,
    headerMode: 'screen',
    headerBackImage: <HeaderLeft />,
    headerBackTitle: null,
  }),
}

const articleScreenDefinition = {
  screen: ArticleScreen,
  navigationOptions: () => ({
    headerTitle: <HeaderTitle translateId={'article'} />,
    headerMode: 'screen',
    headerBackImage: <HeaderLeft />,
    headerBackTitle: null,
  }),
}

const spaceEventArchiveScreenDefinition = {
  screen: SpaceEventArchiveScreen,
  navigationOptions: () => ({
    headerTitle: <HeaderTitle translateId={'exhibitionsAndEvents'} />,
    headerMode: 'screen',
    headerBackImage: <HeaderLeft />,
    headerBackTitle: null,
  }),
}

const HomeStack = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: () => ({
      title: 'Home',
      headerMode: 'none',
      headerBackTitle: null,
      headerBackTitleVisible: false,
      header: null,
    }),
  },
  Article: articleScreenDefinition,
  Event: {
    path: 'event/:id',
    ...eventScreenDefinition,
  },
  Space: spaceScreenDefinition,
  SpaceEventArchive: {
    ...spaceEventArchiveScreenDefinition,
  },
  ArticleArchive: {
    screen: ArticleArchiveScreen,
    navigationOptions: () => ({
      headerTitle: <HeaderTitle translateId={'magazine'} />,
      headerMode: 'screen',
      headerBackImage: <HeaderLeft />,
      headerBackTitle: null,
    }),
  },
});

const homeIconOn = require('../assets/images/navHomeOn.png')
const homeIconOff = require('../assets/images/navHomeOff.png')

HomeStack.navigationOptions = {
  tabBarOptions: {
    showLabel: false,
    style: Layout.tabBar,
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      imageSource={focused ? homeIconOn : homeIconOff}
      width={28}
      height={23}
    />
  ),
};

const CalendarStack = createStackNavigator({
  Calendar: createMaterialTopTabNavigator(
    {
      Explore: {
        screen: CalendarExploreScreen,
        navigationOptions: () => ({
          tabBarLabel: <TabBarLabel translateId={'explore'} />,
        }),
      },
      Exhibitions: {
        screen: CalendarExhibitionsScreen,
        navigationOptions: () => ({
          tabBarLabel: <TabBarLabel translateId={'exhibitions'} />,
        }),
      },
      Events: {
        screen: CalendarEventsScreen,
        navigationOptions: () => ({
          tabBarLabel: <TabBarLabel translateId={'events'} />,
        }),
      },
    },
    {
      navigationOptions: {
        title: 'Calendar',
        headerMode: 'none',
        headerBackTitle: null,
        headerBackTitleVisible: false,
        header: null,
      },
      swipeEnabled: true,
      animationEnabled: true,
      tabBarOptions: {
        indicatorStyle: {
          backgroundColor: Layout.colors.ondaBlue,
          height: 4,
        },
        style: {
          paddingTop: Layout.marginBasic * 1.5,
          backgroundColor: Layout.colors.white,
        },
      },
    }
  ),
  Article: articleScreenDefinition,
  Event: eventScreenDefinition,
  Space: spaceScreenDefinition,
  SpaceEventArchive: spaceEventArchiveScreenDefinition,
  Filter: {
    screen: CalendarFilterScreen,
    navigationOptions: () => ({
      headerTitle: <HeaderTitle translateId={'filterBy'} />,
      headerMode: 'screen',
      headerBackImage: <HeaderLeft />,
      headerBackTitle: null,
    }),
  },
  EventArchive: {
    screen: EventArchiveScreen,
    navigationOptions: () => ({
      headerTitle: <HeaderTitle translateId={'eventArchive'} />,
      headerMode: 'screen',
      headerBackImage: <HeaderLeft />,
      headerBackTitle: null,
    }),
  },
  ExhibitionArchive: {
    screen: ExhibitionArchiveScreen,
    navigationOptions: () => ({
      headerTitle: <HeaderTitle translateId={'exhibitionArchive'} />,
      headerMode: 'screen',
      headerBackImage: <HeaderLeft />,
      headerBackTitle: null,
    }),
  },
});

const calendarIconOn = require('../assets/images/navCalOn.png')
const calendarIconOff = require('../assets/images/navCalOff.png')

CalendarStack.navigationOptions = {
  tabBarOptions: {
    showLabel: false,
    style: Layout.tabBar,
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      imageSource={focused ? calendarIconOn : calendarIconOff}
      width={26}
      height={23}
    />
  ),
};

const DirectoryStack = createStackNavigator({
  Directory: createMaterialTopTabNavigator(
    {
      Map: {
        screen: MapScreen,
        navigationOptions: () => ({
          tabBarLabel: <TabBarLabel translateId={'map'} />,
        }),
      },
      Spaces: {
        screen: DirectoryScreen,
        navigationOptions: () => ({
          tabBarLabel: <TabBarLabel translateId={'directory'} />,
        }),
      },
    },
    {
      navigationOptions: {
        title: 'Directory',
        headerMode: 'none',
        headerBackTitle: null,
        headerBackTitleVisible: false,
        header: null,
      },
      swipeEnabled: true,
      animationEnabled: true,
      tabBarOptions: {
        indicatorStyle: {
          backgroundColor: Layout.colors.ondaBlue,
          height: 4,
        },
        style: {
          paddingTop: Layout.marginBasic * 1.5,
          backgroundColor: Layout.colors.white,
        },
      },
    }
  ),
  Article: articleScreenDefinition,
  Event: eventScreenDefinition,
  Space: spaceScreenDefinition,
  SpaceEventArchive: spaceEventArchiveScreenDefinition,
});

const directoryIconOn = require('../assets/images/navDirectoryOn.png')
const directoryIconOff = require('../assets/images/navDirectoryOff.png')

DirectoryStack.navigationOptions = {
  tabBarOptions: {
    showLabel: false,
    style: Layout.tabBar,
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      imageSource={focused ? directoryIconOn : directoryIconOff}
      width={28}
      height={23}
    />
  ),
};

const BookmarkStack = createStackNavigator({
  Bookmarks: createMaterialTopTabNavigator(
    {
      BookmarkEvents: {
        screen: BookmarkEventsScreen,
        navigationOptions: () => ({
          tabBarLabel: <TabBarLabel translateId={'events'} />,
        }),
      },
      BookmarkSpaces: {
        screen: BookmarkSpacesScreen,
        navigationOptions: () => ({
          tabBarLabel: <TabBarLabel translateId={'spaces'} />,
        }),
      },
      BookmarkArticles: {
        screen: BookmarkArticlesScreen,
        navigationOptions: () => ({
          tabBarLabel: <TabBarLabel translateId={'articles'} />,
        }),
      },
    },
    {
      navigationOptions: {
        title: 'Bookmarks',
        headerMode: 'none',
        headerBackTitle: null,
        headerBackTitleVisible: false,
        header: null,
      },
      swipeEnabled: true,
      animationEnabled: true,
      tabBarOptions: {
        indicatorStyle: {
          backgroundColor: Layout.colors.ondaBlue,
          height: 4,
        },
        style: {
          paddingTop: Layout.marginBasic * 1.5,
          backgroundColor: Layout.colors.white,
        },
      },
    }
  ),
  Article: articleScreenDefinition,
  Event: eventScreenDefinition,
  Space: spaceScreenDefinition,
  SpaceEventArchive: spaceEventArchiveScreenDefinition,
});

const bookmarkIconOn = require('../assets/images/navBookmarkOn.png')
const bookmarkIconOff = require('../assets/images/navBookmarkOff.png')

BookmarkStack.navigationOptions = {
  tabBarOptions: {
    showLabel: false,
    style: Layout.tabBar,
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      imageSource={focused ? bookmarkIconOn : bookmarkIconOff}
      width={28}
      height={23}
    />
  ),
};

const AccountStack = createStackNavigator({
  Account: createMaterialTopTabNavigator(
    {
      Notifications: {
        screen: NotificationsScreen,
        navigationOptions: () => ({
          tabBarLabel: <TabBarLabel translateId={'notifications'} />,
        }),
      },
      Settings: {
        screen: SettingsScreen,
        navigationOptions: () => ({
          tabBarLabel: <TabBarLabel translateId={'settings'} />,
        }),
      },
    },
    {
      navigationOptions: {
        title: 'Account',
        headerMode: 'none',
        headerBackTitle: null,
        headerBackTitleVisible: false,
        header: null,
      },
      swipeEnabled: true,
      animationEnabled: true,
      tabBarOptions: {
        indicatorStyle: {
          backgroundColor: Layout.colors.ondaBlue,
          height: 4,
        },
        style: {
          paddingTop: Layout.marginBasic * 1.5,
          backgroundColor: Layout.colors.white,
        },
      },
    }
  ),
  Static: {
    screen: StaticScreen,
    navigationOptions: ({ navigation }) => ({
      headerTitle: <HeaderTitle title={navigation.state.params.title} />,
      headerMode: 'screen',
      headerBackImage: <HeaderLeft />,
      headerBackTitle: null,
    }),
  },
  LanguageSettings: {
    screen: LanguageSettingsScreen,
    navigationOptions: () => ({
      headerTitle: <HeaderTitle translateId={'language'} />,
      headerMode: 'screen',
      headerBackImage: <HeaderLeft />,
      headerBackTitle: null,
    }),
  },
  LoginSettings: {
    screen: LoginSettingsScreen,
    navigationOptions: () => ({
      headerTitle: <HeaderTitle translateId={'login'} />,
      headerMode: 'screen',
      headerBackImage: <HeaderLeft />,
      headerBackTitle: null,
    }),
  },
  NotificationsSettings: {
    screen: NotificationsSettingsScreen,
    navigationOptions: () => ({
      headerTitle: <HeaderTitle translateId={'notifications'} />,
      headerMode: 'screen',
      headerBackImage: <HeaderLeft />,
      headerBackTitle: null,
    }),
  },
  NewsletterSettings: {
    screen: NewsletterSettingsScreen,
    navigationOptions: () => ({
      headerTitle: <HeaderTitle translateId={'newsletter'} />,
      headerMode: 'screen',
      headerBackImage: <HeaderLeft />,
      headerBackTitle: null,
    }),
  },
});

const accountIconOn = require('../assets/images/navAccountOn.png')
const accountIconOff = require('../assets/images/navAccountOff.png')

AccountStack.navigationOptions = {
  tabBarOptions: {
    showLabel: false,
    style: Layout.tabBar,
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      imageSource={focused ? accountIconOn : accountIconOff}
      width={28}
      height={23}
    />
  ),
};

export default createBottomTabNavigator({
  HomeStack: {
    path: '',
    screen: HomeStack,
  },
  CalendarStack,
  DirectoryStack,
  BookmarkStack,
  AccountStack,
})
