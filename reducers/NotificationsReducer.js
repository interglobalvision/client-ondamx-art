export const UPDATE_LAST_NOTIFICATION_RECEIVED = 'UPDATE_LAST_NOTIFICATION_RECEIVED'
import { nowTimestamp } from '../lib/dates'

const initialState = {
  lastReceived: nowTimestamp()
}

const notificationsReducer = (state = initialState, action) => {
  switch(action.type) {
    case UPDATE_LAST_NOTIFICATION_RECEIVED: {
      console.log('ACTION', action)
      return {
        lastReceived: action.date,
      }
    }
    default:
      return state
  }
}

export default notificationsReducer
