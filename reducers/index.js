import { combineReducers } from 'redux'
import { firebaseReducer } from 'react-redux-firebase'
import { firestoreReducer } from 'redux-firestore'
import { localizeReducer } from 'react-localize-redux'
import TestReducer from './TestReducer'
import NotificationsReducer from './NotificationsReducer'

// Reducers
const appReducer = combineReducers({
  test: TestReducer,
  notifications: NotificationsReducer,
  firebase: firebaseReducer,
  firestore: firestoreReducer,
  localize: localizeReducer,
})

// Setup root reducer
const rootReducer = (state, action) => {
  const newState = (action.type === 'RESET') ? undefined : state
  return appReducer(newState, action)
}

export default rootReducer
