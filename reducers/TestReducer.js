export const TOGGLE_TEST_VALUE = 'TOGGLE_TEST_VALUE'

const initialState = {
  test: true
}

const TestReducer = (state = initialState, action) => {
  switch(action.type) {
    case TOGGLE_TEST_VALUE: {
      return {
        test: !state.test,
      }
    }
    default:
      return state
  }
}

export default TestReducer
