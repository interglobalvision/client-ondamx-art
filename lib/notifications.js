import { Notifications, Linking } from 'expo'
import { withCurrentLanguage, flattenLocalizedContent } from '../lib/translations'
import { nowTimestamp } from './dates'
import setHours from 'date-fns/set_hours'
import getHours from 'date-fns/get_hours'
import subDays from 'date-fns/sub_days'
import format from 'date-fns/format'

export const scheduleEventsNotifications = (events) => {
  events.forEach( event => {

    const content = flattenLocalizedContent(event, 'es')
    // console.log(content)

    { /*
    Notifications.presentLocalNotificationAsync({
      title: `Evento: ${content.name}`,
      body: 'el body'
    })
    */}
  })
}

export const bookmarkToNotification = ({
  uid,
  itemId,
  itemCollection,
  firestore,
  translate,
  currentLanguage
}) => {
  firestore.collection(itemCollection).doc(itemId).get()
    .then( doc => {
      if (doc.exists) {

        const data = doc.data()
        const content = flattenLocalizedContent(data, currentLanguage)
        let notifications = []

        switch(itemCollection) {
          case 'events':

            const openingStart = new Date(data.openingStart.seconds * 1000)
            const closing = new Date(data.closing.seconds * 1000)

            // Check that the opening is in the future
            if (openingStart > nowTimestamp()) {
              let notificationOpeningTitleKey = 'notificationEventReminderOpeningToday' // Key used for translation

              // Set notification date the day of the opening at 9am
              let notificationDate = setHours(openingStart, 9)

              // If the opening is before 9am
              if (getHours(openingStart) < 9) {
                notificationOpeningTitleKey = 'notificationEventReminderOpeningTomorrow'

                // Set notification date the day before the opening at 9pm
                notificationDate = subDays(setHours(openingStart, 21), 1)
              }

              // Uncomment this to test, it overrides the date of the notif to NOW + 1 second
              // notificationDate = new Date(new Date().getTime() + 1000)

              notifications.push({
                title: translate(notificationOpeningTitleKey, {
                  event: content.name,
                  space: content.space.name,
                  time: format(openingStart, 'h A'),
                }),
                body: content.seoDescription,
                notificationDate,
                data: {
                  origin: 'bookmark-event',
                  type: itemCollection,
                  docId: itemId,
                  coverImage: data.coverImage,
                  url: Linking.makeUrl(`/event/${itemId}`),
                  userId: uid,
                }
              })
            }

            // Check that the closing is in the future
            if (closing > nowTimestamp()) {
              let notificationClosingTitleKey = 'notificationEventReminderClosingToday'

              // Set notification date the day of the closing at 9am
              let notificationDate = setHours(closing, 9)

              // If the closing is before 9am
              if (getHours(closing) < 9) {
                notificationClosingTitleKey = 'notificationEventReminderClosingTomorrow'

                // Set notification date the day before the closing at 9pm
                notificationDate = subDays(setHours(closing, 21), 1)
              }

              // Uncomment this to test, it overrides the date of the notif to NOW + 1 second
              // notificationDate = new Date(new Date().getTime() + 1000)

              notifications.push({
                title: translate(notificationClosingTitleKey, {
                  event: content.name,
                  space: content.space.name,
                  time: format(closing, 'h A'),
                }),
                body: content.seoDescription,
                notificationDate,
                data: {
                  origin: 'bookmark-event',
                  type: itemCollection,
                  docId: itemId,
                  coverImage: data.coverImage,
                  url: Linking.makeUrl(`/event/${itemId}`),
                  userId: uid,
                }
              })
            }

            break
        }

        // If notifications were set
        if (notifications.length) {
          notifications.forEach(notification => {

            // Schedule the notification
            Notifications.scheduleLocalNotificationAsync(notification, {
              time: notification.notificationDate.getTime(),
            })
              .then( notificationId => {
                // Save notification in db
                //
                // NOTE:
                // Sometimes the notification ID returned after it has been scheduled
                // starts with a dash (eg. -234235555). This can't be used for doc ID
                // in the collection, so I'm appending the UID at the beggining to avoid
                // problems.
                //
                const { title, body, notificationDate, data } = notification

                firestore.set({
                  collection: 'notifications',
                  doc: `${uid}-${notificationId}`,
                },{
                  createDate: nowTimestamp(),
                  published: false,
                  notificationDate: notification.notificationDate,
                  title,
                  body,
                  notificationDate,
                  notificationId,
                  ...data
                })
              })
              .catch(console.error)
          })
        }

      } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
      }
    }).catch( error => {
      console.log("Error getting document:", error);
    });
}

export const removeBookmarkNotification = ({ uid, itemId, itemCollection, firestore }) => {
  // Find notifications that match the bookmark
  firestore.get({
    collection: 'notifications',
    where: [
      ['published','==',false],
      ['userId','==',uid],
      ['docId','==',itemId],
      ['type','==',itemCollection],
    ]
  })
    .then(querySnapshot => {
      querySnapshot.forEach( doc => {
        const data = doc.data()

        // Cancel scheduled notification
        Notifications.cancelScheduledNotificationAsync(data.notificationId)

        // Delete notification from collection
        firestore.delete({
          collection: 'notifications',
          doc: doc.id,
        })
      })
    })
    .catch(console.error)
}


/*
 * notificationId: string
 * data: object
 *
 * eg.
{
  "type": "events",
  "url": "exp://192.168.1.66:19000/--/event/JGxOIX153BLBuEdphknr",
  "docId": "JGxOIX153BLBuEdphknr",
  "coverImage": {
    "metadata": {},
    "createdDate": 1569882536794,
    "mediaUrl": "https://firebasestorage.googleapis.com/v0/b/prod-ondamx-art.appspot.com/o/media%2F1569882535216-flyer_ojo_histerico_-_nicole_chaput.jpg?alt=media&token=f68aa1f0-9d32-436e-bec4-c765fbf10e38",
    "localizedContent": {
      "en": {
        "title": "Nicole Chaput",
        "caption": ""
      },
      "es": {
        "title": "Nicole Chaput",
        "caption": ""
      }
    },
    "width": 896,
    "updatedDate": 1569882558657,
    "id": "1ax8ixguJUW2Fk0q82Az",
    "height": 772
  },
  "origin": "bookmark-event"
}
 */
export const markNotificationAsPublished = (notificationId, data, firestore) => {
  const { userId } = data

  firestore.update({
    collection: 'notifications',
    doc: `${userId}-${notificationId}`,
  }, {
    published: true,
  })
    .then((res) => {
      console.log('notification updated')
    })
    .catch(console.error)

}
