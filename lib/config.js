import Constants from 'expo-constants'

export const firebase = {
  apiKey: Constants.manifest.extra.keys.FIREBASE_API_KEY,
  authDomain: Constants.manifest.extra.keys.FIREBASE_AUTH_DOMAIN,
  databaseURL: Constants.manifest.extra.keys.FIREBASE_DATABSE_URL,
  projectId: Constants.manifest.extra.keys.FIREBASE_PROJECT_ID,
  storageBucket: Constants.manifest.extra.keys.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: Constants.manifest.extra.keys.FIREBASE_MESSAGING_SENDER_ID,
}

export const rrfConfig = {
	userProfile: 'users',
	useFirestoreForProfile: true, // Store in Firestore instead of Real Time DB
	enableLogging: false
}

export default { firebase, rrfConfig }
