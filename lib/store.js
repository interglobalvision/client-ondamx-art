import { firebase as fbConfig } from './config'
import { getFirebase } from 'react-redux-firebase'
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from '../reducers'

export default function configureStore(initialState, history) {
  const enhancers = []
  const middleware = [thunk.withExtraArgument(getFirebase)] // add middlewares

  // This is boilerplate code from the redux devtools extension
  if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__

    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension())
    }
  }

  const createStoreWithMiddleware = compose(
    applyMiddleware(...middleware),
    ...enhancers
	)(createStore)

	const store = createStoreWithMiddleware(rootReducer)

	if (module.hot) {
		// Enable Webpack hot module replacement for reducers
		module.hot.accept('../reducers', () => {
			const nextRootReducer = require('../reducers')
			store.replaceReducer(nextRootReducer)
		})
	}

	return store
}
