import { showLocation } from 'react-native-map-link'

const googleMapsApiKey = 'AIzaSyBUQnKGSCeMZJHyQJQzNhYoLTTUirJrk7U'

export const getCurrentLocation = () => {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(position => resolve(position), e => reject(e));
  });
};

// calculate distance between coordinate sets
const distanceBetweenPoints = (origin, item) => {
  const latDistance = getDistance(origin.lat, item.location.lat)
  const lonDistance = getDistance(origin.lon, item.location.lon)
  const distance = Math.abs(Math.sqrt(latDistance + lonDistance))
  return distance
}

// calculate distance between two points
const getDistance = (p1, p2) => {
  const distance = (p1 - p2) * (p1 - p2)
  return distance
}

// sort items by distance from origin
export const sortByDistance = (origin, items) => {
  let orderedItems = items

  if (items) {

    // filter out items without lat/lon set
    orderedItems = items.filter(item => {
      return (item.location.lat !== 0 && item.location.lon !== 0)
    })

    // sort items by distance from origin
    orderedItems.sort((a, b) => {

      const aDistance = distanceBetweenPoints(origin, a)
      const bDistance = distanceBetweenPoints(origin, b)

      return aDistance - bDistance
    })

  }

  return orderedItems
}

export const openMapsApp = (lat, lon, title = null, translate) => {
  showLocation({
    latitude: lat,
    longitude: lon,
    title: title,
    appsWhiteList: ['google-maps','apple-maps'],
    dialogTitle: false,
    dialogMessage: false,
    showHeader: false,
    cancelText: translate('cancel'),
  })
}

export const getStaticMapUrl = (lat, lon, width = 640, height = 640, zoom = 15, marker = true, markerColor = 'black') => {
  let url = 'https://maps.googleapis.com/maps/api/staticmap?'
  url += 'center=' + lat + ',' + lon
  url += '&zoom=' + zoom
  url += marker ? '&markers=color:' + markerColor + '%7C' + lat + ',' + lon : ''
  url += '&size=' + width + 'x' + height
  url += '&scale=2'
  url += '&key=' + googleMapsApiKey

  return url
}
