import { nowDate, nowSeconds, oneDayOfSeconds, oneWeekFromNowSeconds } from '../lib/dates'

export const calendarFilters = [
  /*{
    filterId: 'nearby',
    translateId: 'nearby',
    firestoreQuery: {
      collection: 'events',
      where: [
        ['status', '==', 'published'],
        ['closing', '>', nowDate()],
      ],
      orderBy: ['closing'],
      storeAs: 'calendarNearby',
    },
    limit: 20,
  },*/
  {
    filterId: 'ongoing',
    translateId: 'ongoing',
    firestoreQuery: {
      collection: 'events',
      where: [
        ['status', '==', 'published'],
        ['closing', '>', nowDate()],
      ],
      orderBy: ['closing'],
      storeAs: 'calendarOngoing',
    },
    filterCallback: (item) => {
      return item.openingStart.seconds < nowSeconds
    },
    sortCompare: (a, b) => {
      return b.openingStart.seconds - a.openingStart.seconds
    },
  },
  {
    filterId: 'closingSoon',
    translateId: 'closingSoon',
    firestoreQuery: {
      collection: 'events',
      where: [
        ['status', '==', 'published'],
        ['closing', '>', nowDate()],
      ],
      orderBy: ['closing', 'asc'],
      storeAs: 'calendarClosingSoon',
    },
    filterCallback: (item) => {
      return item.openingStart.seconds < nowSeconds && item.closing.seconds < oneWeekFromNowSeconds
    },
  },
]

export const mapFilters = [
  {
    filterId: 'ongoing',
    translateId: 'events',
    collection: 'events',
    firestoreQuery: [{
      collection: 'events',
      where: [
        ['status', '==', 'published'],
        ['closing', '>', nowDate()],
      ],
      orderBy: 'closing',
      storeAs: 'mapOngoing',
    }],
    filterCallback: (item) => {
      return item.openingStart.seconds < (nowSeconds + oneDayOfSeconds)
    },
    sortCompare: (a, b) => {
      // openingStart DESC
      return b.openingStart.seconds - a.openingStart.seconds
    },
  },
  {
    filterId: 'all',
    translateId: 'spaces',
    collection: 'spaces',
    firestoreQuery: [{
      collection: 'spaces',
      where: ['status', '==', 'published'],
      orderBy: 'name',
      storeAs: 'mapAll',
    }],
  },
  {
    filterId: 'bookmarked',
    translateId: 'bookmarked',
    collection: 'events',
    requiresUid: true,
    firestoreQuery: [{
      collection: 'events',
      where: [
        ['status', '==', 'published'],
        ['closing', '>', nowDate()],
      ],
      orderBy: 'closing',
      storeAs: 'mapBookmarked',
    }],
    filterCallback: (item) => {
      return item.openingStart.seconds < (nowSeconds + oneDayOfSeconds)
    },
    sortCompare: (a, b) => {
      // openingStart DESC
      return b.openingStart.seconds - a.openingStart.seconds
    },
  },
  {
    filterId: 'highlights',
    translateId: 'highlights',
    collection: 'events',
    firestoreQuery: [
      {
        collection: 'events',
        where: [
          ['status', '==', 'published'],
          ['highlight', '==', 'highlighted'],
          ['closing', '>', nowDate()],
        ],
        orderBy: 'closing',
        storeAs: 'mapHighlighted',
      },
      {
        collection: 'events',
        where: [
          ['status', '==', 'published'],
          ['highlight', '==', 'featured'],
          ['closing', '>', nowDate()],
        ],
        orderBy: 'closing',
        storeAs: 'mapFeatured',
      }
    ],
    sortCompare: (a, b) => {
      // openingStart DESC
      return b.openingStart.seconds - a.openingStart.seconds
    },
  },
]

export const directoryFilters = [
  {
    filterId: 'all',
    translateId: 'all',
    firestoreQuery: {
      orderBy: ['name'],
      where: ['status','==','published'],
    },
  },
  {
    filterId: 'galleries',
    translateId: 'galleries',
    firestoreQuery: {
      orderBy: ['name'],
      where: [
        ['status','==','published'],
        ['type','==','gallery'],
      ]
    },
  },
  {
    filterId: 'independents',
    translateId: 'independents',
    firestoreQuery: {
      orderBy: ['name'],
      where: [
        ['status','==','published'],
        ['type','==','independent'],
      ],
    },
  },
  {
    filterId: 'museums',
    translateId: 'museums',
    firestoreQuery: {
      orderBy: ['name'],
      where: [
        ['status','==','published'],
        ['type','==','museum'],
      ],
    },
  },
  {
    filterId: 'residencies',
    translateId: 'residencies',
    firestoreQuery: {
      orderBy: ['name'],
      where: [
        ['status','==','published'],
        ['type','==','residency'],
      ],
    },
  },
  {
    filterId: 'services',
    translateId: 'services',
    firestoreQuery: {
      orderBy: ['name'],
      where: [
        ['status','==','published'],
        ['type','==','service'],
      ],
    },
  },
]
