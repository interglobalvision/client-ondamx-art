import React from 'react'
import { Text, View } from 'react-native'
import { format } from 'date-fns'
import en from 'date-fns/locale/en'
import es from 'date-fns/locale/es'
import isPast from 'date-fns/is_past'
import isFuture from 'date-fns/is_future'
import subDays from 'date-fns/sub_days'
import isToday from 'date-fns/is_today'
import isTomorrow from 'date-fns/is_tomorrow'
import distanceInWordsToNow from 'date-fns/distance_in_words_to_now'
import addDays from 'date-fns/add_days'
import { convertToTimeZone } from 'date-fns-timezone'

export const nowDate = () => new Date()
export const nowSeconds = nowDate().getTime() / 1000
export const oneWeekFromNowSeconds = addDays(nowDate(), 7).getTime() / 1000
export const timeZone = 'America/Mexico_City'
export const oneDayOfSeconds = 86400
export const nowTimestamp =  () => new Date(Date.now())

export const formatEventDates = (eventContent, currentLanguage, translate, includeDateSpan = false, textStyle) => {
  const { openingStart, openingEnd, closing, hasOpening, type } = eventContent
  const locale = currentLanguage === 'en' ? en : es

  const start = openingStart !== undefined ? convertToTimeZone(new Date(openingStart.seconds * 1000), { timeZone}) : false
  const end = openingEnd !== undefined ? convertToTimeZone(new Date(openingEnd.seconds * 1000), { timeZone}) : false
  const close = closing !== undefined ? convertToTimeZone(new Date(closing.seconds * 1000), { timeZone}) : false

  const isExpo = type === 'exhibition'
  const isMultiDay = eventContent.multiDay

  let eventDates = null

  const dateFormat = currentLanguage === 'en' ? 'MMM D' : 'D [de] MMM'
  const dateSpanCloseFormat = currentLanguage === 'en' ? 'MMM D[,] YYYY' : 'D [de] MMM[,] YYYY'
  const timeFormat = 'h:mm A'
  const dateSpanDates = format(start, dateFormat, {locale}) + ' – ' + format(close, dateSpanCloseFormat, {locale})

  if (start) {
    if (isExpo) {
      if (isFuture(start)) {
        // is future
        if (hasOpening) {
          // has opening
          if (isToday(start)) {
            // opens today
            eventDates = translate('opens') + ' ' + translate('today').toLowerCase()
          } else if (isTomorrow(start)) {
            // opens tomorrow
            eventDates = translate('opens') + ' ' + translate('tomorrow').toLowerCase()
          } else {
            // opens in future
            eventDates = translate('opensThe') + ' ' + format(start, dateFormat, {locale})
          }
          // concat opening time
          eventDates += ' | ' + format(start, timeFormat)
        } else {
          // doesn't have opening
          if (isToday(start)) {
            // starts today
            eventDates = translate('starts') + ' ' + translate('today').toLowerCase()
          } else if (isTomorrow(start)) {
            // starts tomorrow
            eventDates = translate('starts') + ' ' + translate('tomorrow').toLowerCase()
          } else {
            // starts in future
            eventDates = translate('startsThe') + ' ' + format(start, dateFormat, {locale})
          }
        }
      } else if (isPast(start) && isFuture(close)) {
        // is current
        if (isToday(close)) {
          // closes today
          eventDates = translate('ends') + ' ' + translate('today').toLowerCase()
        } else if (isTomorrow(close)) {
          // closes tomorrow
          eventDates = translate('ends') + ' ' +  translate('tomorrow').toLowerCase()
        } else if (isPast(subDays(close, 8))) {
          // closes within 7 days
          const distance = distanceInWordsToNow(close, {
            includeSeconds: false,
            addSuffix: false,
            locale
          })
          eventDates = translate('endsIn') + ' ' + distance
        } else {
          // closes in more than 7 days
          eventDates = translate('endsThe') + ' ' + format(close, dateFormat, {locale})
        }
      } else if (isPast(close)) {
        // is past: date span
        eventDates = dateSpanDates
      }
    } else {
      // non-exhibition event
      if (isFuture(start) || isToday(start)) {
        if (hasOpening) {
          // has opening
          if (isToday(start)) {
            // opens today
            eventDates = translate('opens') + ' ' + translate('today').toLowerCase()
          } else if (isTomorrow(start)) {
            // opens tomorrow
            eventDates = translate('opens') + ' ' + translate('tomorrow').toLowerCase()
          } else {
            // opens in future
            eventDates = translate('opensThe') + ' ' + format(start, dateFormat, {locale})
          }
          // concat opening time
          eventDates += ' | ' + format(start, timeFormat)
        } else {
          // is today or future
          if (isToday(start)) {
            // is today
            eventDates = translate('today')
          } else if (isTomorrow(start)) {
            // is tomorrow
            eventDates = translate('tomorrow')
          } else {
            // is future
            eventDates = format(start, dateFormat, {locale})
          }
          // concat time span
          eventDates += ' | ' + format(start, timeFormat) + ' – ' + format(close, timeFormat)
        }
      } else if (isMultiDay && isPast(start) && isFuture(close)) {
        // is current
        if (isToday(close)) {
          // closes today
          eventDates = translate('ends') + ' ' + translate('today').toLowerCase()
        } else if (isTomorrow(close)) {
          // closes tomorrow
          eventDates = translate('ends') + ' ' +  translate('tomorrow').toLowerCase()
        } else if (isPast(subDays(close, 8))) {
          // closes within 7 days
          const distance = distanceInWordsToNow(close, {
            includeSeconds: false,
            addSuffix: false,
            locale
          })
          eventDates = translate('endsIn') + ' ' + distance
        } else {
          // closes in more than 7 days
          eventDates = translate('endsThe') + ' ' + format(close, dateFormat, {locale})
        }
      } else {
        // is past: date span
        eventDates = format(start, dateFormat, {locale}) + ' – ' + format(close, dateSpanCloseFormat, {locale})
      }
    }
  }

  if (isMultiDay) {
    return (
      <Text style={textStyle}>{dateSpanDates}</Text>
    )
  }

  return (
    <Text style={textStyle}>{eventDates}</Text>
  )
}
