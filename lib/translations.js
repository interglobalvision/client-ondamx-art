import { connect } from 'react-redux'
import { getActiveLanguage, getTranslate } from 'react-localize-redux'

export const withCurrentLanguage = (WrappedComponent) => {
  return connect((state, props) => {
    return {
      currentLanguage: getActiveLanguage(state.localize).code
    }
  })(WrappedComponent)
}

export const withTranslate = (WrappedComponent) => {
  return connect((state, props) => {
    return {
      translate: getTranslate(state.localize)
    }
  })(WrappedComponent)
}

export const flattenLocalizedContent = (content, language) => {
  if (content.localizedContent !== undefined && content.localizedContent[language] !== undefined) {
    // https://codeburst.io/use-es2015-object-rest-operator-to-omit-properties-38a3ecffe90
    const {
      localizedContent,
      ...otherContent
    } = content

    return {
      ...otherContent,
      ...localizedContent[language]
    }
  }

  else if (content[language] !== undefined) {
    return content[language]
  }

  return content
}
