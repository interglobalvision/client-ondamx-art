import {
  Alert,
  Platform,
} from 'react-native'
import Constants from 'expo-constants'
import { devicesWithNotch } from '../constants/Device'

export const constructUrlSlug = (slug, title) => {
  const urlSlug = slug !== undefined ? slug : title.replace(/[^a-zA-Z0-9]/g,'-').toLowerCase()
  return urlSlug
}

export const constructUrl = (title, slug, type, itemId) => {
  const urlSlug = constructUrlSlug(slug, title)
  return `https://ondamx.art/${type}/${urlSlug}-${itemId}`
}

export const trimByWord = (str, maxWords = 35) => {
  let result = str
  let resultArray = result.split(' ')

  if (resultArray.length > maxWords) {
    resultArray = resultArray.slice(0, maxWords)
    result = resultArray.join(' ') + '…'
  }

  return result
}

export const trimByChar = (str = '', maxChars = 180) => {
  if (str === undefined || str === null) {
    return ''
  }

  let result = str
  let resultArray = result.split('')

  if (resultArray.length > maxChars) {
    resultArray = resultArray.slice(0, maxChars)
    result = resultArray.join('') + '…'
  }

  return result
}

export const displayUserOnlyFeatureAlert = (navigation, translate) => {
  Alert.alert(
    translate('onlyLoggedInUsers'),
    null,
    [
      {
        text: translate('login'),
        onPress: () => {
          navigation.navigate('Auth')
        }
      },
      {
        text: translate('notNow'),
      },
    ]
  );
}

export const getDeviceModel = () => {
  return Platform.select({
    ios: Constants.platform.ios !== undefined ? Constants.platform.ios.model : false,
    android: Constants.deviceName
  })
}

export const hasNotch = () => {
  return (
    devicesWithNotch.findIndex( item => item.model.toLowerCase() === getDeviceModel().toLowerCase() ) !== -1
  )
}
