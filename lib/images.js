import Layout from  '../constants/Layout'
/** Available image sizes
[width, height]
[1005,1260],
[1005,666],
[880,309],
[581,459],
[324,324],
[68,68],
*/

export const getMediaThumb = (url, width, height) => {
  const filename = url.substring(url.lastIndexOf('/')+1);
  const parts = filename.split('.')
  const thumbFilename = `${parts[0]}_${width}x${height}_thumb.${parts[1]}`

  return url.replace(filename,thumbFilename)
}

export const getMediaSizeForFullWidth = (image) => {
  let { width, height } = image
  const { window } = Layout

  // Use 16:9 as default
  let toScreenRatio = 9 / 16

  if (width && height) {
    toScreenRatio = window.width / width

    return {
      width: width * toScreenRatio,
      height: height * toScreenRatio,
    }
  }

  return {
    width: window.width,
    height: window.width * toScreenRatio,
  }
}
