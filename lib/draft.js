import React from 'react';
import { compose } from 'redux'
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Linking
} from 'react-native'
import Layout from '../constants/Layout'
import { flattenLocalizedContent } from '../lib/translations'
import { getMediaSizeForFullWidth } from '../lib/images'

export const atomicHandler = (item, entityMap, currentLanguage) => {
  const localizeCaption = (data) => {
    const content = flattenLocalizedContent(data, currentLanguage)

    if (content.caption.length > 0) {
      return (
        <View style={{
          ...Layout.captionHolder,
          minHeight: 'auto',
          paddingTop: Layout.marginBasic / 2
        }}>
          <Text style={Layout.fontStyles.caption}>{content.caption}</Text>
        </View>
      )
    }

    return null
  }

  if (item.entityRanges.length) {
    const index = item.entityRanges[0].key

    switch (entityMap[index].type) {
      case 'IMAGE':
        const mediaSize = getMediaSizeForFullWidth(entityMap[index].data)
        return (
          <View key={item.key} style={{
            flex: 1,
            marginBottom: Layout.marginBasic,
            marginTop: Layout.marginBasic
           }}>
            <Image
              style={{ width: mediaSize.width, height: mediaSize.height }}
              source={{ uri: entityMap[index].data.src }}
            />
            {localizeCaption(entityMap[index].data)}
          </View>
        )
      case 'draft-js-video-plugin-video':
        const { src } = entityMap[index].data
        const videoId = src.match(/(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=))([\w\-]{10,12})\b/)[1]
        return (
          <View key={item.key}
            style={{
              flex: 1,
              marginBottom: Layout.marginBasic,
              marginTop: Layout.marginBasic,
             }}
          >
            <TouchableOpacity
              style={{
                flex: 1,
                height: Layout.window.width * 0.5625,
              }}
              onPress={() => {
                Linking.openURL(src)
              }}
            >
              <Image
                style={{ flex: 1 }}
                source={{ uri: `https://img.youtube.com/vi/${videoId}/maxresdefault.jpg` }}
              />
              <Image
                source={require('../assets/images/videoPlay.png')}
                style={{
                  width: 50,
                  height: 50,
                  position: 'absolute',
                  top: ((Layout.window.width * 0.5625) / 2) - 25,
                  left: (Layout.window.width / 2) - 25,
                }}
              />
            </TouchableOpacity>
          </View>
        )
      default:
        return null
    }
  }

  return null
}
