import { createStore, compose } from 'redux'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/firestore'
import { reactReduxFirebase } from 'react-redux-firebase'
import { reduxFirestore } from 'redux-firestore'
import Constants from 'expo-constants'

// Firebase configuration
const firebaseConfig = {
  apiKey: Constants.manifest.extra.keys.FIREBASE_API_KEY,
  authDomain: Constants.manifest.extra.keys.FIREBASE_AUTH_DOMAIN,
  databaseURL: Constants.manifest.extra.keys.FIREBASE_DATABSE_URL,
  projectId: Constants.manifest.extra.keys.FIREBASE_PROJECT_ID,
  storageBucket: Constants.manifest.extra.keys.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: Constants.manifest.extra.keys.FIREBASE_MESSAGING_SENDER_ID,
}

// Init firebase instance
firebase.initializeApp(firebaseConfig)

// Initialize Cloud Firestore through Firebase
firebase.firestore()

// react-redux-firebase config
const reduxFirebaseConfig = {
  userProfile: 'users',
  // enableLogging: true, // enable/disable Firebase's database logging
}

// react-firestore config
const reduxFirestoreConfig = {
  logErrors: true,
  enableLogging: true,
  logListenerError: true,
}

// Add redux Firebase to compose
// Add BOTH store enhancers when making store creator
export const createStoreWithFirebase = compose(
  reduxFirestore(firebase, reduxFirestoreConfig),
  reactReduxFirebase(firebase, reduxFirebaseConfig),
)(createStore)
