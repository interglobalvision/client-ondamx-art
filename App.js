import React from 'react'
import Constants from 'expo-constants'
import { AppState } from 'react-native'
import Sentry from 'sentry-expo'
import { AppLoading } from 'expo'
import { Asset } from 'expo-asset'
import * as Font from 'expo-font'
import * as Icon from '@expo/vector-icons'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/firestore' // make sure you add this for firestore
import { createFirestoreInstance } from 'redux-firestore'
import { ReactReduxFirebaseProvider } from 'react-redux-firebase'
import { Provider } from 'react-redux'
import configureStore from './lib/store'
import { firebase as fbConfig } from './lib/config'
import { LocalizeProvider } from 'react-localize-redux'
import Main from './components/Main'

// DISABLE YELLOW EXPO WARNING
//import { YellowBox } from 'react-native';
//import _ from 'lodash';

//YellowBox.ignoreWarnings(['Setting a timer']);
//const _console = _.clone(console);
//console.warn = message => {
  //if (message.indexOf('Setting a timer') <= -1) {
    //_console.warn(message);
  //}
//};

// SENTRY
// Sentry.enableInExpoDevelopment = true
Sentry.config('https://44f78edb2f9b4ef19dea37cbd96015c5@sentry.io/1542291').install()
Sentry.setRelease(Constants.manifest.revisionId)


const store = configureStore(initialState)

// Initialize Firebase instance
firebase.initializeApp(fbConfig)

const initialState = window.__INITIAL_STATE__ || {
  firebase: { authError: null }
}

const rrfProps = {
  firebase,
  config:  {
    userProfile: 'users',
    enableLogging: true, // enable/disable Firebase's database logging
    useFirestoreForProfile: true,
  },
  dispatch: store.dispatch,
  createFirestoreInstance,
}

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
    appState: AppState.currentState,
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange)
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange)
  }

  _handleAppStateChange = nextAppState => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
    }
    this.setState({ appState: nextAppState })
  }

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      )
    } else {
      return (
        <Provider store={store}>
          <ReactReduxFirebaseProvider {...rrfProps}>
            <LocalizeProvider store={store}>
              <Main />
            </LocalizeProvider>
          </ReactReduxFirebaseProvider>
        </Provider>
      )
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        require('./assets/images/robot-dev.png'),
        require('./assets/images/robot-prod.png'),
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Icon.Ionicons.font,
        'heading': require('./assets/fonts/BarlowCondensed-SemiBold.ttf'),
        'medium': require('./assets/fonts/lunchtype22-medium-webfont.ttf'),
        'regular': require('./assets/fonts/lunchtype22-regular-webfont.ttf'),
        'italic': require('./assets/fonts/lunchtype23-regular-italic-webfont.ttf'),
      }),
    ])
  }

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error)
  }

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true })
  }
}
