###Build & Deploy to App Store

[https://docs.expo.io/versions/v33.0.0/distribution/building-standalone-apps/]
[https://docs.expo.io/versions/v33.0.0/distribution/uploading-apps/]

1. `expo login` to igv expo account if you aren't already
2. `expo build:ios` or `expo build:android`
   - you need to be on the igv expo account
   - say yes to all the
   - account details in sync
3. `expo upload:ios` or `expo upload:android`
   - use the igv apple id
   - you may need two-step verification
   - or you may be asked to generate an "application specific password" (red error)
     - https://appleid.apple.com/account/manage
