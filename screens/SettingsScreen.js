import React from 'react'
import Constants from 'expo-constants'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { withCurrentLanguage, withTranslate, flattenLocalizedContent } from '../lib/translations'
import OptionButton from '../components/OptionButton'
import {
  ListView,
  FlatList,
  ScrollView,
  View,
  Text,
  Alert,
  StatusBar,
} from 'react-native'
import Layout from '../constants/Layout'

class SettingsScreen extends React.Component {
  _handleSettingsButtonPress = (data) => {
    const { navigation } = this.props
    const { routeName } = data
    navigation.push(routeName)
  }

  _handlePageButtonPress = (data) => {
    const { navigation } = this.props
    const { title, id, routeName } = data
    navigation.push(routeName, { id, title })
  }


  render() {
    const { pages, currentLanguage, firebase, translate, uid, navigation } = this.props

    if (!isLoaded(pages)) {
      return null
    }

    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <ScrollView style={{
          paddingTop: Layout.marginBasic
        }}>
          { uid === undefined ? (
              <View>
                <OptionButton
                  translateId={'login'}
                  handlePress={() => {
                    navigation.navigate('Auth')
                  }}
                />
                <OptionButton
                  translateId={'language'}
                  handlePress={this._handleSettingsButtonPress}
                  data={{
                    routeName: 'LanguageSettings',
                  }}
                  arrow
                />
              </View>
            ) : (
              <FlatList
                data={[
                  {translateId: 'language', routeName: 'LanguageSettings'},
                  //{translateId: 'notifications', routeName: 'NotificationsSettings'},
                  //{translateId: 'newsletter', routeName: 'NewsletterSettings'},
                ]}
                keyExtractor={(item) => {return 'settings_option_' + item.translateId}}
                scrollEnable={false}
                renderItem={({item, index}) => {
                  return (
                    <OptionButton
                      translateId={item.translateId}
                      handlePress={this._handleSettingsButtonPress}
                      data={{
                        routeName: item.routeName,
                      }}
                      arrow
                    />
                  )
                }}
              />
            )
          }
          { !isEmpty(pages) &&
            <FlatList
              data={pages}
              keyExtractor={(item) => item.id}
              scrollEnable={false}
              style={Layout.dividerTop}
              extraData={currentLanguage}
              renderItem={({item, index}) => {
                const content = flattenLocalizedContent(item, currentLanguage)
                return (
                  <OptionButton
                    title={content.title}
                    handlePress={this._handlePageButtonPress}
                    data={{
                      title: content.title,
                      id: item.id,
                      routeName: 'Static',
                    }}
                    arrow
                  />
                )
              }}
            />
          }
          { uid !== undefined &&
            <View style={Layout.dividerTop}>
              <OptionButton
                translateId={'logout'}
                handlePress={() => {
                  Alert.alert(
                    translate('logout'),
                    translate('areYouSure'),
                    [
                      {
                        text: translate('cancel'),
                        style: 'cancel',
                      },
                      {text: 'OK', onPress: () => {
                        firebase.logout()
                        navigation.navigate('Auth')
                      }},
                    ],
                  );
                }}
              />
            </View>
          }
          <View style={Layout.dividerTop}>
            <OptionButton title={`Version ${Constants.manifest.version}`} />
          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    pages: state.firestore.ordered.pages,
    uid: state.firebase.auth.uid,
  }
}

export default compose(
  withCurrentLanguage,
  withTranslate,
  firestoreConnect((props) => {
    return [{
      collection: 'pages',
      orderBy: ['order', 'asc'],
      where: ['status','==','published'],
    }]
  }),
  connect(mapStateToProps)
)(SettingsScreen)
