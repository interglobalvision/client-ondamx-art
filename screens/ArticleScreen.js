import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { withCurrentLanguage, flattenLocalizedContent } from '../lib/translations'
import { Translate } from 'react-localize-redux'

import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Button,
  StatusBar,
} from 'react-native'

import Layout from '../constants/Layout'
import ArticleContent from '../components/ArticleContent'
import ShareButton from '../components/ShareButton'
import BookmarkButton from '../components/BookmarkButton'
import CoverImage from '../components/CoverImage'
import ImageCarousel from '../components/ImageCarousel'
import RelatedInternalList from '../components/RelatedInternalList'
import ArticleDate from '../components/ArticleDate'

class ArticleScreen extends React.Component {
  returnCoverContent = (content) => {
    const { currentLanguage } = this.props
    if (content.imageCarousel.length) {
      return (
        <ImageCarousel content={content} currentLanguage={currentLanguage} />
      )
    } else if (content.coverImage) {
      return (
        <CoverImage media={content.coverImage} currentLanguage={currentLanguage} />
      )
    }
    return null
  }

  render() {
    const { item, currentLanguage, navigation } = this.props
    const itemId = navigation.getParam('id')

    if (!isLoaded(item)) {
      return null
    }

    else if (isEmpty(item)) {
      return null
    }

    const content = flattenLocalizedContent(item, currentLanguage)

    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <ScrollView style={Layout.container}>
          <View style={{
            padding: Layout.marginBasic * 2,
            backgroundColor: Layout.colors.lightBlue,
          }}>
            <Text style={Layout.fontStyles.article.single.title}>{content.title}</Text>
            {content.subtitle !== undefined &&
              <Text style={Layout.fontStyles.article.single.subtitle}>{content.subtitle}</Text>
            }
            <View style={{
              flexDirection: 'row',
              marginTop: Layout.marginBasic * 2,
            }}>
              <View style={{
                flex: 2
              }}>
                <Text style={Layout.fontStyles.article.single.type}><Translate id={content.type}/></Text>
                <Text style={Layout.fontStyles.article.single.author}>{content.author}</Text>
              </View>
              <View style={{
                flex: 1
              }}>
                <ShareButton
                  slug={item.slug}
                  title={content.title}
                  itemId={itemId}
                  type={'escrito'}
                />
              </View>
              <View>
                <BookmarkButton
                  itemId={itemId}
                  itemCollection={'articles'}
                  itemBookmarks={item.bookmarks}
                />
              </View>
            </View>
          </View>
          <View style={{
            backgroundColor: Layout.colors.white
          }}>
            { this.returnCoverContent(content) }
            <ArticleContent content={content.mainContent} />
            <View style={{
              padding: Layout.marginBasic * 2,
              paddingTop: 0,
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
              <View>
                <ArticleDate timestamp={content.publishDate.seconds} currentLanguage={currentLanguage.code} />
              </View>
              <View>
                <ShareButton
                  slug={item.slug}
                  title={content.title}
                  itemId={itemId}
                  type={'escrito'}
                />
              </View>
            </View>
          </View>
          <View style={{
            marginBottom: Layout.marginBasic * 2
          }}>
            <RelatedInternalList
              docId={itemId}
              collection={'events'}
              translateId={'relatedEvents'}
              navigation={navigation}
            />
            <RelatedInternalList
              docId={itemId}
              collection={'spaces'}
              translateId={'relatedSpaces'}
              navigation={navigation}
            />
          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  // Get the article id from the navigation. Kinda
  // like getting it from a URL param [;
  const docId = props.navigation.getParam('id')
  return {
    item: state.firestore.data[`article-${docId}`]
  }
}

export default compose(
  withCurrentLanguage,
  firestoreConnect( props => {
    // Get the document id from the navigation. Kinda
    // like getting it from a URL param [;
    const docId = props.navigation.getParam('id')

    // Fetch the docuement
    return [{
      collection: 'articles',
      doc: docId,
      storeAs: `article-${docId}`
    }]
  }),
  connect(mapStateToProps)
)(ArticleScreen)
