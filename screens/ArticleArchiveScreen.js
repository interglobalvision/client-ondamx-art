import React, { Component } from 'react'
import {
  ScrollView,
  View,
  StatusBar,
} from 'react-native'
import Layout from '../constants/Layout'
import ArticlesList from '../components/ArticlesList'

export default class ArticleArchiveScreen extends Component {
  render() {
    const { navigation } = this.props

    return (
      <View style={{
        ...Layout.container,
        backgroundColor: Layout.colors.lightBlue,
      }}>
        <StatusBar hidden={false} />
        <ScrollView
          style={{
            ...Layout.container,
            backgroundColor: Layout.colors.lightBlue,
          }}
        >
          <View
            style={{
              paddingTop: Layout.marginBasic,
              paddingBottom: Layout.marginBasic * 2,
            }}
          >
            <ArticlesList
              navigation={navigation}
              firestoreQuery={[{
                collection: 'articles',
                orderBy: ['publishDate', 'desc'],
                where: ['status','==','published'],
                storeAs: 'articles',
              }]}
            />
          </View>
        </ScrollView>
      </View>
    )
  }
}
