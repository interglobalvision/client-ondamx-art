import React from 'react'
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native'
import Layout from '../constants/Layout'
import EventsList from '../components/EventsList'
import OptionButton from '../components/OptionButton'
import { nowDate, nowSeconds } from '../lib/dates'

export default class CalendarExhibitionsScreen extends React.Component {
  _handleArchivePress = () => {
    const { navigation } = this.props
    navigation.push('ExhibitionArchive')
  }

  render() {
    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <ScrollView style={Layout.container}
          contentContainerStyle={{
            paddingBottom: Layout.marginBasic
          }}
        >
          <EventsList
            navigation={this.props.navigation}
            firestoreQuery={[{
              collection: 'events',
              orderBy: ['closing', 'asc'],
              where: [
                ['status', '==', 'published'],
                ['type', '==', 'exhibition'],
                ['closing', '>=', nowDate()],
              ],
              storeAs: 'calendarExhibitionsOngoing',
            }]}
            filterCallback={(item) => {
              return item.openingStart.seconds < nowSeconds
            }}
            sortCompare={(a, b) => {
              return b.openingStart.seconds - a.openingStart.seconds
            }}
            translateId={'ongoing'}
          />
          <EventsList
            navigation={this.props.navigation}
            firestoreQuery={[{
              collection: 'events',
              orderBy: ['openingStart', 'asc'],
              where: [
                ['status', '==', 'published'],
                ['type', '==', 'exhibition'],
                ['openingStart', '>=', nowDate()],
              ],
              storeAs: 'calendarExhibitionsUpcoming',
            }]}
            translateId={'upcoming'}
          />
          <OptionButton
            translateId={'viewExhibitionArchive'}
            handlePress={this._handleArchivePress}
            arrow
          />
        </ScrollView>
      </View>
    );
  }
}
