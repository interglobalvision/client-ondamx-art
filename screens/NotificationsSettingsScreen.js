import React, { Component } from 'react'
import { compose } from 'redux'
import OptionButton from '../components/OptionButton'
import {
  Text,
  TouchableOpacity,
  View,
  FlatList,
  Alert,
  StatusBar,
} from 'react-native'

import Layout from '../constants/Layout'

export default class NotificationsSettingsScreen extends Component {
  render() {
    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <Text>Notifications Settings</Text>
      </View>
    );
  }
}
