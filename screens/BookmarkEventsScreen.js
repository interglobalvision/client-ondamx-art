import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import {
  Image,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native'
import Layout from '../constants/Layout'
import BookmarkList from '../components/BookmarkList'
import EventsItemHalf from '../components/EventsItemHalf'
import UserOnlyFeatureNotice from '../components/UserOnlyFeatureNotice'

class BookmarkEventsScreen extends Component {
  render() {
    const { uid, navigation } = this.props

    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <ScrollView style={Layout.container}
          contentContainerStyle={{
            paddingBottom: Layout.marginBasic * 2,
            paddingTop: Layout.marginBasic,
            minHeight: '100%',
          }}
        >
          {uid !== undefined ? (
            <BookmarkList
              navigation={navigation}
              collection={'events'}
              ItemComponent={EventsItemHalf}
              navTitle={'Event'}
              emptyTranslateId={'emptyBookmarkEvents'}
            />
          ) : (
            <UserOnlyFeatureNotice />
          )}
        </ScrollView>
      </View>
    );
  }
}

export default compose(
  connect((state) => ({ uid: state.firebase.auth.uid })),
)(BookmarkEventsScreen)
