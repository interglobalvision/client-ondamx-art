import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import {
  Image,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native'
import Layout from '../constants/Layout'
import BookmarkList from '../components/BookmarkList'
import ArticlesListItem from '../components/ArticlesListItem'
import UserOnlyFeatureNotice from '../components/UserOnlyFeatureNotice'

class BookmarkArticlesScreen extends Component {
  render() {
    const { uid, navigation } = this.props

    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <ScrollView style={Layout.container}
          contentContainerStyle={{
            paddingBottom: Layout.marginBasic * 2,
            paddingTop: Layout.marginBasic,
            minHeight: '100%',
          }}
        >
          {uid !== undefined ? (
            <BookmarkList
              navigation={navigation}
              collection={'articles'}
              ItemComponent={ArticlesListItem}
              navTitle={'Article'}
              emptyTranslateId={'emptyBookmarkArticles'}
            />
          ) : (
            <UserOnlyFeatureNotice />
          )}
        </ScrollView>
      </View>
    );
  }
}

export default compose(
  connect((state) => ({ uid: state.firebase.auth.uid })),
)(BookmarkArticlesScreen)
