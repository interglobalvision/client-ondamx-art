import React from 'react'
import {
	View,
  Text,
} from 'react-native'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firesbaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'

class AuthLoadingScreen extends React.Component {
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { auth, navigation } = this.props

    if (isEmpty(auth)) {
      navigation.navigate('Auth')
      return null
    }

    else {
      navigation.navigate('Main')
      return null
    }
  }

  render() {
    return null
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth
  }
}

export default compose(
  connect(mapStateToProps)
)(AuthLoadingScreen)
