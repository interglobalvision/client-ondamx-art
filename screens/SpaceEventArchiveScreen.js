import React, { Component } from 'react'
import {
  ScrollView,
  View,
} from 'react-native'
import Layout from '../constants/Layout'
import EventsList from '../components/EventsList'

export default class SpaceEventArchiveScreen extends Component {
  render() {
    const { navigation } = this.props
    const spaceId = navigation.getParam('id')

    return (
      <View style={Layout.container}>
        <ScrollView
          style={Layout.container}
        >
          <View
            style={{
              paddingTop: Layout.marginBasic,
              paddingBottom: Layout.marginBasic * 2,
            }}
          >
            <EventsList
              navigation={navigation}
              firestoreQuery={[{
                collection: 'events',
                storeAs: `spaceEvents-${spaceId}`,
                where: [
                  ['status', '==', 'published'],
                  ['space.id', '==', spaceId],
                ],
                orderBy: ['openingStart', 'desc']
              }]}
            />
          </View>
        </ScrollView>
      </View>
    )
  }
}
