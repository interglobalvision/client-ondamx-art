import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { withCurrentLanguage, flattenLocalizedContent, withTranslate } from '../lib/translations'
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Button,
  StatusBar,
  Linking,
} from 'react-native'
import { Translate } from 'react-localize-redux'
import ShareButton from '../components/ShareButton'
import BookmarkButton from '../components/BookmarkButton'
import TextContent from '../components/TextContent'
import CoverImage from '../components/CoverImage'
import ImageCarousel from '../components/ImageCarousel'
import SpaceDetails from '../components/SpaceDetails'
import RelatedExternalCarousel from '../components/RelatedExternalCarousel'
import RelatedInternalList from '../components/RelatedInternalList'
import { formatEventDates } from '../lib/dates'

import Layout from '../constants/Layout'
import { getStaticMapUrl, openMapsApp } from '../lib/geoloc'

class EventScreen extends Component {
  returnCoverContent = (content) => {
    const { currentLanguage } = this.props

    if (content.imageCarousel.length) {
      return (
        <ImageCarousel content={content} currentLanguage={currentLanguage} />
      )
    } else if (content.coverImage) {
      return (
        <CoverImage media={content.coverImage} currentLanguage={currentLanguage} />
      )
    }
    return null
  }

  handleSpacePress = () => {
    const { navigation, item } = this.props
    navigation.push('Space', { id: item.space.id })
  }

  render() {
    const { item, currentLanguage, navigation, translate } = this.props
    const itemId = navigation.getParam('id')

    if (!isLoaded(item)) {
      return null
    }

    else if (isEmpty(item)) {
      return null
    }

    const content = flattenLocalizedContent(item, currentLanguage)
    const { location } = content
    const { lat, lon } = location
    const mapSize = 100
    let mapUrl = false
    if (location.address !== undefined) {
      if (location.address.street.length) {
        mapUrl = getStaticMapUrl(lat, lon, mapSize * 1.5, mapSize * 1.5)
      }
    }

    const isExpo = content.type === 'exhibition'
    const isMultiday = content.multiDay === true

    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <ScrollView style={Layout.container}>
          <View style={{
            backgroundColor: Layout.colors.white
          }}>
            { this.returnCoverContent(content) }
            <View style={{
              paddingVertical: Layout.marginBasic * 2
            }}>
              <View style={{
                paddingHorizontal: Layout.marginBasic * 2
              }}>
                <View style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                  paddingBottom: Layout.marginBasic,
                  alignItems: 'center',
                }}>
                  <View style={{
                    marginRight: Layout.marginBasic * 2,
                  }}>
                    <ShareButton
                      slug={item.slug}
                      title={content.name}
                      itemId={itemId}
                      type={'evento'}
                    />
                  </View>
                  <View>
                    <BookmarkButton
                      itemId={itemId}
                      itemCollection={'events'}
                      itemBookmarks={item.bookmarks}
                    />
                  </View>
                </View>
                <View style={{
                  marginBottom: Layout.marginBasic * 2
                }}>
                  <Text style={Layout.fontStyles.event.single.name}>{content.name}</Text>
                </View>
                {content.title !== '' &&
                  <View style={{
                    marginBottom: Layout.marginBasic * .5
                  }}>
                    <Text style={Layout.fontStyles.event.single.title}>{content.title}</Text>
                  </View>
                }
                <View style={{
                  marginBottom: Layout.marginBasic
                }}>
                  <Text style={Layout.fontStyles.event.single.type}>{translate(content.type)}</Text>
                </View>
                <View style={{
                  marginBottom: Layout.marginBasic
                }}>
                  {formatEventDates(content, currentLanguage.code, translate, isMultiday || isExpo)}
                </View>
                <View style={{
                  flexDirection: 'row',
                  marginBottom: Layout.marginBasic,
                }}>
                  <View style={{
                    flex: 1,
                  }}>
                    <View style={{
                      marginBottom: Layout.marginBasic * .5
                    }}>
                      {content.space.name !== '' && content.space.id !== undefined &&
                        <TouchableOpacity onPress={this.handleSpacePress}>
                          <Text style={Layout.fontStyles.event.single.spaceLinked}>{content.space.name}</Text>
                        </TouchableOpacity>
                      }
                      {content.space.name !== '' && content.space.id === undefined &&
                        <Text style={Layout.fontStyles.event.single.space}>{content.space.name}</Text>
                      }
                    </View>
                    <View style={{
                      flexDirection: 'row',
                      flexWrap: 'nowrap',
                      flex: 1,
                    }}>
                      <View style={{
                        flex: 1,
                        paddingRight: Layout.marginBasic * 2,
                      }}>
                        <SpaceDetails
                          spaceContent={content}
                          spaceName={content.space.name}
                          showAddress
                          showHours
                          translate={translate}
                        />
                      </View>
                      {mapUrl &&
                        <View style={{
                          ...Layout.outerCard,
                          width: mapSize,
                          height: mapSize,
                        }}>
                          <View style={Layout.innerCard}>
                            <TouchableOpacity onPress={() => { openMapsApp(lat, lon, content.space.name, translate) }}>
                              <Image style={{
                                width: mapSize,
                                height: mapSize,
                              }} source={{ uri: mapUrl }} />
                            </TouchableOpacity>
                          </View>
                        </View>
                      }
                    </View>
                    {content.ticketUrl.length > 0 &&
                      <View style={{
                        paddingTop: Layout.marginBasic
                      }}>
                        <Text style={Layout.fontStyles.event.single.buyTickets} onPress={() => {
                          Linking.openURL(content.ticketUrl)
                        }}><Translate id='buyTickets' /></Text>
                      </View>
                    }
                  </View>
                </View>
              </View>
              <TextContent content={content.mainContent} />
            </View>
          </View>
          <View style={{
            paddingBottom: Layout.marginBasic * 2,
          }}>
            <RelatedInternalList
              docId={itemId}
              collection={'articles'}
              translateId={'relatedArticles'}
              navigation={navigation}
            />
            <RelatedInternalList
              docId={itemId}
              collection={'events'}
              translateId={'relatedEvents'}
              navigation={navigation}
            />
            {content.relatedExternal.length > 0 &&
              <RelatedExternalCarousel items={content.relatedExternal} />
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state, props) => {
  // Get the event id from the navigation. Kinda
  // like getting it from a URL param [;
  const docId = props.navigation.getParam('id')
  return {
    item: state.firestore.data[`event-${docId}`]
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // toggleTestValue: () => dispatch(toggleTestValue())
  }
}

export default compose(
  withCurrentLanguage,
  withTranslate,
  firestoreConnect( props => {
    // Get the document id from the navigation. Kinda
    // like getting it from a URL param [;
    const docId = props.navigation.getParam('id')

    // Fetch the docuement
    return [{
      collection: 'events',
      doc: docId,
      storeAs: `event-${docId}`
    }]
  }),
  connect(mapStateToProps, mapDispatchToProps)
)(EventScreen)

/*<SpaceDetails
  spaceContent={content}
  spaceName={content.space.name}
  showAddress
  showHours
  translate={translate}
/>*/
