import React, { Component } from 'react'
import { compose } from 'redux'
import {
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
  StatusBar
} from 'react-native';

import { withTranslate } from '../lib/translations'

import EmailLoginForm from '../components/EmailLoginForm'

import Layout from '../constants/Layout'

class InitialAuthScreen extends Component {

  render() {
    const { translate } = this.props
    const showSocial = false

    return (
      <KeyboardAvoidingView
        style={{
          ...Layout.container,
          backgroundColor: Layout.colors.ondaBlue,
        }}
        behavior={'height'}
      >
        <StatusBar hidden={true} />
        <ScrollView
          keyboardShouldPersistTaps='handled'
          keyboardDismissMode='on-drag'
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >

            <View style={{
              maxWidth: 400,
              width: '70%',
            }}>

              <View style={{
                paddingBottom: Layout.marginBasic * 3,
                alignItems: 'center',
              }}>
                <Image style={{width: '80%', maxWidth: 300}} source={require('../assets/images/ondaLogoWhite.png')} resizeMode='contain' />
              </View>
{/*
              <View style={{
                marginBottom: Layout.marginBasic * 3,
                flexDirection: 'row',
                flexWrap: 'wrap'
              }}>

                <View style={{
                  marginBottom: Layout.marginBasic,
                  width: '100%'
                }}>
                  <Text style={{
                    color: Layout.colors.white,
                    textAlign: 'center',
                  }}>{translate('continueWith')}</Text>
                </View>

                <View style={{
                  width: '50%',
                  paddingRight: Layout.marginBasic * .5
                }}>
                  <TouchableOpacity
                    onPress={() => { console.log('Google') }}
                    style={Layout.socialLoginButton}
                  >
                    <Image style={{width: 20, height: 20}} source={require('../assets/images/loginGoogle.png')} resizeMode='contain' />
                    <View style={{flex: 1, paddingTop: 2}}>
                      <Text
                        style={{
                          color: Layout.colors.white,
                          textAlign: 'center'
                        }}
                      >
                        Google
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>

                <View style={{
                  width: '50%',
                  paddingLeft: Layout.marginBasic * .5
                }}>
                  <TouchableOpacity
                    onPress={() => { console.log('Facebook') }}
                    style={Layout.socialLoginButton}
                  >
                    <Image style={{width: 20, height: 20}} source={require('../assets/images/loginFacebook.png')} resizeMode='contain' />
                    <View style={{flex: 1, paddingTop: 2}}>
                      <Text
                        style={{
                          color: Layout.colors.white,
                          textAlign: 'center'
                        }}
                      >
                        Facebook
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>

              </View>

              <View style={{
                marginBottom: Layout.marginBasic * 3,
                flex: 1,
                flexDirection: 'row',
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
                <View style={{
                  width: '30%',
                  height: 1,
                  backgroundColor: Layout.colors.white,
                }}></View>
                <View style={{
                  flex: 0,
                  paddingHorizontal: Layout.marginBasic,
                }}>
                  <Text style={{
                    color: Layout.colors.white,
                    fontSize: Layout.fontSizes.smaller
                  }}>{translate('or')}</Text>
                </View>
                <View style={{
                  width: '30%',
                  height: 1,
                  backgroundColor: Layout.colors.white,
                }}></View>
              </View>
*/}
              <EmailLoginForm navigation={this.props.navigation}/>

              <View>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    marginBottom: Layout.marginBasic * 3,
                  }}
                  onPress={() => this.props.navigation.push('EmailCreateUserScreen')}
                >
                  <Text style={{
                    color: Layout.colors.white,
                  }}>{translate('dontHaveAnAccount')} </Text>
                  <Text style={{
                    color: Layout.colors.white,
                    fontFamily: 'medium',
                  }}>{translate('signUp')}</Text>
                </TouchableOpacity>
              </View>

              <View>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                  }}
                  onPress={() => this.props.navigation.navigate('Main')}
                >
                  <Text style={{
                    color: Layout.colors.white,
                    fontFamily: 'medium',
                    fontSize: Layout.fontSizes.smaller,
                  }}>{translate('continueWithoutAccount')}</Text>
                </TouchableOpacity>
              </View>

            </View>

        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

export default compose(
  withTranslate
)(InitialAuthScreen)
