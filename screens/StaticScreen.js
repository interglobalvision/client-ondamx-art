import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { withCurrentLanguage, flattenLocalizedContent } from '../lib/translations'
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native'
import Layout from '../constants/Layout'
import ArticleContent from '../components/ArticleContent'

class StaticScreen extends Component {
  render() {
    const { navigation, page } = this.props

    if (!isLoaded(page)) {
      return null
    }

    else if (isEmpty(page)) {
      return null
    }

    const content = flattenLocalizedContent(page, this.props.currentLanguage)

    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <ScrollView
          style={Layout.container}
          contentContainerStyle={{
            paddingTop: Layout.marginBasic * 2,
          }}
        >
          <ArticleContent content={content.mainContent} />
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state, props) => {
  const docId = props.navigation.getParam('id')

  return {
    page: state.firestore.data[`page-${docId}`],
  }
}

export default compose(
  withCurrentLanguage,
  firestoreConnect((props) => {
    const docId = props.navigation.getParam('id')

    return [{
      collection: 'pages',
      doc: docId,
      storeAs: `page-${docId}`,
    }]
  }),
  connect(mapStateToProps)
)(StaticScreen)
