import React, { Component } from 'react'
import {
  ScrollView,
  View,
} from 'react-native'
import Layout from '../constants/Layout'
import EventsList from '../components/EventsList'

export default class EventArchiveScreen extends Component {
  render() {
    const { navigation } = this.props

    return (
      <View style={Layout.container}>
        <ScrollView
          style={Layout.container}
        >
          <View
            style={{
              paddingTop: Layout.marginBasic,
              paddingBottom: Layout.marginBasic * 2,
            }}
          >
            <EventsList
              navigation={navigation}
              firestoreQuery={[{
                collection: 'events',
                storeAs: 'events',
                where: ['status', '==', 'published'],
                orderBy: ['openingStart', 'desc']
              }]}
              filterCallback={(item) => {
                return item.type !== 'exhibition'
              }}
            />
          </View>
        </ScrollView>
      </View>
    )
  }
}
