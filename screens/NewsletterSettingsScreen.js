import React, { Component } from 'react'
import { compose } from 'redux'
import { withLocalize } from 'react-localize-redux'
import { withTranslate, withCurrentLanguage } from '../lib/translations'
import OptionButton from '../components/OptionButton'
import {
  Text,
  TouchableOpacity,
  View,
  FlatList,
  Alert,
  StatusBar,
} from 'react-native'

import Layout from '../constants/Layout'

export default class NewsletterSettingsScreen extends Component {
  render() {
    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <Text>Newsletter Settings</Text>
      </View>
    );
  }
}
