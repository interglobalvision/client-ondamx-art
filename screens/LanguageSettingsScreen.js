import React, { Component } from 'react'
import { compose } from 'redux'
import { withLocalize } from 'react-localize-redux'
import { withTranslate, withCurrentLanguage } from '../lib/translations'
import OptionButton from '../components/OptionButton'
import {
  Text,
  TouchableOpacity,
  View,
  FlatList,
  Alert,
  StatusBar,
} from 'react-native'

import Layout from '../constants/Layout'

class LanguageSettingsScreen extends Component {
  _handlePress = (data) => {
    const { translate, currentLanguage, setActiveLanguage, navigation } = this.props
    const { languageKey } = data

    if (languageKey !== currentLanguage) {
      Alert.alert(
        translate('changeLanguage'),
        null,
        [
          {
            text: translate('cancel'),
            style: 'cancel',
          },
          {
            text: translate('change'),
            onPress: () => {
              // Change language
              setActiveLanguage(languageKey)
              // Return to Settings
              navigation.goBack()
            }
          },
        ]
      );
    } else {
      // Same language selected
      // Return to Settings
      navigation.goBack()
    }
  }

  render() {
    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <FlatList
          data={[
            {key: 'en', label: 'English'},
            {key: 'es', label: 'Español'}
          ]}
          renderItem={({item}) => (
            <OptionButton handlePress={this._handlePress} title={item.label} data={{ languageKey: item.key }}/>
          )}
        />
      </View>
    );
  }
}

export default compose(
  withLocalize,
  withCurrentLanguage,
  withTranslate,
)(LanguageSettingsScreen)
