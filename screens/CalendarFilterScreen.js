import React from 'react'
import { connect } from 'react-redux'
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native'
import Layout from '../constants/Layout'
import EventsList from '../components/EventsList'
import EventsFilterBar from '../components/EventsFilterBar'
import { calendarFilters } from '../lib/filters'

export default class CalendarFilterScreen extends React.Component {
  constructor(props) {
    super(props)

    const activeId = props.navigation.getParam('filterId')

    this.state = {
      activeId
    }
  }

  updateFilterId(id) {
    this.setState({
      activeId: id
    })
  }

  render() {
    const { navigation } = this.props
    const activeFilter = calendarFilters.find(f => f.filterId === this.state.activeId)

    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <ScrollView style={Layout.container}
          contentContainerStyle={{
            paddingBottom: Layout.marginBasic * 2
          }}
        >
          <EventsFilterBar navigation={navigation} updateActiveFilter={this.updateFilterId.bind(this)} activeId={activeFilter.filterId} />
          <EventsList
            navigation={navigation}
            firestoreQuery={[activeFilter.firestoreQuery]}
            translateId={'exhibitionsAndEvents'}
            filterCallback={activeFilter.filterCallback ? activeFilter.filterCallback : null}
            sortCompare={activeFilter.sortCompare ? activeFilter.sortCompare : null}
            filterId={activeFilter.filterId}
          />
        </ScrollView>
      </View>
    );
  }
}
