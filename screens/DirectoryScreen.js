import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native'
import Layout from '../constants/Layout'
import AtoZListView from '../components/AtoZListView'
import FilterBar from '../components/FilterBar'
import { directoryFilters } from '../lib/filters'

export default class DirectoryScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      activeId: directoryFilters[0].filterId
    }
  }

  _updateFilterId = (id) => {
    this.setState({
      activeId: id
    })
  }

  render() {
    const { navigation } = this.props
    const activeFilter = directoryFilters.find(f => f.filterId === this.state.activeId)

    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <FilterBar filters={directoryFilters} activeId={activeFilter.filterId} updateActiveFilter={this._updateFilterId} />
        <AtoZListView navigation={navigation} firestoreQuery={{
          storeAs: 'directoryFiltered',
          ...activeFilter.firestoreQuery
        }}/>
      </View>
    );
  }
}
