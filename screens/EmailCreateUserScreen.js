import React from 'react'
import {
  Platform,
  ScrollView,
  View,
  Text,
  KeyboardAvoidingView,
  StatusBar,
} from 'react-native';

import Layout from '../constants/Layout'
import EmailCreateUserForm from '../components/EmailCreateUserForm'

export default class EmailAuthScreen extends React.Component {
  render() {
    return (
      <View
        style={{
          ...Layout.container,
          backgroundColor: Layout.colors.white,
        }}
      >
        <StatusBar hidden={false} />
        <ScrollView
          contentContainerStyle={{
            alignItems: 'center',
          }}
          keyboardShouldPersistTaps='handled'
          keyboardDismissMode='on-drag'
        >
          <View style={{
            paddingVertical: Layout.marginBasic * 3,
            maxWidth: 400,
            width: '70%',
          }}>
            <EmailCreateUserForm navigation={this.props.navigation}/>
          </View>
{/*
          <View style={{
            paddingBottom: Layout.marginBasic * 3,
            maxWidth: 400,
            width: '100%',
            paddingHorizontal: Layout.marginBasic * 2
          }}>
            <Text style={Layout.fontStyles.finePrint}>
              Though we assume the latter, alike watchmakers show us how vases can be lows. Nowhere is it disputed that the cafes could be said to resemble parotid bottoms. In ancient times we can assume that any instance of a rooster can be construed as a prostrate humor. The zeitgeist contends that few can name a roomy opera that isn't a snowlike rowboat.
            </Text>
          </View>
*/}
        </ScrollView>
      </View>
    );
  }
}
