import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native'
import Layout from '../constants/Layout'
import EventsCarousel from '../components/EventsCarousel'
import EventsList from '../components/EventsList'
import EventsFilterBar from '../components/EventsFilterBar'
import startOfToday from 'date-fns/start_of_today'
import endOfToday from 'date-fns/end_of_today'

class CalendarExploreScreen extends React.Component {
  render() {
    const { bookmarkedSpaces, navigation, uid } = this.props
    let bookmarkedSpacesIds = []

    if (isLoaded(bookmarkedSpaces) && !isEmpty(bookmarkedSpaces)) {
      bookmarkedSpacesIds = bookmarkedSpaces.map(item => item.id)
    }

    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <ScrollView style={Layout.container}>
          <EventsList
            navigation={navigation}
            firestoreQuery={[{
              collection: 'events',
              orderBy: ['openingStart', 'desc'],
              where: [
                ['status', '==', 'published'],
                ['highlight', '==', 'featured']
              ],
              storeAs: 'calendarExploreFeatured',
            }, {
              collection: 'events',
              orderBy: ['openingStart', 'desc'],
              where: [
                ['status', '==', 'published'],
                ['highlight', '==', 'highlighted']
              ],
              storeAs: 'calendarExploreHighlights',
            }]}
            sortCompare={(a, b) => {
              // openingStart ASC
              return a.openingStart.seconds - b.openingStart.seconds
            }}
            translateId={'highlights'}
            fullItem={true}
          />
          {bookmarkedSpacesIds.length > 0 &&
            <EventsCarousel
              navigation={navigation}
              firestoreQuery={[{
                collection: 'events',
                orderBy: ['closing', 'asc'],
                where: [
                  ['status', '==', 'published'],
                  ['highlight', '==', 'none'],
                  ['closing', '>', new Date()],
                ],
                storeAs: 'calendarExploreRecommended',
                limit: 10,
              }]}
              filterCallback={(item) => {
                if (item.space.id === undefined || item.space.id === '') {
                  return false
                }
                return bookmarkedSpacesIds.includes(item.space.id)
              }}
              translateId={'recommended'}
            />
          }
          <EventsFilterBar navigation={this.props.navigation} translateId={'filterBy'} />
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state, props) => {
  const storeAs = props.uid ? 'bookmarks-spaces' : 'spaces'
  return {
    bookmarkedSpaces: state.firestore.ordered[storeAs],
  }
}

export default compose(
  connect((state) => ({ uid: state.firebase.auth.uid })),
  firestoreConnect((props) => {
    const { uid } = props
    return [{
      collection: 'spaces',
      where: uid ? ['bookmarks', 'array-contains', uid] : null,
      storeAs: uid ? 'bookmarks-spaces' : 'spaces',
    }]
  }),
  connect(mapStateToProps)
)(CalendarExploreScreen)
