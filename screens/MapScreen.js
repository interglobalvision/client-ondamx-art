import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native'
import Layout from '../constants/Layout'
import FilterBar from '../components/FilterBar'
import MapContainer from '../components/MapContainer'
import { mapFilters } from '../lib/filters'

export default class MapScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeId: mapFilters[0].filterId,
    }
  }

  _updateFilterId = (id) => {
    this.setState({
      activeId: id,
    })
  }

  render() {
    const { navigation } = this.props
    const activeFilter = mapFilters.find(f => f.filterId === this.state.activeId)

    return (
      <View style={{
        flex: 1
      }}>
        <StatusBar hidden={false} />
        <FilterBar filters={mapFilters} activeId={activeFilter.filterId} updateActiveFilter={this._updateFilterId.bind(this)} />
        <MapContainer navigation={navigation} activeFilter={activeFilter} />
      </View>
    )
  }
}
