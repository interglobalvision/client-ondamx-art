import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native'
import Layout from '../constants/Layout'
import NotificationList from '../components/NotificationList'
import NotificationItem from '../components/NotificationItem'
import UserOnlyFeatureNotice from '../components/UserOnlyFeatureNotice'

class NotificationsScreen extends Component {
  render() {
    const { navigation, uid } = this.props

    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <ScrollView style={Layout.container}
          contentContainerStyle={{
            paddingBottom: Layout.marginBasic * 2,
            paddingTop: Layout.marginBasic,
            minHeight: '100%',
          }}
        >
          {uid !== undefined ? (
            <NotificationList navigation={navigation} uid={uid}/>
          ) : (
            <UserOnlyFeatureNotice />
          )}
        </ScrollView>
      </View>
    );
  }
}

export default compose(
  connect((state) => ({ uid: state.firebase.auth.uid })),
)(NotificationsScreen)
