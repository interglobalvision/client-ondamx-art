import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { withCurrentLanguage, flattenLocalizedContent, withTranslate } from '../lib/translations'
import startOfToday from 'date-fns/start_of_today'
import endOfToday from 'date-fns/end_of_today'

import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Button,
  StatusBar,
} from 'react-native'

import BookmarkButton from '../components/BookmarkButton'
import TextContent from '../components/TextContent'
import SpaceDetails from '../components/SpaceDetails'
import Layout from '../constants/Layout'
import CoverImage from '../components/CoverImage'
import RelatedExternalCarousel from '../components/RelatedExternalCarousel'
import EventsList from '../components/EventsList'
import OptionButton from '../components/OptionButton'
import RelatedInternalList from '../components/RelatedInternalList'

import MapView, { Marker, Callout } from 'react-native-maps'
import { openMapsApp } from '../lib/geoloc'

class SpaceScreen extends React.Component {
  _handleArchivePress = () => {
    const { navigation } = this.props
    const id = navigation.getParam('id')
    navigation.push('SpaceEventArchive', { id })
  }

  render() {
    const { item, spaceEvents, currentLanguage, navigation, translate } = this.props
    const itemId = navigation.getParam('id')

    if (!isLoaded(item)) {
      return null
    }

    else if (isEmpty(item)) {
      return null
    }

    const content = flattenLocalizedContent(item, currentLanguage)
    const { location } = content
    const { lat, lon } = location
    let showMap = false
    if (location.address !== undefined && lat !== 0 && lon !== 0) {
      if (location.address.street.length) {
        showMap = true
      }
    }

    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <ScrollView style={Layout.container}>
          {content.coverImage &&
            <CoverImage media={content.coverImage} currentLanguage={currentLanguage} />
          }
          <View style={{
            paddingVertical: Layout.marginBasic * 2,
            backgroundColor: Layout.colors.white,
          }}>
            <View style={{
              paddingHorizontal: Layout.marginBasic * 2,
            }}>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                paddingBottom: Layout.marginBasic,
              }}>
                <View>
                  <BookmarkButton
                    itemId={itemId}
                    itemCollection={'spaces'}
                    itemBookmarks={item.bookmarks}
                  />
                </View>
              </View>
              <Text style={Layout.fontStyles.space.single.name}>{content.name}</Text>
              <View style={{
                marginBottom: Layout.marginBasic * 2
              }}>
                <Text style={Layout.fontStyles.space.single.type}>{translate(content.type)}</Text>
              </View>
              <SpaceDetails
                spaceContent={content}
                spaceName={content.name}
                showAddress
                showHours
                showPhone
                showWebsite
                showEmail
                showInstagram
                translate={translate}
              />
            </View>
            <TextContent content={content.mainContent} />
          </View>
          {showMap &&
            <View>
              <TouchableOpacity style={{
                width: '100%',
                height: Layout.window.width * .7,
                ...Layout.shadow,
              }} onPress={() => { openMapsApp(lat, lon, content.name, translate) }}>
                <MapView
                  style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                  }}
                  ref={ref => { this.map = ref }}
                  initialRegion={{
                    latitude: lat,
                    longitude: lon,
                    latitudeDelta: 0.007,
                    longitudeDelta: 0.01,
                  }}
                  onRegionChange={this._handleRegionChange}
                  showsUserLocation={true}
                  userLocationAnnotationTitle={''}
                  showsMyLocationButton={true}
                  followsUserLocation={false}
                  zoomEnabled={false}
                  zoomTapEnabled={false}
                  zoomControlEnabled={false}
                  scrollEnabled={false}
                  pitchEnabled={false}
                  toolbarEnabled={false}
                  moveOnMarkerPress={false}
                  cacheEnabled={false}
                  loadingEnabled={true}
                >
                  <Marker
                    coordinate={{
                      latitude: lat,
                      longitude: lon
                    }}
                    title={''}
                    pinColor={Layout.colors.black}
                    style={{ zIndex: 1 }}
                  >
                    <Callout tooltip={true} />
                  </Marker>
                </MapView>
                <View style={{
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0,
                }}></View>
              </TouchableOpacity>
            </View>
          }
          <View>
            <EventsList
              navigation={navigation}
              firestoreQuery={[{
                collection: 'events',
                orderBy: ['closing', 'asc'],
                where: [
                  ['status', '==', 'published'],
                  ['type', '==', 'exhibition'],
                  ['closing', '>=', endOfToday()],
                  ['space.id', '==', itemId],
                ],
                storeAs: `spaceOngoingExhibitions-${itemId}`,
              }]}
              filterCallback={(item) => {
                return item.openingStart.seconds < (endOfToday().getTime() / 1000)
              }}
              sortCompare={(a, b) => {
                return b.openingStart.seconds - a.openingStart.seconds
              }}
              translateId={'ongoingExhibitions'}
            />
            <EventsList
              navigation={navigation}
              firestoreQuery={[{
                collection: 'events',
                orderBy: ['openingStart', 'asc'],
                where: [
                  ['status', '==', 'published'],
                  ['openingStart', '>=', startOfToday()],
                  ['space.id', '==', itemId],
                ],
                storeAs: `spaceUpcomingEvents-${itemId}`,
              }]}
              translateId={'upcomingEvents'}
            />
            <EventsList
              navigation={navigation}
              firestoreQuery={[{
                collection: 'events',
                orderBy: ['closing', 'desc'],
                where: [
                  ['status', '==', 'published'],
                  ['closing', '<', startOfToday()],
                  ['space.id', '==', itemId],
                ],
                storeAs: `spaceRecentEvents-${itemId}`,
                limit: 2
              }]}
              translateId={'recentEvents'}
            />
            {isLoaded(spaceEvents) && !isEmpty(spaceEvents) &&
              <OptionButton
                translateId={'viewEventExhibitionArchive'}
                handlePress={this._handleArchivePress}
                arrow
              />
            }
          </View>
          <View style={{
            paddingBottom: Layout.marginBasic * 2,
          }}>
            <RelatedInternalList
              docId={itemId}
              collection={'articles'}
              translateId={'relatedArticles'}
              navigation={navigation}
            />
            {content.relatedExternal.length > 0 &&
              <RelatedExternalCarousel items={content.relatedExternal} />
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state, props) => {
  const docId = props.navigation.getParam('id')
  return {
    item: state.firestore.data[`space-${docId}`],
    spaceEvents: state.firestore.data[`spaceEvents-${docId}`],
  }
}

export default compose(
  withCurrentLanguage,
  withTranslate,
  firestoreConnect( props => {
    const docId = props.navigation.getParam('id')
    return [
      {
        collection: 'spaces',
        doc: docId,
        storeAs: `space-${docId}`
      },
      {
        collection: 'events',
        storeAs: `spaceEvents-${docId}`,
        where: [
          ['status', '==', 'published'],
          ['space.id', '==', docId],
        ],
        orderBy: ['openingStart', 'desc']
      }
    ]
  }),
  connect(mapStateToProps)
)(SpaceScreen)
