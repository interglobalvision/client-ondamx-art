import React from 'react'
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native'
import Layout from '../constants/Layout'
import EventsList from '../components/EventsList'
import OptionButton from '../components/OptionButton'
import startOfToday from 'date-fns/start_of_today'
import endOfToday from 'date-fns/end_of_today'
import startOfTomorrow from 'date-fns/start_of_tomorrow'
import endOfTomorrow from 'date-fns/end_of_tomorrow'

export default class CalendarEventsScreen extends React.Component {
  _handleArchivePress = () => {
    const { navigation } = this.props
    navigation.push('EventArchive')
  }

  render() {
    return (
      <View style={Layout.container}>
        <StatusBar hidden={false} />
        <ScrollView style={Layout.container}
          contentContainerStyle={{
            paddingBottom: Layout.marginBasic
          }}
        >
          <EventsList
            navigation={this.props.navigation}
            firestoreQuery={[
              {
                collection: 'events',
                orderBy: ['openingStart', 'asc'],
                where: [
                  ['status', '==', 'published'],
                  ['openingStart', '>=', startOfToday()],
                  ['openingStart', '<=', endOfToday()],
                ],
                storeAs: 'calendarEventsToday',
              },
              {
                collection: 'events',
                orderBy: ['closing', 'asc'],
                where: [
                  ['status', '==', 'published'],
                  ['closing', '>=', startOfToday()],
                  ['multiDay', '==', true]
                ],
                storeAs: 'calendarEventsTodayMultiday',
              },
            ]}
            translateId={'today'}
            filterCallback={(item) => {
              return (item.type !== 'exhibition' || item.hasOpening) && item.openingStart.seconds < endOfToday().getTime() / 1000
            }}
          />
          <EventsList
            navigation={this.props.navigation}
            firestoreQuery={[
              {
                collection: 'events',
                orderBy: ['openingStart', 'asc'],
                where: [
                  ['status', '==', 'published'],
                  ['openingStart', '>=', startOfTomorrow()],
                  ['openingStart', '<=', endOfTomorrow()],
                ],
                storeAs: 'calendarEventsTomorrow',
              },
              {
                collection: 'events',
                orderBy: ['closing', 'asc'],
                where: [
                  ['status', '==', 'published'],
                  ['closing', '>=', startOfTomorrow()],
                  ['multiDay', '==', true]
                ],
                storeAs: 'calendarEventsTomorrowMultiday',
              },
            ]}
            translateId={'tomorrow'}
            filterCallback={(item) => {
              return (item.type !== 'exhibition' || item.hasOpening) && item.openingStart.seconds < endOfTomorrow().getTime() / 1000
            }}
          />
          <EventsList
            navigation={this.props.navigation}
            firestoreQuery={[{
              collection: 'events',
              orderBy: ['openingStart', 'asc'],
              where: [
                ['status', '==', 'published'],
                ['openingStart', '>=', endOfTomorrow()],
              ],
              storeAs: 'calendarEventsUpcoming',
            }]}
            filterCallback={(item) => {
              return item.type !== 'exhibition' || item.hasOpening
            }}
            translateId={'upcoming'}
          />
          <OptionButton
            translateId={'viewEventArchive'}
            handlePress={this._handleArchivePress}
            arrow
          />
        </ScrollView>
      </View>
    );
  }
}
