import React from 'react'
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Button,
  Animated,
  StatusBar,
} from 'react-native'

import { Translate } from 'react-localize-redux'

import HomeFeatured from '../components/HomeFeatured'
import EventsCarousel from '../components/EventsCarousel'
import HomeHighlights from '../components/HomeHighlights'
import BannerAd from '../components/BannerAd'
import HomeArticlesList from '../components/HomeArticlesList'
import SearchForm from '../components/SearchForm'

import Layout from '../constants/Layout'
import { nowSeconds, nowDate, oneWeekFromNowSeconds, nowTimestamp } from '../lib/dates'
import { hasNotch } from '../lib/utils'
import endOfToday from 'date-fns/end_of_today'

export default class HomeScreen extends React.Component {
  searchOpacity = new Animated.Value(0)

  constructor(props) {
    super(props)
    this.state = {
      searchActive: false,
      isAnimatingSearch: false,
    }
  }

  toggleSearch = () => {
    const { searchActive, isAnimatingSearch } = this.state

    Animated.spring(this.searchOpacity, {
      toValue: searchActive ? 0 : 1,
      duration: 100,
    }).start(() => {
      if (!searchActive) {
        this.focusInput()
      }
      this.setState(prevState => ({
        searchActive: !prevState.searchActive,
      }))
    })
  }

  render() {
    const { navigation } = this.props
    const { searchActive, isAnimatingSearch } = this.state

    const timeNow = nowTimestamp()

    return (
      <View style={{
        ...Layout.container,
        paddingTop: hasNotch() ? Layout.marginBasic * 2 : 0,
      }}>
        <StatusBar hidden={false} />

        <ScrollView style={Layout.container}>
          <View style={{
            marginTop: Layout.marginBasic * 4,
          }}>
            <View style={Layout.search.outerCard}>
              <View style={Layout.innerCard}>
                <TouchableOpacity
                  activeOpacity={1}
                  style={Layout.search.inputHolder}
                  onPress={this.toggleSearch}
                >
                  <View>
                    <Image style={{ width: 58, height: 16 }} source={require('../assets/images/ondaLogoBlue.png')} resizeMode='contain' />
                  </View>
                  <View>
                    <Text style={{
                      color: Layout.colors.grey
                    }}><Translate id='search' /></Text>
                  </View>
                  <View style={{
                    width: 56,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                  }}>
                    <Image style={{ width: 17, height: 16 }} source={require('../assets/images/searchMag.png')} resizeMode='contain' />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <HomeFeatured navigation={navigation}/>

          <EventsCarousel
            navigation={navigation}
            firestoreQuery={[{
              collection: 'events',
              orderBy: ['openingStart', 'desc'],
              storeAs: 'homeWhatsOn',
              where: [
                ['openingStart', '<', endOfToday()],
                ['status', '==', 'published'],
                ['highlight', '==', 'none'],
              ],
              limit: 80,
            }]}
            filterCallback={(item) => {
              return item.closing.seconds > nowSeconds
            }}
            translateId={'whatsOn'}
            sectionHeaderWrapperStyle={{
              marginTop: 0
            }}
          />

          <HomeHighlights navigation={navigation} />

          <EventsCarousel
            navigation={navigation}
            firestoreQuery={[{
              collection: 'events',
              orderBy: ['closing', 'asc'],
              storeAs: 'homeClosingSoon',
              where: [
                ['closing', '>', nowDate()],
                ['status', '==', 'published'],
                ['highlight', '==', 'none'],
              ],
              limit: 80,
            }]}
            filterCallback={(item) => {
              return item.openingStart.seconds < nowSeconds && item.closing.seconds < oneWeekFromNowSeconds
            }}
            translateId={'closingSoon'}
          />

          <BannerAd />

          <HomeArticlesList navigation={navigation}/>

        </ScrollView>

        <SearchForm
          setFocus={focusHandler => this.focusInput = focusHandler}
          navigation={navigation}
          toggleSearch={this.toggleSearch}
          searchActive={searchActive}
          searchOpacity={this.searchOpacity}
          isAnimatingSearch={isAnimatingSearch}
        />

      </View>
    );
  }
}
