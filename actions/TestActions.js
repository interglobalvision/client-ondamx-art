import { TOGGLE_TEST_VALUE } from '../reducers/TestReducer'

export const toggleTestValue = () => {
  return {
    type: TOGGLE_TEST_VALUE,
  }
}
