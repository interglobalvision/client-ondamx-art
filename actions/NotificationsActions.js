import { UPDATE_LAST_NOTIFICATION_RECEIVED } from '../reducers/NotificationsReducer'
import { nowTimestamp } from '../lib/dates'

export const updateLastNotificationReceived = () => {
  return {
    type: UPDATE_LAST_NOTIFICATION_RECEIVED,
    date: nowTimestamp(),
  }
}
