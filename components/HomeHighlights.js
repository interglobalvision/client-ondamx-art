import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withCurrentLanguage, withTranslate } from '../lib/translations'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  View,
  FlatList,
  Button,
  Text,
  Easing,
  ScrollView
} from 'react-native'
import { Translate } from 'react-localize-redux'
import EventsCarouselItemMid from './EventsCarouselItemMid'
import SectionHeader from './SectionHeader'
import Layout from '../constants/Layout'

class HomeHighlights extends Component {

  _handleEventPress = (id) => {
    const { navigation } = this.props
    navigation.push('Event', { id })
  }

  _renderCarouselItems = () => {
    const { items, currentLanguage, translate } = this.props
    return items.map((item) => {
      return (
        <EventsCarouselItemMid item={item} handlePress={this._handleEventPress} key={item.id} currentLanguage={currentLanguage} translate={translate} />
      )
    })
  }

  render() {
    const { items } = this.props

    if (!isLoaded(items)) {
      return null
    }

    else if (isEmpty(items)) {
      return null
    }

    return (
      <View style={{
        ...Layout.events.carouselMid.slider.wrapper,
        backgroundColor: Layout.colors.yellow,
      }}>
        <SectionHeader
          translateId={'highlights'}
          wrapperStyle={{
            paddingHorizontal: Layout.marginBasic,
          }}
        />
        <ScrollView
          ref={(scrollView) => { this.scrollView = scrollView; }}
          contentContainerStyle={Layout.events.carouselMid.slider.contentContainer}
          horizontal= {true}
          showsHorizontalScrollIndicator={false}
          decelerationRate={0}
          snapToInterval={Layout.events.carouselMid.slider.snapToInterval}
        >
          {this._renderCarouselItems()}
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
   return {
     items: state.firestore.ordered['home-highlights'],
   }
}

export default compose(
  withCurrentLanguage,
  withTranslate,
  firestoreConnect([{
    collection: 'events',
    orderBy: ['openingStart', 'desc'],
    where: [
      ['highlight', '==', 'highlighted'],
      ['status', '==', 'published'],
    ],
    storeAs: 'home-highlights'
  }]),
  connect(mapStateToProps)
)(HomeHighlights)
