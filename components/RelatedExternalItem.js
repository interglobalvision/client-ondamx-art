import React, { Component } from 'react'
import { compose } from 'redux'
import {
  View,
  TouchableOpacity,
  Text,
} from 'react-native'
import Layout from '../constants/Layout'
import ResponsiveImage from '@expo/react-native-responsive-image'

class EventsCarouselItemSmall extends Component {
  render() {
    const { item, handlePress } = this.props

    return (
      <View style={Layout.relatedExternal.item}>
        <TouchableOpacity activeOpacity={1} style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-between'
        }} onPress={() => {handlePress(item.ogUrl)}}>
          <View style={Layout.relatedExternal.imageHolder}>
            { item.ogImage !== undefined &&
              <ResponsiveImage
                style={Layout.responsiveImage}
                sources={{
                  2: { uri: item.ogImage.url },
                }}
                preferredPixelRatio={2}
                resizeMode='cover'
              />
            }
          </View>
          <View style={{
            marginTop: Layout.marginBasic
          }}>
            <View>
              <Text style={{
                fontFamily: 'medium',
                fontSize: Layout.fontSizes.small,
              }}>{item.ogTitle}</Text>
            </View>
            <View>
              <Text style={{
                color: Layout.colors.grey,
                fontSize: Layout.fontSizes.smaller,
              }}>{item.ogSiteName}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

export default EventsCarouselItemSmall
