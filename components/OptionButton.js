import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native'
import Layout from '../constants/Layout'
import { Translate } from 'react-localize-redux'

export default class OptionButton extends Component {
  _handlePress = () => {
    if(this.props.handlePress) {
      this.props.handlePress(this.props.data)
    }
  }

  render() {
    const { translateId, wrapperStyle, title, arrow } = this.props

    return (
      <View>
        <TouchableOpacity
          style={{
            padding: Layout.marginBasic * 2,
            paddingLeft: Layout.marginBasic * 3,
            flexDirection: 'row',
            justifyContent: 'space-between',
            ...wrapperStyle,
          }}
          onPress={this._handlePress}
        >
          <Text style={ Layout.fontStyles.optionButton }>
            {translateId ? (
              <Translate id={translateId} />
            ) :
              title
            }
          </Text>
          {arrow &&
            <Image source={require('../assets/images/navArrow.png')} width={12} height={18} />
          }
        </TouchableOpacity>
      </View>
    )
  }
}
