import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withFirestore, firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { withNavigation } from 'react-navigation'
import { withTranslate, withCurrentLanguage } from '../lib/translations'
import find from 'lodash/find'
import {
  View,
  TouchableOpacity,
  Image,
  Text
} from 'react-native'
import { displayUserOnlyFeatureAlert } from '../lib/utils'
import { bookmarkToNotification, removeBookmarkNotification } from '../lib/notifications'

class BookmarkButton extends React.Component {
  _updateBookmark = () => {
    const {
      firestore,
      uid,
      itemId,
      itemCollection,
      itemBookmarks,
      isBookmarked,
      bookmarkDoc,
      translate,
      currentLanguage,
    } = this.props

    // clone item bookmark array
    let updatedItemBookmarks = itemBookmarks !== undefined ? itemBookmarks.slice() : []
    let incrementValue = 1

    const bookmarkData = {
      docId: itemId,
      userId: uid,
      updatedDate: new Date().getTime(),
    }

    if (!isBookmarked) {
      // push UID to item bookmark array
      updatedItemBookmarks.push(uid)
      // add bookmark doc to bookmarks collection
      firestore.add({ collection: 'bookmarks' }, bookmarkData)

      // Schedule notification
      bookmarkToNotification({
        uid,
        itemId,
        itemCollection,
        firestore,
        translate,
        currentLanguage,
      })
    } else {
      // filter UID from item bookmark array
      updatedItemBookmarks = updatedItemBookmarks.filter(userId => userId !== uid)
      // delete bookmark doc to bookmarks collection
      firestore.delete({ collection: 'bookmarks', doc: bookmarkDoc.docId })
      // set negative incrementValue
      incrementValue = -1

      removeBookmarkNotification({
        uid,
        itemId,
        itemCollection,
        firestore,
      })
    }

    // update item bookmark array in database
    firestore.update(
      {
        collection: itemCollection,
        doc: itemId
      }, {
        bookmarks: updatedItemBookmarks,
        bookmarkCount: firestore.FieldValue.increment(incrementValue)
      }
    )
  }

  // THIS IS BREAKING EXPO IDK WHY
  //shouldComponentUpdate(nextProps) {
    //const { isBookmarked }  = this.props
    //if (this.props.itemId === 'jsNUZ3aItUZkNOHil2mA') {
      //console.log('this.props', this.props)
      //console.log('nextProps', nextProps)
    //}
    //if (isBookmarked === nextProps.isBookmarked) {
      //return false
    //}

    //return true
  //}

  render() {
    const { isBookmarked, style, uid, navigation, translate } = this.props
    const imageStyle = { width: 22, height: 22 }

    return (
      <View style={style}>
        <TouchableOpacity
          onPress={() => {
            if (uid !== undefined) {
              this._updateBookmark()
            } else {
              displayUserOnlyFeatureAlert(navigation, translate)
            }
          }}
        >
          {!!isBookmarked ? (
            <Image style={imageStyle} source={require('../assets/images/bookmarkOn.png')} resizeMode='contain' />
          ) : (
            <Image style={imageStyle} source={require('../assets/images/bookmarkOff.png')} resizeMode='contain' />
          )}
        </TouchableOpacity>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  const { itemId } = props
  const { auth } = state.firebase

  let bookmarkDoc = []
  let isBookmarked = false
  if (isLoaded(auth) && !isEmpty(auth)) {
    const uid = auth.uid
    if (uid !== undefined) {
      // get bookmark from store
      const bookmarks = state.firestore.ordered['bookmarks']
      bookmarkDoc = find(state.firestore.ordered['bookmarks'], item => item.docId = itemId)

      // Check for UID in item bookmark array
      isBookmarked = props.itemBookmarks !== undefined ? props.itemBookmarks.includes(uid) : false
    }

    return {
      isBookmarked,
      bookmarkDoc,
      uid,
    }
  }

  return {
    isBookmarked,
    bookmarkDoc,
  }
}

export default compose(
  withFirestore,
  withTranslate,
  withCurrentLanguage,
  withNavigation,
  connect(mapStateToProps)
)(BookmarkButton)
