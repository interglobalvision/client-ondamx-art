import React from 'react'
import { compose } from 'redux'
import {
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native';

import Layout from '../constants/Layout'

import { withFirebase } from 'react-redux-firebase'
import { withTranslate } from '../lib/translations'

class EmailLoginForm extends React.Component {
  state = {
    email: '',
    password: '',
    error: false,
  }

  constructor(props) {
    super(props)
  }

  handleLogin = () => {
    const { email, password } = this.state
    const { navigation, firebase, translate } = this.props

    this.setState({ error: false })

    const cleanEmail = email.trim ? email.trim() : email
    const cleanPassword = password.trim ? password.trim() : password

    firebase.login(
      {
        email: cleanEmail,
        password: cleanPassword,
      },
    )
      .then( res => {
        navigation.navigate('Main')
      })
      .catch( error => {
        this.setState({
          error: translate(error.message),
        })
      })
  }

  render() {
    const {
      email,
      password,
      error
    } = this.state
    const { translate } = this.props

    return (
      <View style={{
        width: '100%',
        marginBottom: Layout.marginBasic * 2,
      }}>
        <TextInput
          style={Layout.textInput}
          onChangeText={(email) => this.setState({email})}
          value={email}
          placeholder={'Email'}
          placeholderTextColor={Layout.colors.grey}
          textContentType={'emailAddress'}
          keyboardType={'email-address'}
          autoCapitalize='none'
        />
        <TextInput
          style={Layout.textInput}
          onChangeText={(password) => this.setState({password})}
          value={password}
          placeholder={translate('password')}
          placeholderTextColor={Layout.colors.grey}
          textContentType={'password'}
          secureTextEntry={true}
          autoCapitalize='none'
        />
        {error &&
          <Text style={{
            color: Layout.colors.yellow,
            textAlign: 'center',
            minHeight: Layout.marginBasic
          }}>{error}</Text>
        }
        <View style={{
          alignItems: 'center',
          marginTop: Layout.marginBasic * 2,
        }}>
          <TouchableOpacity
            onPress={this.handleLogin}
            style={{
              ...Layout.submitButton,
              borderColor: Layout.colors.white,
              borderWidth: 1,
            }}
          >
            <Text
              style={Layout.fontStyles.submitButtonText}
            >
              {translate('login')}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default compose(
  withFirebase,
  withTranslate
)(EmailLoginForm)
