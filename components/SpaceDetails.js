import React, { Component } from 'react'
import {
  View,
  Text,
  Linking,
  Platform,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native'
import Layout from '../constants/Layout'
import { openMapsApp } from '../lib/geoloc'
import { format } from 'date-fns'
import { Translate } from 'react-localize-redux'
import { convertToTimeZone } from 'date-fns-timezone'
import { timeZone } from '../lib/dates'

export default class SpaceDetails extends Component {
  constructor(props) {
    super(props)

    this.state = {
      hoursOpen: false
    }
  }

  render() {
    const {
      spaceContent,
      spaceName,
      showAddress,
      showHours,
      showPhone,
      showWebsite,
      showEmail,
      showInstagram,
      translate,
    } = this.props

    const {
      location,
      hours,
      phone,
      website,
      email,
      instagram,
    } = spaceContent

    let appointment = spaceContent.appointment === undefined ? 'default' : spaceContent.appointment

    const hoursOrder = [
      'monday',
      'tuesday',
      'wednesday',
      'thursday',
      'friday',
      'saturday',
      'sunday',
    ]
    let hoursArray = []
    let hasHours = appointment === 'only' ? true : false

    if (Object.keys(hours).length > 0) {
      hoursOrder.forEach(day => {
        if (hours[day][0] !== null && hours[day][1] !== null) {
          // assemble hoursArray with ordered hours
          hoursArray.push({
            id: day,
            times: hours[day],
          })
          hasHours = true
        }
      })
    }

    const renderAppointment = () => {
      switch (appointment) {
        case 'option':
          return (
            <Text><Translate id='orAppointment' /></Text>
          )
          break
        case 'only':
          return (
            <Text><Translate id='onlyAppointment' /></Text>
          )
          break
        case 'and':
          return (
            <Text><Translate id='andAppointment' /></Text>
          )
          break
        default:
          return null
      }
    }

    const renderAllHours = () => {
      if (appointment !== 'only') {
        return (
          hoursArray.map(day => {
            const openDate = convertToTimeZone(new Date(day['times'][0].seconds * 1000), { timeZone })
            const closeDate = convertToTimeZone(new Date(day['times'][1].seconds * 1000), { timeZone })
            return(
              <View key={day['id']} style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
                <Text>{translate(day['id'])}</Text>
                <Text>{format(openDate, 'HH:mm')} — {format(closeDate, 'HH:mm')}</Text>
              </View>
            )
          })
        )
      }
      return null
    }

    const renderTodayHours = () => {
      const now = new Date()
      // index returned by getDay() is based on a week
      // that starts with Sunday, so we adjust for that here
      const todayIndex = now.getDay() === 0 ? 6 : now.getDay() - 1
      const todayHours = hours[hoursOrder[todayIndex]]

      if (appointment === 'only') {
        return <Text><Translate id='onlyAppointment' /></Text>
      }

      if (todayHours[0] !== null && todayHours[1] !== null) {
        // space open today
        // form today string with today's hours
        const openDate = convertToTimeZone(new Date(todayHours[0].seconds * 1000), { timeZone })
        const closeDate = convertToTimeZone(new Date(todayHours[1].seconds * 1000), { timeZone })

        return (
          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
            <Text>{translate('today').toLowerCase()} {translate('open')}</Text>
            <Text>{format(openDate, 'HH:mm')} — {format(closeDate, 'HH:mm')}</Text>
          </View>
        )
      } else {
        return (
          <View>
            <Text>{translate('closed')} {translate('today').toLowerCase()}</Text>
          </View>
        )
      }
      return null
    }

    const renderAddress = () => {

      if (!showAddress || location.address === undefined) {
        return null
      }

      if (!location.address.street.length) {
        return null
      }

      const { lat, lon } = location
      const hasLatLon = lat !== 0 && lon !== 0

      const {
        street,
        number,
        extra,
        neighborhood,
      } = location.address

      return (
        <View style={{
          flexDirection: 'row',
          marginBottom: Layout.marginBasic * .5,
        }}>
          <View style={{
            paddingRight: Layout.marginBasic,
            paddingTop: 2,
            width: 20,
            alignItems: 'center'
          }}>
            <Image source={require('../assets/images/detailsLocation.png')} width={15} height={11} />
          </View>
            <TouchableWithoutFeedback
              onPress={() => {
                if (hasLatLon) {
                  openMapsApp(lat, lon, spaceName, translate)
                }
              }}
            >
              <View style={{
                flex: 1,
                flexWrap: 'wrap',
              }}>
                <Text>
                  <Text style={hasLatLon ? Layout.fontStyles.linkText : null}>{street} {number}</Text>
                  <Text style={hasLatLon ? Layout.fontStyles.linkText : null}>{extra.length ? `, ${extra}` : '' }</Text>
                </Text>
                <Text style={hasLatLon ? Layout.fontStyles.linkText : null}>{neighborhood}</Text>
              </View>
            </TouchableWithoutFeedback>
        </View>
      )
    }

    return (
      <View>
        {renderAddress()}
        {(showHours && hasHours) &&
          <View style={{
            flex: 1,
          }}>
            <View style={{
              flexDirection: 'row',
              marginBottom: Layout.marginBasic * .5,
              flex: 1,
            }}>
              <View style={{
                paddingRight: Layout.marginBasic,
                paddingTop: 3,
                width: 20,
                alignItems: 'center'
              }}>
                <Image source={require('../assets/images/detailsHours.png')} width={11} height={11} />
              </View>
              <TouchableOpacity
                style={{
                  flex: 1,
                }}
                onPress={() => {
                  this.setState(prevState => ({
                    hoursOpen: !prevState.hoursOpen
                  }))
                }}
              >
                <View style={{
                  flexDirection: 'row',
                  maxWidth: 200,
                }}>
                  <View style={{
                    flex: 1
                  }}>
                    {this.state.hoursOpen ? (
                      <View>
                        {renderAllHours()}
                        {renderAppointment()}
                      </View>
                    ) : (
                      <View>
                        {renderTodayHours()}
                        {appointment === 'and' &&
                          <View>
                            {renderAppointment()}
                          </View>
                        }
                      </View>
                    )}
                  </View>
                  {appointment !== 'only' &&
                    <View
                      style={{
                        marginTop: 2,
                        marginLeft: Layout.marginBasic,
                        width: 11,
                        height: 11,
                      }}
                    >
                      <Image
                        source={require('../assets/images/accordionToggle.png')}
                        width={11}
                        height={11}
                        style={this.state.hoursOpen ? {
                          transform: [{ rotate: '-90deg' }],
                        } : null}
                      />
                    </View>
                  }
                </View>
              </TouchableOpacity>
            </View>
          </View>
        }
        {(showPhone && phone.length > 0) &&
          <View style={{
            flexDirection: 'row',
            marginBottom: Layout.marginBasic * .5,
          }}>
            <View style={{
              paddingRight: Layout.marginBasic,
              width: 20,
              alignItems: 'center'
            }}>
              <Image source={require('../assets/images/detailsPhone.png')} width={14} height={15} />
            </View>
            <View style={{
              flex: 1
            }}>
              <Text style={Layout.fontStyles.linkText} onPress={() => {
                const protocol = Platform.OS === 'ios' ? 'tel://' : 'tel:'
                Linking.openURL(protocol + phone)
              }}>{phone}</Text>
            </View>
          </View>
        }
        {(showWebsite && website.length > 0) &&
          <View style={{
            flexDirection: 'row',
            marginBottom: Layout.marginBasic * .5,
          }}>
            <View style={{
              paddingRight: Layout.marginBasic,
              paddingTop: 3,
              width: 20,
              alignItems: 'center'
            }}>
              <Image source={require('../assets/images/detailsLink.png')} width={14} height={15} />
            </View>
            <View style={{
              flex: 1
            }}>
              <Text style={Layout.fontStyles.linkText} onPress={() => {
                Linking.openURL(website)
              }}>{website}</Text>
            </View>
          </View>
        }
        {(showEmail && email.length > 0) &&
          <View style={{
            flexDirection: 'row',
            marginBottom: Layout.marginBasic * .5,
          }}>
            <View style={{
              paddingRight: Layout.marginBasic,
              paddingTop: 3,
              width: 20,
              alignItems: 'center'
            }}>
              <Image source={require('../assets/images/detailsEmail.png')} width={16} height={11} />
            </View>
            <View style={{
              flex: 1
            }}>
              <Text style={Layout.fontStyles.linkText} onPress={() => {
                Linking.openURL(`mailto:${email}`)
              }}>{email}</Text>
            </View>
          </View>
        }
        {(showInstagram && instagram.length > 0) &&
          <View style={{
            flexDirection: 'row',
            marginBottom: Layout.marginBasic * .5,
          }}>
            <View style={{
              paddingRight: Layout.marginBasic,
              paddingTop: 2,
              width: 20,
              alignItems: 'center'
            }}>
              <Image source={require('../assets/images/detailsInstagram.png')} width={13} height={13} />
            </View>
            <View style={{
              flex: 1
            }}>
              <Text style={Layout.fontStyles.linkText} onPress={() => {
                Linking.openURL('https://instagram.com/' + instagram)
              }}>@{instagram}</Text>
            </View>
          </View>
        }
      </View>
    )
  }
}
