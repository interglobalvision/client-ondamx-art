import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native'
import ResponsiveImage from '@expo/react-native-responsive-image'
import Layout from '../constants/Layout'
import { flattenLocalizedContent } from '../lib/translations'
import { getMediaThumb } from '../lib/images'
import { Translate } from 'react-localize-redux'
import Bullet from '../components/Bullet'
import { trimByChar } from '../lib/utils'
import { formatEventDates } from '../lib/dates'

export default class SearchResult extends Component {
  _handlePress = () => {
    const { result: { _index, _id }, navigation } = this.props

    let route = ''
    switch(_index) {
      case 'spaces':
        route = 'Space'
        break
      case 'events':
        route = 'Event'
        break
      case 'articles':
        route = 'Article'
        break
    }

    navigation.push(route, { id: _id })
  }

  _renderResultDetail = (content) => {
    const { result: { _index }, currentLanguage, translate } = this.props

    if (_index === 'events') {
      const eventContent = {
        ...content,
        openingStart: {
          seconds: content.openingStart._seconds
        },
        closing: {
          seconds: content.closing._seconds
        }
      }
      return (
        <Text><Bullet textStyle={Layout.fontStyles.type} />{formatEventDates(eventContent, currentLanguage.code, translate, false, {
          ...Layout.fontStyles.type,
          textTransform: 'none'
        })}</Text>
      )
    } else if (_index === 'spaces') {
      if (content.location.address.neighborhood.length) {
        return (
          <Text style={{
            ...Layout.fontStyles.type,
            textTransform: 'none'
          }}><Bullet textStyle={Layout.fontStyles.type} />{content.location.address.neighborhood}</Text>
        )
      }
    } else if (_index === 'articles') {
      if (content.author.length) {
        return (
          <Text style={{
            ...Layout.fontStyles.type,
            textTransform: 'none'
          }}><Bullet textStyle={Layout.fontStyles.type} /> {content.author}</Text>
        )
      }
    }
    return null
  }

  render() {
    const { result: { _index, _source }, currentLanguage, isLast } = this.props
    const content = flattenLocalizedContent(_source, currentLanguage)
    let resultName = _index === 'articles' ? content.title : content.name

    return (
      <View>
        <TouchableOpacity activeOpacity={1} style={{
          paddingHorizontal: Layout.marginBasic * 2,
          paddingVertical: Layout.marginBasic,
          borderBottomWidth: isLast ? 0 : 1,
          borderBottomColor: Layout.colors.mediumGrey,
          flexDirection: 'row',
          alignItems: 'center',
        }} onPress={() => { this._handlePress() }}>
          <View style={Layout.search.imageHolder}>
            {content.coverImage && content.coverImage.mediaUrl &&
              <ResponsiveImage
                style={Layout.responsiveImage}
                sources={{
                  2: { uri: getMediaThumb(content.coverImage.mediaUrl, 68, 68) },
                }}
                preferredPixelRatio={2}
                resizeMode='cover'
              />
            }
          </View>
          <View>
            <Text>{trimByChar(resultName, 42)}</Text>
            <Text>
              <Text style={Layout.fontStyles.type}><Translate id={content.type} /></Text>
              {this._renderResultDetail(content)}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}
