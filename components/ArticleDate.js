import React, { Component } from 'react'
import { Text } from 'react-native'
import { format } from 'date-fns'
import en from 'date-fns/locale/en'
import es from 'date-fns/locale/es'
import { convertToTimeZone } from 'date-fns-timezone'
import { timeZone } from '../lib/dates'
import Layout from '../constants/Layout'

class ArticleDate extends Component {
  render() {
    const { timestamp, currentLanguage } = this.props
    if (timestamp !== undefined) {
      const date = convertToTimeZone(new Date(timestamp * 1000), { timeZone})
      const locale = currentLanguage === 'en' ? en : es
      const dateString = currentLanguage === 'en' ? format(date, 'MMM D YYYY', {locale}) : format(date, 'D MMM YYYY', {locale})

      return (
        <Text style={Layout.fontStyles.article.single.date}>{dateString}</Text>
      )
    }

    return null
  }
}

export default ArticleDate
