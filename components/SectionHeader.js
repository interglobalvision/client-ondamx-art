import React, { Component } from 'react'
import {
  View,
  Text,
} from 'react-native'
import Layout from '../constants/Layout'
import { Translate } from 'react-localize-redux'

export default class SectionHeader extends Component {
  render() {
    return (
      <View style={{
        marginTop: Layout.marginBasic * 3,
        marginBottom: Layout.marginBasic * 2,
        marginLeft: Layout.marginBasic,
        marginRight: Layout.marginBasic,
        ...this.props.wrapperStyle,
      }}>
        <Text style={Layout.fontStyles.sectionHeader}><Translate id={this.props.translateId} /></Text>
      </View>
    )
  }
}
