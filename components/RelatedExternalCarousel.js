import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  View,
  FlatList,
  Button,
  Text,
  Easing,
  ScrollView,
  Linking,
} from 'react-native'
import RelatedExternalItem from './RelatedExternalItem'
import SectionHeader from './SectionHeader'
import Layout from '../constants/Layout'

class RelatedExternalCarousel extends Component {
  _handleItemPress = (url) => {
    Linking.openURL(url).catch((err) => console.error('An error occurred', err));
  }

  _renderCarouselItems = () => {
    return this.props.items.map((item) => {
      return (
        <RelatedExternalItem item={item} handlePress={this._handleItemPress} key={item.ogUrl} />
      )
    })
  }

  render() {
    const { items } = this.props

    if (!isLoaded(items)) {
      return null
    }

    else if (isEmpty(items)) {
      return null
    }

    return (
      <View style={Layout.relatedExternal.slider.wrapper}>
        <SectionHeader
          translateId={'fromAroundTheWeb'}
          wrapperStyle={{
            paddingLeft: Layout.marginBasic,
            paddingRight: Layout.marginBasic
          }}
        />
        <ScrollView
          ref={(scrollView) => { this.carousel = scrollView; }}
          contentContainerStyle={Layout.relatedExternal.slider.contentContainer}
          horizontal= {true}
          showsHorizontalScrollIndicator={false}
          decelerationRate={0}
          snapToInterval={Layout.relatedExternal.slider.snapToInterval}
          snapToAlignment={"left"}
        >
          {this._renderCarouselItems()}
        </ScrollView>
      </View>
    )
  }
}

export default RelatedExternalCarousel
