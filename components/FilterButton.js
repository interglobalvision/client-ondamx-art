import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native'
import Layout from '../constants/Layout'
import { Translate } from 'react-localize-redux'

export default class FilterButton extends Component {
  _handlePress = () => this.props.handlePress(this.props.filter.filterId)

  render() {
    const { filter, isActive } = this.props
    const backgroundColor = isActive ? Layout.colors.ondaBlue : Layout.colors.lightGrey
    const color = isActive ? Layout.colors.white : Layout.colors.black
    const borderColor = isActive ? Layout.colors.ondaBlue : Layout.colors.mediumGrey

    return (
      <View style={{
        ...Layout.outerCard,
        flex: 0,
        padding: Layout.marginBasic,
      }}>
        <TouchableOpacity
          style={{
            ...Layout.innerCard,
            height: 'auto',
            width: 'auto',
            padding: Layout.marginBasic,
            backgroundColor,
            borderRadius: 10,
            borderColor,
            borderWidth: 1,
          }}
          onPress={this._handlePress}
        >
          <Text style={{
            color
          }}><Translate id={filter.translateId} /></Text>
        </TouchableOpacity>
      </View>
    )
  }
}
