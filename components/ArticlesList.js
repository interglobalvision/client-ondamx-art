import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  View,
  FlatList,
} from 'react-native'
import ArticlesListItem from './ArticlesListItem'
import Layout from '../constants/Layout'
import SectionHeader from './SectionHeader'

class ArticlesList extends Component {

  _handleArticlePress = (id) => {
    const { navigation } = this.props
    navigation.push('Article', { id })
  }

  render() {
    const { items, translateId } = this.props

    if (!isLoaded(items)) {
      return null
    }

    else if (isEmpty(items)) {
      return null
    }

    return (
      <View style={{
        paddingHorizontal: Layout.marginBasic,
      }}>
        {translateId &&
          <SectionHeader translateId={translateId} />
        }
        <FlatList
          data={items}
          keyExtractor={(item) => item.id}
          renderItem={({item, index}) => (
            <ArticlesListItem item={item} handlePress={this._handleArticlePress} />
          )}
        />
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  const { firestoreQuery, filterCallback, sortCompare, limit } = props
  let items = []

  firestoreQuery.forEach(query => {
    const queryItems = state.firestore.ordered[query.storeAs]

    if (queryItems !== undefined) {
      if (filterCallback) {
        const filteredItems = queryItems.filter(filterCallback)
        items.push.apply(items, filteredItems)
      } else {
        items.push.apply(items, queryItems)
      }
    }
  })

  if (sortCompare) {
    items.sort(sortCompare)
  }

  if (limit) {
    items = items.slice(0, limit)
  }

  return {
    items
  }
}

export default compose(
  firestoreConnect((props) => {
    return props.firestoreQuery
  }),
  connect(mapStateToProps)
)(ArticlesList)
