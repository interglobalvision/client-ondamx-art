import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  View,
  FlatList,
  Button,
  Text,
  Easing,
  ScrollView,
} from 'react-native'
import { Translate } from 'react-localize-redux'
import MapCarouselItem from './MapCarouselItem'
import Layout from '../constants/Layout'

export default class MapCarousel extends Component {
  constructor(props) {
    super(props)

    this.state = {
      currentSlide: 0,
    }
  }

  renderCarouselItems = () => {
    const { collection, navigation, items } = this.props
    return items.map((item) => {
      return (
        <MapCarouselItem item={item} key={item.id} collection={collection} navigation={navigation} />
      )
    })
  }

  render() {
    const { onSlideChange } = this.props

    return (
      <View style={Layout.map.slider.wrapper}>
        <ScrollView
          ref={'slider'}
          contentContainerStyle={Layout.map.slider.contentContainer}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          decelerationRate={0}
          snapToInterval={Layout.map.slider.snapToInterval}
          onScroll={e => {
            const index = e.nativeEvent.contentOffset.x / (Layout.cards.map.width + (Layout.marginBasic * 2))
            if (index % 1 === 0) {
              this.setState({
                currentSlide: index,
              })
              onSlideChange(index)
            }
          }}
          scrollEventThrottle={100}
        >
          {this.renderCarouselItems()}
        </ScrollView>
      </View>
    )
  }
}
