import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { withCurrentLanguage, withTranslate } from '../lib/translations'
import {
  View,
  Animated,
  FlatList,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native'
import { Translate } from 'react-localize-redux'
import Layout from '../constants/Layout'
import axios from 'axios'
import debounce from 'lodash/debounce'
import Base64 from 'Base64'
import { elasticUsername, elasticPassword, elasticEndpoint } from '../constants/Services'
import SearchResult from './SearchResult'

window.btoa = Base64.btoa

//const elasticsearchClient = new Client({
	//cloud: {
		//id: 'onda-dev:dXMtZWFzdC0xLmF3cy5mb3VuZC5pbyRhOWYwMTNkOWQ4NGQ0ZWZiYWU5MjAzMTRkMDZkZjI4NyQ4ZmI5YjY0NDYxN2U0MDZmYmVkMDY3MmI4NThiNjkzNg==',
		//username: 'elastic',
		//password: 'sddLCHLgIRm3ANVCJBbfRBIe'
// https://a9f013d9d84d4efbae920314d06df287.us-east-1.aws.found.io:9243
	//}
//})

const initialState =  {
  searchText: '',
  results: [],
  active: false,
  loading: false,
}

class SearchForm extends Component {

  state = initialState

  constructor(props) {
    super(props)
    this.searchDebounced = debounce(this.search, 1000)
  }

  componentDidMount() {
    this.props.setFocus(this.focusInput)
  }

  focusInput = () => {
    this.searchInput.focus()
  }

  clearInput = () => {
    this.setState({
      searchText: ''
    })
  }

  handleOnChange = (searchText) => {
    this.setState({searchText}, () => this.searchDebounced(searchText))
  }

  search(searchText) {
    this.setState({ loading: true })

    axios.get(`${elasticEndpoint}/_all/_search?q=${searchText}`)
      .then((response) => {
        const { data } = response
        const { hits } = data

        this.setState({
          loading: false,
          results: hits.hits,
        })
      })
      .catch((err) => {
        this.setState({ loading: false })
        console.error(err)
      })
  }

  render() {
    const { searchText, loading, results } = this.state
    const {
      navigation,
      currentLanguage,
      toggleSearch,
      searchActive,
      searchOpacity,
      translate
    } = this.props

    return (
      <Animated.View
        style={{
          ...Layout.container,
          position: 'absolute',
          width: Layout.window.width,
          height: '100%',
          top: 0,
          left: 0,
          paddingTop: Layout.marginBasic * 4,
          opacity: searchOpacity,
        }}
        pointerEvents={searchActive ? 'auto' : 'none'}
      >
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
          <View style={{
            ...Layout.search.outerCard,
            flex: -1,
          }}>
            <View style={Layout.innerCard}>
              <View style={Layout.search.inputHolder}>
                <View>
                  <Image style={{ width: 17, height: 16 }} source={require('../assets/images/searchMag.png')} resizeMode='contain' />
                </View>
                <TextInput
                  ref={ref => { this.searchInput = ref }}
                  style={Layout.search.textInput}
                  onChangeText={this.handleOnChange}
                  value={searchText}
                  placeholder={translate('search')}
                  placeholderTextColor={Layout.colors.grey}
                />
                <View style={{
                  paddingLeft: Layout.marginBasic
                }}>
                  <TouchableOpacity onPress={this.clearInput}>
                    <Image style={{ width: 14, height: 14 }} source={require('../assets/images/searchClose.png')} resizeMode='contain' />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>

          <View style={{
            marginRight: Layout.marginBasic * 2
          }}>
            <TouchableOpacity onPress={toggleSearch}>
              <Text style={{
                textTransform: 'uppercase',
                color: Layout.colors.brown,
                fontSize: Layout.fontSizes.small,
              }}><Translate id='cancel' /></Text>
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={Layout.search.resultsContainer}
        >
          {results.length > 0 &&
            <FlatList
              keyboardShouldPersistTaps='handled'
              keyboardDismissMode='on-drag'
              data={results}
              keyExtractor={item => item._id}
              initialScrollIndex={0}
              renderItem={({item, index}) => {
                const isLast = results.length === index + 1
                return (
                  <SearchResult
                    result={item}
                    isLast={isLast}
                    navigation={navigation}
                    currentLanguage={currentLanguage}
                    translate={translate}
                  />
                )
              }}
            />
          }
        </View>
      </Animated.View>
    )
  }
}

export default compose(
  withCurrentLanguage,
  withTranslate,
)(SearchForm)
