import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { withCurrentLanguage, flattenLocalizedContent } from '../lib/translations'
import {
  View,
  Button,
  Text,
  ScrollView
} from 'react-native'
import Layout from '../constants/Layout'
import HomeFeaturedItem from './HomeFeaturedItem'
import ResponsiveImage from '@expo/react-native-responsive-image'
import { getMediaThumb } from '../lib/images'

class HomeFeatured extends Component {

  _handleEventPress = (id) => {
    const { navigation } = this.props
    navigation.push('Event', { id })
  }

  _renderCarouselItems = () => {
    return this.props.events.map((item) => {
      return (
        <HomeFeaturedItem item={item} handlePress={this._handleEventPress} key={item.id} />
      )
    })
  }

  render() {
    const { events, settings, currentLanguage } = this.props

    if (!isLoaded(events) || !isLoaded(settings)) {
      return null
    } else if (isEmpty(events) || isEmpty(settings)) {
      return null
    }

    const content = flattenLocalizedContent(settings[0], currentLanguage)

    return (
      <View style={Layout.events.featured.slider.wrapper}>
        <ScrollView
          ref={(scrollView) => { this.carousel = scrollView; }}
          contentContainerStyle={Layout.events.featured.slider.contentContainer}
          horizontal= {true}
          showsHorizontalScrollIndicator={false}
          decelerationRate={0}
          snapToInterval={Layout.events.featured.slider.snapToInterval}
        >
          {content.featuredCover &&
            <View style={Layout.events.featured.outerCard}>
              <View style={Layout.innerCard}>
                <View style={Layout.events.featured.coverImageHolder}>
                  <ResponsiveImage
                    style={Layout.responsiveImage}
                    sources={{
                      2: { uri: getMediaThumb(content.featuredCover.mediaUrl, '1005', '1260') },
                    }}
                    preferredPixelRatio={2}
                    resizeMode='cover'
                  />
                </View>
              </View>
            </View>
          }
          {this._renderCarouselItems()}
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
   return {
     events: state.firestore.ordered['home-featured'],
     settings: state.firestore.ordered['settings-client'],
   }
}

export default compose(
  withCurrentLanguage,
  firestoreConnect([
    {
      collection: 'events',
      orderBy: ['featuredOrder', 'asc'],
      where: [
        ['highlight', '==', 'featured'],
        ['status', '==', 'published'],
      ],
      storeAs: 'home-featured'
    },
    {
      collection: 'settings',
      doc: 'client',
      storeAs: 'settings-client',
    }
  ]),
  connect(mapStateToProps)
)(HomeFeatured)
