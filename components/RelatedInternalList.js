import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  View,
  FlatList
} from 'react-native'
import SectionHeader from './SectionHeader'
import Layout from '../constants/Layout'
import EventsItemHalf from './EventsItemHalf'
import ArticlesListItem from './ArticlesListItem'
import SpacesItem from './SpacesItem'

class RelatedInternalList extends Component {
  _handleItemPress = (id) => {
    const { navigation, collection } = this.props
    let screenTitle = 'Home'
    if (collection === 'events') {
      screenTitle = 'Event'
    } else if (collection === 'articles') {
      screenTitle = 'Article'
    } else if (collection === 'spaces') {
      screenTitle = 'Space'
    }
    navigation.push(screenTitle, { id })
  }

  render() {
    const { items, collection, translateId } = this.props

    if (!isLoaded(items)) {
      return null
    }

    else if (isEmpty(items)) {
      return null
    }

    return (
      <View style={{
        paddingHorizontal: Layout.marginBasic,
      }}>
        <SectionHeader translateId={translateId} />
        <FlatList
          data={items}
          keyExtractor={(item) => item.id}
          renderItem={({item, index}) => {
            return (
              <View>
                {collection === 'events' &&
                  <EventsItemHalf
                    item={item}
                    handlePress={this._handleItemPress}
                  />
                }
                {collection === 'articles' &&
                  <ArticlesListItem
                    item={item}
                    handlePress={this._handleItemPress}
                  />
                }
                {collection === 'spaces' &&
                  <SpacesItem
                    item={item}
                    handlePress={this._handleItemPress}
                  />
                }
              </View>
            )
          }}
        />
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  const { collection, docId } = props

  return {
    items: state.firestore.ordered[`related-${collection}-${docId}`]
  }
}

export default compose(
  firestoreConnect((props) => {
    const { collection, docId } = props

    let orderBy = ['createdDate', 'desc']

    if (collection === 'events') {
      orderBy = ['openingStart', 'desc']
    } else if (collection === 'articles') {
      orderBy = ['publishDate', 'desc']
    } else if (collection === 'spaces') {
      orderBy = ['name', 'asc']
    }

    return [
      {
        collection,
        orderBy,
        where: [
          ['status', '==', 'published'],
          ['relatedTo', 'array-contains', docId]
        ],
        storeAs: `related-${collection}-${docId}`,
      }
    ]
  }),
  connect(mapStateToProps)
)(RelatedInternalList)
