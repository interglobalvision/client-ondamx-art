import React, { Component } from 'react'
import { compose } from 'redux'
import { withCurrentLanguage, flattenLocalizedContent, withTranslate } from '../lib/translations'
import {
  View,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native'
import ResponsiveImage from '@expo/react-native-responsive-image'
import Layout from '../constants/Layout'
import BookmarkButton from '../components/BookmarkButton'
import { getMediaThumb } from '../lib/images'

class SpacesItem extends Component {
  render() {
    const { item, currentLanguage, handlePress, translate } = this.props
    const content = flattenLocalizedContent(item, currentLanguage)
    return (
      <View style={{
        backgroundColor: Layout.colors.lightGrey,
      }}>
        <TouchableOpacity activeOpacity={1} style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: Layout.marginBasic,
          paddingVertical: Layout.marginBasic,
        }} onPress={() => handlePress(item.id)}>
          <View style={{
            width: 72,
            height: 72,
            backgroundColor: Layout.colors.mediumGrey,
            borderRadius: Layout.borderRadius,
            overflow: 'hidden',
          }}>
            {item.coverImage && item.coverImage.mediaUrl &&
              <ResponsiveImage
                style={{
                  width: '100%',
                  height: '100%',
                }}
                sources={{
                  2: { uri: getMediaThumb(item.coverImage.mediaUrl, 324, 324) },
                }}
                preferredPixelRatio={2}
              />
            }
          </View>
          <View style={{
            padding: Layout.marginBasic,
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            flexWrap: 'nowrap',
          }}>
            <View style={{
              flex: 1
            }}>
              <Text style={Layout.fontStyles.space.card.name}>{content.name}</Text>
              <Text style={Layout.fontStyles.space.card.type}>{translate(content.type)}</Text>
              {content.location.address !== undefined &&
                <Text style={Layout.fontStyles.space.card.neighborhood}>{content.location.address.neighborhood}</Text>
              }
            </View>
            <View style={{
              width: 35,
            }}>
              <BookmarkButton
                style={{
                  paddingHorizontal: Layout.marginBasic
                }}
                itemId={item.id}
                itemCollection={'spaces'}
                itemBookmarks={item.bookmarks}
              />
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

export default compose(
  withCurrentLanguage,
  withTranslate,
)(SpacesItem)
