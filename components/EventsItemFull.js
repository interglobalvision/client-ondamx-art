import React, { Component } from 'react'
import { compose } from 'redux'
import { withCurrentLanguage, flattenLocalizedContent, withTranslate } from '../lib/translations'
import {
  View,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native'
import ResponsiveImage from '@expo/react-native-responsive-image'
import Layout from '../constants/Layout'
import { getMediaThumb } from '../lib/images'
import BookmarkButton from '../components/BookmarkButton'
import { formatEventDates } from '../lib/dates'
import Bullet from '../components/Bullet'

class EventsItemFull extends Component {
  render() {
    const { item, handlePress, currentLanguage, translate } = this.props
    const content = flattenLocalizedContent(item, currentLanguage)

    return (
      <View style={Layout.events.full.outerCard}>
        <View style={Layout.innerCard}>
          <TouchableOpacity activeOpacity={1} style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-between'
          }} onPress={() => {handlePress(item.id)}}>
            <View style={Layout.events.full.imageHolder}>
              {item.coverImage && item.coverImage.mediaUrl &&
                <ResponsiveImage
                  style={Layout.responsiveImage}
                  sources={{
                    2: { uri: getMediaThumb(item.coverImage.mediaUrl, 581, 459) },
                  }}
                  preferredPixelRatio={2}
                  resizeMode='cover'
                />
              }
            </View>
            <View style={{
              padding: Layout.marginBasic
            }}>
              <View style={{
                marginBottom: Layout.marginBasic
              }}>
                <Text style={Layout.fontStyles.event.card.name}>{content.name}</Text>
              </View>
              <View style={{
                marginBottom: Layout.marginBasic * .5,
              }}>
                <Text style={Layout.fontStyles.event.card.space}>{content.space.name}</Text>
              </View>
              <View>
                <Text><Text style={Layout.fontStyles.event.card.type}>{translate(content.type)}</Text><Bullet textStyle={Layout.fontStyles.event.card.date} />{formatEventDates(content, currentLanguage.code, translate, false, Layout.fontStyles.event.card.date)}</Text>
              </View>
            </View>
          </TouchableOpacity>
          <BookmarkButton
            itemId={item.id}
            itemCollection={'events'}
            itemBookmarks={item.bookmarks}
            style={{
              position: 'absolute',
              top: Layout.marginBasic * 2,
              right: Layout.marginBasic * 2,
            }}
          />
        </View>
      </View>
    )
  }
}

export default compose(
  withCurrentLanguage,
  withTranslate,
)(EventsItemFull)
