import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { withCurrentLanguage, withTranslate } from '../lib/translations'
import {
  View,
  FlatList,
  Button,
  Text,
  Easing,
  ScrollView,
} from 'react-native'
import { Translate } from 'react-localize-redux'
import EventsCarouselItemSmall from './EventsCarouselItemSmall'
import SectionHeader from './SectionHeader'
import Layout from '../constants/Layout'

class EventsCarousel extends Component {
  _handleEventPress = (id) => {
    const { navigation } = this.props
    navigation.push('Event', { id })
  }

  _renderCarouselItems = () => {
    const { items, currentLanguage, translate } = this.props
    return items.map((item) => {
      return (
        <EventsCarouselItemSmall item={item} handlePress={this._handleEventPress} key={item.id} currentLanguage={currentLanguage} translate={translate} />
      )
    })
  }

  render() {
    const { items, translateId, sectionHeaderWrapperStyle } = this.props

    if (!isLoaded(items)) {
      return null
    }

    else if (isEmpty(items)) {
      return null
    }

    return (
      <View style={Layout.events.carouselSmall.slider.wrapper}>
        <SectionHeader
          translateId={translateId}
          wrapperStyle={{
            ...sectionHeaderWrapperStyle,
            paddingHorizontal: Layout.marginBasic,
          }}
        />
        <ScrollView
          ref={(scrollView) => { this.carousel = scrollView }}
          contentContainerStyle={Layout.events.carouselSmall.slider.contentContainer}
          horizontal= {true}
          showsHorizontalScrollIndicator={false}
          decelerationRate={0}
          snapToInterval={Layout.events.carouselSmall.slider.snapToInterval}
          snapToAlignment={"left"}
        >
          {this._renderCarouselItems()}
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  const { firestoreQuery, filterCallback, sortCompare, limit } = props
  let items = []

  firestoreQuery.forEach(query => {
    const queryItems = state.firestore.ordered[query.storeAs]

    if (queryItems !== undefined) {
      if (filterCallback) {
        const filteredItems = queryItems.filter(filterCallback)
        items.push.apply(items, filteredItems)
      } else {
        items.push.apply(items, queryItems)
      }
    }
  })

  if (sortCompare) {
    items.sort(sortCompare)
  }

  if (limit) {
    items = items.slice(0, limit)
  }

  return {
    items
  }
}
export default compose(
  withCurrentLanguage,
  withTranslate,
  firestoreConnect(props => {
    return props.firestoreQuery
  }),
  connect(mapStateToProps)
)(EventsCarousel)
