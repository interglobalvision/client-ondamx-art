import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  View,
  FlatList,
  ScrollView,
  Text,
  Platform,
} from 'react-native'
import Layout from '../constants/Layout'
import DirectoryMap from '../components/DirectoryMap'
import { nowSeconds } from '../lib/dates'

class MapContainer extends Component {
  render() {
    const { items, navigation, activeFilter } = this.props

    if (!isLoaded(items)) {
      return null
    }

    else if (isEmpty(items)) {
      return null
    }

    return (
      <DirectoryMap items={items} navigation={navigation} activeFilter={activeFilter} />
    )
  }
}

const mapStateToProps = (state, props) => {
  const { activeFilter: { firestoreQuery, collection, sortCompare, filterCallback, limit } } = props
  let items = []

  firestoreQuery.forEach(query => {
    const queryItems = state.firestore.ordered[query.storeAs]

    if (queryItems !== undefined) {
      if (filterCallback) {
        const filteredItems = queryItems.filter(filterCallback)
        items.push.apply(items, filteredItems)
      } else {
        items.push.apply(items, queryItems)
      }
    }
  })

  if (sortCompare) {
    items.sort(sortCompare)
  }

  if (limit) {
    items = items.slice(0, limit)
  }

  return {
    items
  }
}

export default compose(
  connect((state) => ({ uid: state.firebase.auth.uid })),
  firestoreConnect(props => {
    const { uid, activeFilter } = props
    if (uid && activeFilter.filterId === 'bookmarked') {
      return [{
        ...activeFilter.firestoreQuery[0],
        where: [
          ['status', '==', 'published'],
          ['bookmarks','array-contains',uid]
        ],
      }]
    }

    return activeFilter.firestoreQuery
  }),
  connect(mapStateToProps)
)(MapContainer)
