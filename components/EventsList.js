import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  View,
  FlatList,
} from 'react-native'
import EventsItemFull from './EventsItemFull'
import EventsItemHalf from './EventsItemHalf'
import SectionHeader from './SectionHeader'
import Layout from '../constants/Layout'

class EventsList extends Component {

  _handleEventCardPress = (id) => {
    const { navigation } = this.props
    navigation.push('Event', { id })
  }

  render() {
    const { items, translateId, fullItem } = this.props

    if (!isLoaded(items)) {
      return null
    }

    else if (isEmpty(items)) {
      return null
    }

    return (
      <View>
        <View style={{
          paddingHorizontal: Layout.marginBasic,
        }}>
          {translateId &&
            <SectionHeader translateId={translateId} />
          }
          <FlatList
            data={items}
            keyExtractor={(item) => item.id}
            renderItem={({item, index}) => {
              if (fullItem) {
                return (<EventsItemFull item={item} handlePress={this._handleEventCardPress} />)
              }
              return (<EventsItemHalf item={item} handlePress={this._handleEventCardPress} />)
            }}
          />
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  const { firestoreQuery, filterCallback, sortCompare, limit } = props
  let items = []

  firestoreQuery.forEach(query => {
    const queryItems = state.firestore.ordered[query.storeAs]

    if (queryItems !== undefined) {
      if (filterCallback) {
        const filteredItems = queryItems.filter(filterCallback)
        items.push.apply(items, filteredItems)
      } else {
        items.push.apply(items, queryItems)
      }
    }
  })

  if (items.length) {
    // filter out duplicates
    items = Array.from(new Set(items.map(i => i.id))).map(id => items.find(i => i.id === id))
  }
  
  if (sortCompare) {
    items.sort(sortCompare)
  }

  if (limit) {
    items = items.slice(0, limit)
  }

  return {
    items
  }
}

export default compose(
  firestoreConnect(props => {
    return props.firestoreQuery
  }),
  connect(mapStateToProps)
)(EventsList)
