import React, { Component } from 'react';
import { Text, Platform } from 'react-native';

export default class Bullet extends Component {
  render() {
    const { textStyle } = this.props
    const bulletFont = Platform.OS === 'ios' ? 'Helvetica' : 'Roboto'

    return <Text style={{...textStyle, fontFamily: bulletFont }}> &bull; </Text>
  }
}
