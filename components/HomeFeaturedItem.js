import React, { Component } from 'react'
import { compose } from 'redux'
import { withCurrentLanguage, flattenLocalizedContent, withTranslate } from '../lib/translations'
import {
  View,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native'
import ResponsiveImage from '@expo/react-native-responsive-image'
import Layout from '../constants/Layout'
import BookmarkButton from '../components/BookmarkButton'
import { getMediaThumb } from '../lib/images'
import TextContent from '../components/TextContent'
import { trimByChar } from '../lib/utils'
import { Translate } from 'react-localize-redux'
import { formatEventDates } from '../lib/dates'
import Bullet from '../components/Bullet'

class HomeFeaturedItem extends Component {
  render() {
    const { item, handlePress, currentLanguage, translate } = this.props
    const content = flattenLocalizedContent(item, currentLanguage)
    const contentState = JSON.parse(content.mainContent)
    const firstParagraph = contentState.blocks[0].text

    return (
      <View style={Layout.events.featured.outerCard}>
        <View style={Layout.innerCard}>
          <TouchableOpacity activeOpacity={1} style={Layout.events.featured.wrapper} onPress={() => {handlePress(item.id)}}>
            <View style={Layout.events.featured.imageHolder}>
              {item.coverImage && item.coverImage.mediaUrl &&
                <ResponsiveImage
                  style={Layout.responsiveImage}
                  sources={{
                    2: { uri: getMediaThumb(item.coverImage.mediaUrl, '1005', '666') },
                  }}
                  preferredPixelRatio={2}
                  resizeMode='cover'
                />
              }
            </View>
            <View style={{
              margin: Layout.marginBasic * 3
            }}>
              <View>
                <Text style={Layout.fontStyles.event.featured.name}>{content.name}</Text>
              </View>
              <View>
                <Text style={Layout.fontStyles.event.featured.space}>{content.space.name}</Text>
                <Text><Text style={Layout.fontStyles.event.featured.type}>{translate(content.type)}</Text><Bullet textStyle={Layout.fontStyles.event.featured.date} />{formatEventDates(content, currentLanguage.code, translate, false, Layout.fontStyles.event.featured.date)}</Text>
              </View>
              <View style={{
                marginTop: Layout.marginBasic,
              }}>
                {content.featuredSummary ? (
                  <Text>{trimByChar(content.featuredSummary)} <Text style={Layout.fontStyles.buttonText}><Translate id='readMore' /></Text></Text>
                ) : (
                  <Text>{trimByChar(firstParagraph)} <Text style={Layout.fontStyles.buttonText}><Translate id='readMore' /></Text></Text>
                )}
              </View>
            </View>
          </TouchableOpacity>
          <BookmarkButton
            itemId={item.id}
            itemCollection={'events'}
            itemBookmarks={item.bookmarks}
            style={{
              position: 'absolute',
              top: Layout.marginBasic * 2,
              right: Layout.marginBasic * 2,
            }}
          />
        </View>
      </View>
    )
  }
}

export default compose(
  withCurrentLanguage,
  withTranslate,
)(HomeFeaturedItem)
