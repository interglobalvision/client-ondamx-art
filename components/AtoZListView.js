import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  View,
  FlatList,
  ScrollView,
  Text,
} from 'react-native'
import Layout from '../constants/Layout'
import { directoryFilters } from '../lib/filters'
import SpacesItem from '../components/SpacesItem'
import groupBy from 'lodash/groupBy'
import orderBy from 'lodash/orderBy'
import AlphabetList from '../components/AlphabetList'

class AtoZListView extends Component {
  _handleSpacePress = (id) => {
    const { navigation } = this.props
    navigation.push('Space', { id })
  }

  _handleLetterPress = (listIndex) => {
    if (((this.listLength - listIndex) * 90) > this.wrapperHeight) {
      this.flatListRef.scrollToIndex({
        animated: false,
        index: listIndex,
        viewOffset: 0,
        viewPosition: 0
      })
    } else {
      this.flatListRef.scrollToEnd({
        animated: false
      })
    }
  }

  render() {
    const { spaces } = this.props

    if (!isLoaded(this.props.spaces)) {
      return null
    }

    else if (isEmpty(spaces)) {
      return null
    }

    const spacesNormalized = spaces.map((item) => {
      item['nameNormal'] = item.name.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
      return item
    })

    const spacesOrdered = orderBy(spacesNormalized,[space => space.nameNormal.toUpperCase()],'asc')

    const regex = /^[a-z]+$/i

    const letters = spacesOrdered.filter((item) => {
      first = item.nameNormal[0].toUpperCase()
      if (regex.exec(first)) {
        return true
      }
      return false
    })

    const numbers = spacesOrdered.filter((item) => {
      first = item.nameNormal[0].toUpperCase()
      if (regex.exec(first)) {
        return false
      }
      return true
    })

    const spacesReordered = letters.concat(numbers)
    this.listLength = spacesReordered.length

    return (
      <View
        style={Layout.container}
        onLayout={(event) => {
          const {height} = event.nativeEvent.layout
          this.wrapperHeight = height
        }}
      >
        <FlatList
          style={{
            flex: 1,
            paddingTop: Layout.marginBasic,
            paddingHorizontal: Layout.marginBasic,
          }}
          data={spacesReordered}
          ref={(ref) => { this.flatListRef = ref; }}
          keyExtractor={item => item.id}
          initialScrollIndex={0}
          renderItem={({item}) => {
            return (
              <SpacesItem item={item} handlePress={this._handleSpacePress} />
            )
          }}
        />
        <AlphabetList handleLetterPress={this._handleLetterPress} listItems={spacesReordered} />
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    spaces: state.firestore.ordered[props.firestoreQuery.storeAs],
  }
}

export default compose(
  firestoreConnect((props) => {
    return [{
      collection: 'spaces',
      ...props.firestoreQuery,
    }]
  }),
  connect(mapStateToProps)
)(AtoZListView)
