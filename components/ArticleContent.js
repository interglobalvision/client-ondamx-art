import React, { Component } from 'react'
import { compose } from 'redux'
import {
  View,
  StyleSheet,
} from 'react-native'
import Layout from '../constants/Layout'
import getRNDraftJSBlocks from 'react-native-draftjs-render'
import { atomicHandler } from '../lib/draft'
import { withCurrentLanguage } from '../lib/translations'

class ArticleContent extends Component {
  render() {
    const { content, currentLanguage } = this.props

    // Parse content
    const contentState = JSON.parse(content)

    const draftParams = {
      contentState,
      atomicHandler: (item, entityMap) => atomicHandler(item, entityMap, currentLanguage),
      customStyles: StyleSheet.flatten(Layout.draftContent),
      depthMargin: Layout.marginBasic
    }

    return (
      <View style={{
        paddingVertical: Layout.marginBasic * 2
      }}>
        {getRNDraftJSBlocks(draftParams)}
      </View>
    )
  }
}

export default compose(
  withCurrentLanguage
)(ArticleContent)
