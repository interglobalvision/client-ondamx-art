import React, { Component } from 'react'
import { compose } from 'redux'
import { withCurrentLanguage, flattenLocalizedContent, withTranslate } from '../lib/translations'
import {
  View,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native'
import ResponsiveImage from '@expo/react-native-responsive-image'
import Layout from '../constants/Layout'
import { getMediaThumb } from '../lib/images'
import BookmarkButton from '../components/BookmarkButton'
import { trimByChar } from '../lib/utils'
import { formatEventDates } from '../lib/dates'
import Bullet from '../components/Bullet'

class MapCarouselItem extends Component {
  _handlePress = () => {
    const { navigation, item: { id }, collection } = this.props
    const title = collection === 'events' ? 'Event' : 'Space'
    navigation.push(title, { id })
  }

  componentDidUpdate(prevProps) {
    if (this.props.item.bookmarks !== prevProps.item.bookmarks) {

    }
  }

  render() {
    const { item, currentLanguage, collection, translate } = this.props
    const content = flattenLocalizedContent(item, currentLanguage)

    return (
      <View style={Layout.map.outerCard}>
        <View style={Layout.innerCard}>
          <TouchableOpacity activeOpacity={1} style={{
            flex: 1,
            flexDirection: 'column',
          }} onPress={this._handlePress}>
            <View style={Layout.map.imageHolder}>
              { item.coverImage && item.coverImage.mediaUrl &&
                <ResponsiveImage
                  style={Layout.responsiveImage}
                  sources={{
                    2: { uri: getMediaThumb(item.coverImage.mediaUrl, '880', '309') },
                  }}
                  preferredPixelRatio={2}
                  resizeMode='cover'
                />
              }
            </View>
            <View style={{
              marginHorizontal: Layout.marginBasic,
              marginTop: Layout.marginBasic / 2,
              justifyContent: 'flex-start',
              flex: 1,
            }}>
              <Text style={Layout.fontStyles.event.carousel.name}>{trimByChar(content.name, 28)}</Text>
              {collection === 'events' && content.space !== undefined &&
                <Text style={Layout.fontStyles.event.carousel.space}>{trimByChar(content.space.name, 32)}</Text>
              }
              <Text>
                <Text style={Layout.fontStyles.event.carousel.type}>{translate(content.type)}</Text>
                {collection === 'events' && <Text><Bullet textStyle={Layout.fontStyles.event.carousel.date} />{formatEventDates(content, currentLanguage.code, translate, false, Layout.fontStyles.event.carousel.date)}</Text>}</Text>
            </View>
          </TouchableOpacity>
          {
            /*<BookmarkButton
              itemId={item.id}
              itemCollection={collection}
              itemBookmarks={item.bookmarks}
              style={{
                position: 'absolute',
                top: Layout.marginBasic,
                right: Layout.marginBasic,
              }}
            />*/
          }
        </View>
      </View>
    )
  }
}

export default compose(
  withCurrentLanguage,
  withTranslate,
)(MapCarouselItem)
