import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
} from 'react-native'
import Layout from '../constants/Layout'
import getRNDraftJSBlocks from 'react-native-draftjs-render'
import { trimByWord } from '../lib/utils'
import { Translate } from 'react-localize-redux'

export default class TextContent extends Component {
  constructor(props) {
    super(props)

    this.state = {
      expanded: false
    }
  }

  handleExpand = () => {
    this.setState({
      expanded: true
    })
  }

  render() {
    const { content } = this.props

    // Parse content
    const contentState = JSON.parse(content)
    const firstParagraph = contentState.blocks[0].text

    if (firstParagraph.length === 0) {
      return null
    }

    const draftParams = {
      contentState,
      customStyles: StyleSheet.flatten(Layout.draftContent)
    }

    return (
      <View style={{
        marginTop: Layout.marginBasic * 2
      }}>
        {this.state.expanded ? (
          getRNDraftJSBlocks(draftParams)
        ) : (
          <View style={{
            paddingHorizontal: Layout.marginBasic * 2,
            paddingBottom: Layout.marginBasic
          }}>
            <Text>{trimByWord(firstParagraph)} <Text onPress={this.handleExpand} style={Layout.fontStyles.buttonText}><Translate id='readMore' /></Text></Text>
          </View>
        )}
      </View>
    )
  }
}
