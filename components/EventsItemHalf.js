import React, { Component } from 'react'
import { compose } from 'redux'
import { withCurrentLanguage, flattenLocalizedContent, withTranslate } from '../lib/translations'
import {
  View,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native'
import ResponsiveImage from '@expo/react-native-responsive-image'
import Layout from '../constants/Layout'
import { getMediaThumb } from '../lib/images'
import { trimByChar } from '../lib/utils'
import BookmarkButton from '../components/BookmarkButton'
import { formatEventDates } from '../lib/dates'

class EventsItemHalf extends Component {
  render() {
    const { item, handlePress, currentLanguage, translate } = this.props
    const content = flattenLocalizedContent(item, currentLanguage)

    return (
      <View style={Layout.events.half.outerCard}>
        <View style={Layout.innerCard}>
          <TouchableOpacity activeOpacity={1} style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }} onPress={() => {handlePress(item.id)}}>
            <View style={{
              width: '60%',
              padding: Layout.marginBasic,
              justifyContent: 'space-between',
            }}>
              <View>
                <View style={{
                  marginBottom: Layout.marginBasic
                }}>
                  <Text style={Layout.fontStyles.event.card.name}>{trimByChar(content.name, 60)}</Text>
                </View>
                <View style={{
                  marginBottom: Layout.marginBasic * .5,
                }}>
                  <Text style={Layout.fontStyles.event.card.space}>{content.space.name}</Text>
                </View>
              </View>
              <View>
                <Text style={Layout.fontStyles.event.card.type}>{translate(content.type)}</Text>
                {formatEventDates(content, currentLanguage.code, translate, false, Layout.fontStyles.event.card.date)}
              </View>
            </View>
            <View style={{
              width: '40%',
              height: '100%',
              backgroundColor: Layout.imageBackgroundColor,
            }}>
              {item.coverImage && item.coverImage.mediaUrl &&
                <ResponsiveImage
                  style={Layout.responsiveImage}
                  sources={{
                    2: { uri: getMediaThumb(item.coverImage.mediaUrl, 581, 459) },
                  }}
                  preferredPixelRatio={2}
                  resizeMode='cover'
                />
              }
            </View>
          </TouchableOpacity>
          <BookmarkButton
            itemId={item.id}
            itemCollection={'events'}
            itemBookmarks={item.bookmarks}
            style={{
              position: 'absolute',
              top: Layout.marginBasic * 1.5,
              right: Layout.marginBasic * 1.5,
            }}
          />
        </View>
      </View>
    )
  }
}

export default compose(
  withCurrentLanguage,
  withTranslate,
)(EventsItemHalf)
