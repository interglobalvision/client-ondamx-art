import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ScrollView } from 'react-native';
import Layout from '../constants/Layout'
import { flattenLocalizedContent } from '../lib/translations'

export default class ImageCarousel extends Component {
  constructor(props) {
    super(props)

    this.captions = []
    this.media = props.content.imageCarousel

    this.state = {
      caption: '',
      currentSlide: 0,
    }
  }

  componentDidMount() {
    if (this.media && this.media.length) {
      this.captions = this.media.map(item => {
        const content = flattenLocalizedContent(item, this.props.currentLanguage)
        return content.caption
      })
      this.setState({
        caption: this.captions[0]
      })
    }
  }

  render() {
    const { width } = Layout.window
    const height = width * .714

    if (this.media && this.media.length) {
      return (
        <View>
          <View style={{
            width: width,
            height: height
          }}>
            <ScrollView
              horizontal
              pagingEnabled
              showsHorizontalScrollIndicator={false}
              onScroll={e => {
                const index = e.nativeEvent.contentOffset.x / width
                if (index % 1 === 0) {
                  this.setState({
                    currentSlide: index,
                    caption: this.captions[index]
                  })
                }
              }}
            >
              {this.media.map(item => (
                <View style={{
                  width: width,
                  height: height,
                  justifyContent: 'center',
                  alignItems: 'center',
                }} key={item.id}>
                  <Image
                    style={{
                      width: '100%',
                      height: '100%',
                    }}
                    source={{
                      uri: item.mediaUrl
                    }}
                    resizeMode='cover'
                  />
                </View>
              ))}
            </ScrollView>
          </View>
          <View style={{
            flexDirection: 'row',
            paddingVertical: Layout.marginBasic,
            justifyContent: 'center',
          }}>
            {this.media.map((item, index) => (
              <View key={item.id} style={{
                width: 5,
                height: 5,
                borderRadius: 5,
                borderWidth: 1,
                borderColor: Layout.colors.grey,
                borderStyle: 'solid',
                backgroundColor: this.state.currentSlide === index ? Layout.colors.grey : 'transparent',
                marginHorizontal: 3,
              }}></View>
            ))}
          </View>
          {this.captions.length > 0 &&
            <View style={Layout.captionHolder}>
              <Text style={Layout.fontStyles.caption}>{this.state.caption}</Text>
            </View>
          }
        </View>
      );
    }

    return null
  }
}
