import React, { Component } from 'react'
import { compose } from 'redux'
import { flattenLocalizedContent } from '../lib/translations'
import {
  View,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native'
import ResponsiveImage from '@expo/react-native-responsive-image'
import Layout from '../constants/Layout'
import { getMediaThumb } from '../lib/images'
import { trimByChar } from '../lib/utils'
import BookmarkButton from '../components/BookmarkButton'
import { formatEventDates } from '../lib/dates'
import Bullet from '../components/Bullet'

export default class EventsCarouselItemSmall extends Component {
  render() {
    const { item, handlePress, currentLanguage, translate } = this.props
    const content = flattenLocalizedContent(item, currentLanguage)

    return (
      <View style={Layout.events.carouselSmall.outerCard}>
        <View style={Layout.innerCard}>
          <TouchableOpacity activeOpacity={1} style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-between'
          }} onPress={() => {handlePress(item.id)}}>
            <View style={Layout.events.carouselSmall.imageHolder}>
              { item.coverImage && item.coverImage.mediaUrl &&
                <ResponsiveImage
                  style={Layout.responsiveImage}
                  sources={{
                    2: { uri: getMediaThumb(item.coverImage.mediaUrl, '880', '309') },
                  }}
                  preferredPixelRatio={2}
                  resizeMode='cover'
                />
              }
            </View>
            <View style={{
              margin: Layout.marginBasic
            }}>
              <View>
                <Text style={Layout.fontStyles.event.carousel.name}>{trimByChar(content.name, 26)}</Text>
              </View>
              <View>
                <View style={{
                  marginBottom: Layout.marginBasic / 3,
                }}>
                  <Text style={Layout.fontStyles.event.carousel.space}>{trimByChar(content.space.name,30)}</Text>
                </View>
                <Text><Text style={Layout.fontStyles.event.carousel.type}>{translate(content.type)}</Text><Bullet textStyle={Layout.fontStyles.event.card.date} />{formatEventDates(content, currentLanguage.code, translate, false, Layout.fontStyles.event.card.date)}</Text>
              </View>
            </View>
          </TouchableOpacity>
          <BookmarkButton
            itemId={item.id}
            itemCollection={'events'}
            itemBookmarks={item.bookmarks}
            style={{
              position: 'absolute',
              top: Layout.marginBasic,
              right: Layout.marginBasic,
            }}
          />
        </View>
      </View>
    )
  }
}
