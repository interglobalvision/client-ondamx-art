import React from 'react'
import { compose } from 'redux'
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Button,
  TextInput,
} from 'react-native';

import Layout from '../constants/Layout'

import { withFirebase } from 'react-redux-firebase'
import { withTranslate } from '../lib/translations'

class EmailCreateUserForm extends React.Component {
  state = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: '',
    error: false,
  }

  constructor(props) {
    super(props)
  }

  handleSignUp = () => {
    this.setState({ error: false })

    console.log(this.state)

    const { firstName, lastName, email, password, confirmPassword } = this.state
    const { navigation, firebase, translate } = this.props

    if (firstName && firstName === '') {
      return this.setState({
        error: translate("First name is required."),
      })
    }

    if (lastName && lastName === '') {
      return this.setState({
        error: translate("Last name is required."),
      })
    }

    if (password && password !== confirmPassword) {
      return this.setState({
        error: translate("Passwords don't match."),
      })
    }

    const cleanEmail = email.trim ? email.trim() : email
    const cleanPassword = password.trim ? password.trim() : password
    const cleanFirstName = firstName.trim ? firstName.trim() : firstName
    const cleanLastName = lastName.trim ? lastName.trim() : lastName

    firebase.createUser({
      email: cleanEmail,
      password: cleanPassword,
    }, {
      firstName: cleanFirstName,
      lastName: cleanLastName,
      email: cleanEmail,
      active: true,
      role: 'user',
      createdDate: new Date().getTime(),
      updatedDate: new Date().getTime(),
    })
      .then( res => {
        navigation.navigate('Main')
      })
      .catch( error => {
        this.setState({
          error: translate(error.message),
        })
      })
  }

  render() {
    const {
      firstName,
      lastName,
      email,
      password,
      confirmPassword,
      error
    } = this.state
    const { translate } = this.props
    return (
      <View style={{
        width: '100%',
        marginBottom: Layout.marginBasic * 2,
      }}>
        <View style={{
          flexDirection: 'row',
          justifyContent: 'center',
          marginBottom: Layout.marginBasic * 2,
        }}>
          <Text style={Layout.fontStyles.sectionHeader}>{translate('createAccount')}</Text>
        </View>
        <View style={{
          marginBottom: Layout.marginBasic * 2,
        }}>
          <TextInput
            style={Layout.textInput}
            onChangeText={(firstName) => this.setState({firstName})}
            value={firstName}
            placeholder={translate('firstName')}
            placeholderTextColor={Layout.colors.grey}
            textContentType='givenName'
          />
          <TextInput
            style={Layout.textInput}
            onChangeText={(lastName) => this.setState({lastName})}
            value={lastName}
            placeholder={translate('lastName')}
            placeholderTextColor={Layout.colors.grey}
            textContentType='familyName'
          />
          <TextInput
            style={Layout.textInput}
            onChangeText={(email) => this.setState({email})}
            value={email}
            placeholder='Email'
            placeholderTextColor={Layout.colors.grey}
            textContentType='emailAddress'
            keyboardType='email-address'
            autoCapitalize='none'
          />
          <TextInput
            style={Layout.textInput}
            onChangeText={(password) => this.setState({password})}
            value={password}
            placeholder={translate('password')}
            placeholderTextColor={Layout.colors.grey}
            textContentType='newPassword'
            autoCapitalize='none'
            keyboardType='visible-password'
          />
          <TextInput
            style={Layout.textInput}
            onChangeText={(confirmPassword) => this.setState({confirmPassword})}
            value={confirmPassword}
            placeholder={translate('confirmPassword')}
            placeholderTextColor={Layout.colors.grey}
            textContentType='password'
            autoCapitalize='none'
            keyboardType='visible-password'
          />
          {error && <Text>{error}</Text>}
        </View>
        <View style={{
          alignItems: 'center'
        }}>
          <TouchableOpacity
            onPress={this.handleSignUp}
            style={Layout.submitButton}
          >
            <Text
              style={Layout.fontStyles.submitButtonText}
            >
              {translate('createAccount')}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default compose(
  withFirebase,
  withTranslate
)(EmailCreateUserForm)
