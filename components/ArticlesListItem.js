import React, { Component } from 'react'
import { compose } from 'redux'
import { withCurrentLanguage, flattenLocalizedContent } from '../lib/translations'
import { Translate } from 'react-localize-redux'
import {
  View,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native'
import ResponsiveImage from '@expo/react-native-responsive-image'
import Bullet from './Bullet'
import Layout from '../constants/Layout'
import { getMediaThumb } from '../lib/images'
import BookmarkButton from '../components/BookmarkButton'

class ArticlesListItem extends Component {
  render() {
    const { item, handlePress, currentLanguage } = this.props
    const content = flattenLocalizedContent(item, currentLanguage)

    return (
      <View>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {handlePress(item.id)}}
        >
          <View style={{
            flexDirection: 'row'
          }}>
            <View style={Layout.articles.item.imageHolder}>
              {item.coverImage && item.coverImage.mediaUrl &&
                <ResponsiveImage
                  style={Layout.responsiveImage}
                  sources={{
                    2: { uri: getMediaThumb(item.coverImage.mediaUrl, 324, 324) },
                  }}
                  preferredPixelRatio={2}
                  resizeMode='cover'
                />
              }
              <BookmarkButton
                itemId={item.id}
                itemCollection={'articles'}
                itemBookmarks={item.bookmarks}
                style={{
                  position: 'absolute',
                  top: Layout.marginBasic,
                  right: Layout.marginBasic,
                }}
              />
            </View>
            <View style={{
              padding: Layout.marginBasic,
              flex: 1,
            }}>
              <View style={{
                flex: 1,
              }}>
                <Text style={Layout.fontStyles.article.card.title}>{content.title}</Text>
              </View>
              <View style={{
                flex: 1,
              }}>
                <Text>
                  <Text style={Layout.fontStyles.article.card.type}><Translate id={content.type}/></Text>
                  <Bullet textStyle={Layout.fontStyles.article.card.author} />
                  <Text style={Layout.fontStyles.article.card.author}>{content.author}</Text>
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

export default compose(
  withCurrentLanguage
)(ArticlesListItem)
