import React, { Component } from 'react'
import { Notifications } from 'expo'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  View,
  FlatList,
  Text,
} from 'react-native'
import Layout from '../constants/Layout'
import { Translate } from 'react-localize-redux'
import NotificationListItem from './NotificationListItem'
import { nowTimestamp } from '../lib/dates'
import { withNavigation } from 'react-navigation'

class NotificationList extends Component {
  _handlePress = (id, type) => {
    const { navigation } = this.props

    switch(type) {
      case 'events':
        navigation.push('Event', { id })
        break
    }
  }

  // Reset notification badge

  render() {
    const { items } = this.props

    Notifications.setBadgeNumberAsync(0)

    return (
      <View style={Layout.container}>
        {!isLoaded(items) && ( // SHOW LOADING
          <View style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
            <Text style={{
              color: Layout.colors.grey,
              fontSize: Layout.fontSizes.mid,
              textAlign: 'center',
              alignSelf: 'center',
            }}><Translate id={'loading'} /></Text>
          </View>
        )}
        {isLoaded(items) && isEmpty(items) && ( // SHOW EMPTY
          <View style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
            <Text style={{
              color: Layout.colors.grey,
              fontSize: Layout.fontSizes.mid,
              textAlign: 'center',
            }}><Translate id={'emptyNotifications'} /></Text>
          </View>
        )}
        {isLoaded(items) && !isEmpty(items) && ( // SHOW NOTIFS
          <FlatList
            data={items}
            keyExtractor={(item) => item.id}
            renderItem={({item, index}) => (
              <NotificationListItem item={item} handlePress={this._handlePress} />
            )}
          />
        )}
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    items: state.firestore.ordered['user-notifications'],
  }
}

export default compose(
  withNavigation,
  connect((state) => ({ lastReceived: state.notifications.lastReceived })),
  firestoreConnect((props) => {
    const { uid } = props

    return [{
      collection: 'notifications',
      where: [
        ['notificationDate', '<=', props.lastReceived],
        ['userId', '==', uid ],
      ],
      storeAs: 'user-notifications',
      orderBy: ['notificationDate', 'desc'],
      ...props.firestoreQuery,
    }]
  }),
  connect(mapStateToProps)
)(NotificationList)
