import React, { Component } from 'react'
import { compose } from 'redux'
import { withCurrentLanguage, flattenLocalizedContent, withTranslate } from '../lib/translations'
import {
  View,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native'
import ResponsiveImage from '@expo/react-native-responsive-image'
import isToday from 'date-fns/is_today'
import isPast from 'date-fns/is_past'
import { format } from 'date-fns'
import en from 'date-fns/locale/en'
import es from 'date-fns/locale/es'
import Layout from '../constants/Layout'
import { getMediaThumb } from '../lib/images'
import BookmarkButton from '../components/BookmarkButton'
import Bullet from './Bullet'

class NotificationListItem extends Component {
  render() {
    const { item, handlePress, currentLanguage, translate } = this.props
    const content = flattenLocalizedContent(item, currentLanguage)

    const dateFormat = currentLanguage === 'en' ? 'MMM D' : 'D [de] MMM'
    const locale = currentLanguage === 'en' ? en : es
    const notificationDate = new Date(content.notificationDate.seconds * 1000)
    const isPastNotification = !isToday(notificationDate) && isPast(notificationDate)

    return (
      <View>
        <TouchableOpacity activeOpacity={1} style={{
          paddingHorizontal: Layout.marginBasic * 2,
          paddingVertical: Layout.marginBasic,
          borderBottomWidth: 1,
          borderBottomColor: Layout.colors.mediumGrey,
          flexDirection: 'row',
          alignItems: 'center',
          opacity: isPastNotification ? 0.3 : 1,
        }} onPress={() => handlePress(item.docId, item.type)}>
          <View style={Layout.search.imageHolder}>
            {content.coverImage && content.coverImage.mediaUrl &&
              <ResponsiveImage
                style={Layout.responsiveImage}
                sources={{
                  2: { uri: getMediaThumb(content.coverImage.mediaUrl, 68, 68) },
                }}
                preferredPixelRatio={2}
                resizeMode='cover'
              />
            }
          </View>
          <Text style={{flex: 1, flexWrap: 'wrap'}}>
            {content.title}
            {isPastNotification && <Bullet textStyle={Layout.fontStyles.type} />}
            {isPastNotification && format(notificationDate, dateFormat, { locale })}
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
}

export default compose(
  withCurrentLanguage,
  withTranslate,
)(NotificationListItem)
