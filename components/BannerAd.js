import React, { Component } from 'react'
import {
  View,
  Text,
} from 'react-native'
import Layout from '../constants/Layout'

export default class BannerAd extends Component {
  render() {
    return null
    return (
      <View style={{
        ...this.props.wrapperStyle,
        paddingTop: Layout.marginBasic * 4,
        paddingBottom: Layout.marginBasic * 4,
        paddingLeft: Layout.marginBasic * 2,
        paddingRight: Layout.marginBasic * 2,
        backgroundColor: Layout.colors.white
      }}>
        <View style={{
          height: 130,
          justifyContent: 'center',
          alignItems: 'center',
          borderColor: Layout.colors.ondaBlue,
          borderWidth: 2,
          backgroundColor: Layout.colors.lightGrey
        }}>
          <Text>ADVERTISEMENT</Text>
        </View>
      </View>
    )
  }
}
