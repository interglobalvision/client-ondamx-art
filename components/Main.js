import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { Platform, StatusBar, StyleSheet, View } from 'react-native'
import { AppLoading, Notifications, Linking } from 'expo'
import * as Permissions from 'expo-permissions'
import Constants from 'expo-constants'
import * as Font from 'expo-font'
import { Asset } from 'expo-asset'
import * as Icon from '@expo/vector-icons'
import AppNavigator from '../navigation/AppNavigator'
import { withLocalize } from 'react-localize-redux'
import englishTranslations from '../translations/en.translations.json'
import spanishTranslations from '../translations/es.translations.json'
import Layout from '../constants/Layout'
import {
  setCustomView,
  setCustomTextInput,
  setCustomText,
  setCustomImage,
  setCustomTouchableOpacity
} from 'react-native-global-props'
import { scheduleEventsNotifications, markNotificationAsPublished }  from '../lib/notifications'
import { updateLastNotificationReceived } from '../actions/NotificationsActions'

const customTextProps = {
  style: Layout.fontStyles.defaultTextStyle,
}

setCustomText(customTextProps)

class Main extends React.Component {
  constructor(props) {
    super(props)

    this.props.initialize({
      languages: [
        { name: 'English', code: 'en' },
        { name: 'Español', code: 'es' }
      ],
      options: {
        renderToStaticMarkup: false
      }
      //translation: globalTranslations,
    })

    this.props.addTranslationForLanguage(englishTranslations, 'en')
    this.props.addTranslationForLanguage(spanishTranslations, 'es')

    this.props.setActiveLanguage('es')

  }

  registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      )
      let finalStatus = existingStatus
      if (existingStatus !== 'granted') {
        try {
          const { status } = await Permissions.askAsync(
            Permissions.NOTIFICATIONS
          )
          finalStatus = status
        } catch (error) {
          console.error(error)
        }
      }

      if (finalStatus !== 'granted') {
        // alert('Failed to get push token for push notification!')
        return
      }
      let token = await Notifications.getExpoPushTokenAsync()
      // console.log(token)
    } else {
      alert('Must use physical device for Push Notifications')
    }
  }

  componentDidMount() {
    this.registerForPushNotificationsAsync()

    // Handle notifications that are received or selected while the app
    // is open. If the app was closed and then opened by tapping the
    // notification (rather than just tapping the app icon to open it),
    // this function will fire on the next tick after the app starts
    // with the notification data.
    this._notificationSubscription = Notifications.addListener(
      this._handleNotification
    )

  }

  _handleNotification = notification => {
    const { firestore } = this.props
    const { notificationId, data } = notification

    // Update time of last notification
    this.props.updateLastNotificationReceived()

    // Mark notification as published
    markNotificationAsPublished(notificationId, data, firestore)
    // Check it comes from selecting a notification
    if (notification.origin === 'selected') {

      if (data.url !== undefined) {
        Linking.openURL(data.url)
      }
    }
  }

  componentDidUpdate() {
    const { auth, notificationsEvents } = this.props

    if (isLoaded(auth) && !isEmpty(auth) && isLoaded(notificationsEvents) && !isEmpty(notificationsEvents)) {
      // scheduleEventsNotifications(notificationsEvents)
    }

  }

  render() {
    const prefix = Linking.makeUrl('/')

    return (
      <View style={Layout.container}>
        {Platform.OS === 'ios' && <StatusBar hidden={true} />}
        <AppNavigator
          uriPrefix={prefix}
        />
      </View>
    )
  }
}

const mapAuthStateToProps = (state, props) => {
  return {
    auth: state.firebase.auth,
  }
}

const mapStateToProps = (state, props) => {
  const { auth } = props

  if (isLoaded(auth) && !isEmpty(auth) && isLoaded(state.firestore.ordered) && !isEmpty(state.firestore.ordered['notifications-events'])) {
    return {
      notificationsEvents: state.firestore.ordered['notifications-events'],
    }
  }

  return {}
}

const mapDispatchToProps = dispatch => {
  return {
    updateLastNotificationReceived: () => {
      dispatch(updateLastNotificationReceived())
    }

  }
}


export default compose(
  connect(mapAuthStateToProps),
  firestoreConnect(props => {
    const { auth } = props
    if (isLoaded(auth) && !isEmpty(auth)) {
      return [{
        collection: 'bookmarks',
        where: [
          ['userId','==',auth.uid]
        ],
        storeAs: 'bookmarks'
        }, {
        collection: 'events',
        storeAs: 'notifications-events',
        where: [
          ['status', '==', 'published'],
          // TODO: limit by dates (aka Today)
        ],
        orderBy: ['openingStart', 'desc']
      }]
    }

    return []
  }),
  connect(mapStateToProps,mapDispatchToProps),
  withLocalize,
)(Main)
