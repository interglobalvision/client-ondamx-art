import React, { Component } from 'react'
import { compose } from 'redux'
import { withCurrentLanguage, flattenLocalizedContent } from '../lib/translations'
import {
  View,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native'
import ResponsiveImage from '@expo/react-native-responsive-image'
import Layout from '../constants/Layout'
import { trimByWord } from '../lib/utils'
import { Translate } from 'react-localize-redux'
import { getMediaThumb } from '../lib/images'
import BookmarkButton from '../components/BookmarkButton'

class ArticlesFeaturedItem extends Component {
  render() {
    const { item, handlePress, currentLanguage } = this.props
    const content = flattenLocalizedContent(item, currentLanguage)

    // Parse content
    const contentState = JSON.parse(content.mainContent)

    const firstParagraph = contentState.blocks[0].text

    return (
      <View style={{
        marginBottom: Layout.marginBasic * 2
      }}>
        <TouchableOpacity onPress={() => {handlePress(item.id)}}>
          <View style={{
            flexDirection: 'row'
          }}>
            <View style={Layout.articles.featured.imageHolder}>
              {item.coverImage && item.coverImage.mediaUrl &&
                <ResponsiveImage
                  style={Layout.responsiveImage}
                  sources={{
                    2: { uri: getMediaThumb(item.coverImage.mediaUrl, 581, 459) },
                  }}
                  preferredPixelRatio={2}
                  resizeMode='cover'
                />
              }
              <BookmarkButton
                itemId={item.id}
                itemCollection={'articles'}
                itemBookmarks={item.bookmarks}
                style={{
                  position: 'absolute',
                  top: Layout.marginBasic,
                  right: Layout.marginBasic,
                }}
              />
            </View>
            <View style={{
              padding: Layout.marginBasic,
              flex: 1,
              justifyContent: 'space-between',
            }}>
              <Text style={Layout.fontStyles.article.featured.title}>{content.title}</Text>
              <View>
                <Text style={Layout.fontStyles.article.featured.type}><Translate id={content.type}/></Text>
                <Text style={Layout.fontStyles.article.featured.author}>{content.author}</Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <View style={{
          padding: Layout.marginBasic
        }}>
          <Text>{trimByWord(firstParagraph)} <Text onPress={() => {handlePress(item.id)}} style={Layout.fontStyles.buttonText}><Translate id='readMore' /></Text></Text>
        </View>
      </View>
    )
  }
}

export default compose(
  withCurrentLanguage
)(ArticlesFeaturedItem)
