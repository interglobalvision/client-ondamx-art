import React, { Component } from 'react'
import { compose } from 'redux'
import { withNavigation } from 'react-navigation'
import { withTranslate } from '../lib/translations'
import {
  View,
  TouchableOpacity,
  Text
} from 'react-native'
import Layout from '../constants/Layout'

class UserOnlyFeatureNotice extends Component {
  render() {
    const { navigation, translate } = this.props

    return (
      <View style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: Layout.marginBasic,
        paddingRight: Layout.marginBasic,
      }}>
        <Text style={{
          color: Layout.colors.grey,
          fontSize: Layout.fontSizes.mid,
          textAlign: 'center',
          marginBottom: Layout.marginBasic * 2,
        }}>{translate('onlyLoggedInUsers')}</Text>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Auth')
          }}
          style={Layout.submitButton}
        >
          <Text style={Layout.fontStyles.submitButtonText}>
            {translate('iWantToLogin')}
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
}

export default compose(
  withTranslate,
  withNavigation,
)(UserOnlyFeatureNotice)
