import React, { Component } from 'react'
import {
  View,
  Text,
} from 'react-native'
import Layout from '../constants/Layout'
import { Translate } from 'react-localize-redux'

export default class SectionHeaderSmall extends Component {
  render() {
    return (
      <View style={{
        ...this.props.wrapperStyle,
        marginTop: Layout.marginBasic * 3,
        marginBottom: Layout.marginBasic * 2,
        marginLeft: Layout.marginBasic,
        marginRight: Layout.marginBasic
      }}>
        <Text style={Layout.fontStyles.sectionHeaderSmall}><Translate id={this.props.translateId} /></Text>
      </View>
    )
  }
}
