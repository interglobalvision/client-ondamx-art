import React, { Component } from 'react'
import {
  View,
  Text,
} from 'react-native'
import Layout from '../constants/Layout'
import { Translate } from 'react-localize-redux'

export default class HeaderTitle extends Component {
  render() {
    const { title, translateId } = this.props
    return (
      <Text>
        {translateId ? (
          <Translate id={translateId} />
        ) :
          title
        }
      </Text>
    )
  }
}
