import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native'
import Layout from '../constants/Layout'

export default class AlphabetList extends Component {
  constructor(props) {
    super(props)

    this.lastGestureIndex = null

    const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ#".split('')
    let lastIndex = 0

    this.dataAlphabet = alphabet.map((letter) => {
      let index

      if (letter === '#') {
        index = props.listItems.length - 1
      } else {
        const foundItem = props.listItems.find((item) => {
          return item.nameNormal[0].toUpperCase() === letter
        })
        if (foundItem !== undefined) {
          index = props.listItems.indexOf(foundItem)
          lastIndex = index
        } else {
          index = lastIndex
        }
      }
      return {
        letter,
        index
      }
    })
  }

  handleGesture = (e) => {
    const ev = e.nativeEvent.touches[0]

    let letterIndex = Math.floor((ev.pageY - this.wrapperPageY) / (this.wrapperHeight / 26))

    if (letterIndex < 0) {
      letterIndex = 0
    } else if (letterIndex > 26) {
      letterIndex = 26
    }

    if (this.dataAlphabet[letterIndex] !== undefined) {
      const index = this.dataAlphabet[letterIndex].index

      if (index !== this.lastGestureIndex) {
        this.props.handleLetterPress(index)
        this.lastGestureIndex = index
      }
    }
  }

  render() {
    const { handleLetterPress } = this.props

    return (
      <View
        style={{
          position: 'absolute',
          top: 0,
          right: 0,
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <View
          style={{
            paddingHorizontal: Layout.marginBasic
          }}
          ref={(ref) => { this.alphaWrapperRef = ref }}
          onStartShouldSetResponder={() => true}
          onMoveShouldSetResponder={() => true}
          onResponderTerminationRequest={() => false}
          onResponderGrant={this.handleGesture}
          onResponderMove={this.handleGesture}
          onLayout={({nativeEvent}) => {
            if (this.alphaWrapperRef) {
              this.alphaWrapperRef.measure((x, y, width, height, pageX, pageY) => {
                this.wrapperHeight = height
                this.wrapperPageY = pageY
              })
            }
          }}
        >
          {
            this.dataAlphabet.map((item) => {
              return (
                <View key={'alphabetListItem' + item.letter}>
                  <Text style={{
                    color: Layout.colors.grey,
                    textAlign: 'center',
                  }}>{item.letter}</Text>
                </View>
              )
            })
          }
        </View>
      </View>
    )
  }
}
