import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  View,
  FlatList,
  Text,
} from 'react-native'
import ArticlesFeaturedItem from './ArticlesFeaturedItem'
import ArticlesListItem from './ArticlesListItem'
import SectionHeader from './SectionHeader'
import OptionButton from './OptionButton'
import Layout from '../constants/Layout'

class HomeArticlesList extends Component {

  _handleArticleTitlePress = (id) => {
    const { navigation } = this.props
    navigation.push('Article', { id })
  }

  _handleViewAllPress = () => {
    const { navigation } = this.props
    navigation.push('ArticleArchive')
  }

  render() {
    const { articles } = this.props

    if (!isLoaded(articles)) {
      return null
    }

    else if (isEmpty(articles)) {
      return null
    }

    return (
      <View style={{
        backgroundColor: Layout.colors.lightBlue,
        paddingBottom: Layout.marginBasic,
      }}>
        <View style={{
          padding: Layout.marginBasic,
          paddingTop: 0,
        }}>
          <SectionHeader translateId={'magazine'} />
          <FlatList
            data={articles}
            keyExtractor={(item) => item.id}
            renderItem={({item, index}) => {
              if(item.localizedContent && item.localizedContent.en.title) {
                if (index === 0) {
                  return <ArticlesFeaturedItem item={item} handlePress={this._handleArticleTitlePress} />
                } else {
                  return <ArticlesListItem item={item} handlePress={this._handleArticleTitlePress} />
                }
              } else {
                return
              }
            }}
          />
        </View>
        <OptionButton
          translateId={'viewAll'}
          handlePress={this._handleViewAllPress}
          arrow
        />
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
   return {
     articles: state.firestore.ordered.articles,
   }
}

export default compose(
  firestoreConnect([{
    collection: 'articles',
    orderBy: ['publishDate', 'desc'],
    where: ['status','==','published'],
    limit: 4,
  }]),
  connect(mapStateToProps)
)(HomeArticlesList)
