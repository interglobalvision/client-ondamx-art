import React, { Component } from 'react'
import {
  View,
  Image,
} from 'react-native'
import Layout from '../constants/Layout'

export default class HeaderLeft extends Component {
  render() {
    return (
      <View style={{
        paddingLeft: Layout.marginBasic * 1.5
      }}>
        <Image source={require('../assets/images/navArrowBack.png')} width={12} height={18} />
      </View>
    )
  }
}
