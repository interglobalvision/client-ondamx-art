import React, { Component } from 'react'
import {
  View,
  Text,
  Image,
} from 'react-native'
import Layout from '../constants/Layout'

export default class CoverImage extends Component {
  render() {
    const { media, currentLanguage } = this.props

    return (
      <View style={{
        width: Layout.window.width,
        height: Layout.window.width * .714,
        backgroundColor: Layout.colors.mediumGrey
      }} key={media.id}>
        <Image
          style={{
            width: '100%',
            height: '100%',
          }}
          source={{
            uri: media.mediaUrl
          }}
          resizeMode='cover'
        />
      </View>
    )
  }
}
