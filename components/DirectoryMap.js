import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  View,
  FlatList,
  ScrollView,
  Text,
  Platform,
} from 'react-native'
import Layout from '../constants/Layout'
import MapView, { Marker, Callout } from 'react-native-maps'
import { mapFilters } from '../lib/filters'
import MapCarousel from '../components/MapCarousel'
import { getCurrentLocation, sortByDistance } from '../lib/geoloc'

const defaultRegion = {
  latitude: 19.405050967447785,
  longitude: -99.15067164009709,
  latitudeDelta: 0.15,
  longitudeDelta: 0.12,
}

export default class DirectoryMap extends Component {
  constructor(props) {
    super(props)

    this.state = {
      activeId: mapFilters[0].filterId,
      region: defaultRegion,
      isOrdering: true,
      activeIndex: 0,
    }
  }

  componentDidMount() {
    this._orderItemsAndResetRegion()
  }

  componentDidUpdate(prevProps) {
    if (this.props.activeFilter.filterId !== prevProps.activeFilter.filterId) {
      // filter has changed
      this.setState({ isOrdering: true })
      this._orderItemsAndResetRegion()
    }
  }

  _orderItemsAndResetRegion = () => {
    const { items } = this.props

    if (items && items.length) {
      // get device location
      return getCurrentLocation().then(position => {
        // reorder items by distance from device location
        this.items = sortByDistance({lat: position.coords.latitude, lon: position.coords.longitude}, items)
        // animate to first item location
        this._animateToRegion(this.items[0].location.lat, this.items[0].location.lon)
        // allow render items
        this.setState({
          isOrdering: false,
          activeIndex: 0,
        })
      }).catch(e => {
        this.items = items
        this._animateToRegion(this.items[0].location.lat, this.items[0].location.lon)
        this.setState({
          isOrdering: false,
          activeIndex: 0,
        })
      })
    }
  }

  _animateToRegion = (lat, lon) => {
    const markerLatDelta = 0.025,
          markerLonDelta = 0.02,
          currentLatDelta = this.state.region.latitudeDelta,
          currentLonDelta = this.state.region.longitudeDelta
    const newLatDelta = currentLatDelta < markerLatDelta ? currentLatDelta : markerLatDelta
    const newLonDelta = currentLonDelta < markerLonDelta ? currentLonDelta : markerLonDelta

    this.map.animateToRegion({
      latitude: lat - (newLatDelta * 0.11),
      longitude: lon,
      latitudeDelta: newLatDelta,
      longitudeDelta: newLonDelta,
    }, 500)
  }

  _handleMarkerPress = (e, index) => {
    const data = e.nativeEvent
    const sliderPositionX = (Layout.cards.map.width + (Layout.marginBasic * 2)) * index

    this._animateToRegion(data.coordinate.latitude, data.coordinate.longitude)

    this.animateSliderTimer = setTimeout(() => {
      this.refs.mapCarousel.refs.slider.scrollTo({x: sliderPositionX, y: 0, animated: true})
    }, 500)

    this.setState({
      activeIndex: index,
    })
  }

  _handleSlideChange = (index) => {
    const item = this.items[index]

    if (item) {
      this._animateToRegion(item.location.lat, item.location.lon)
      this.setState({
        activeIndex: index,
      })
    }
  }

  render() {
    const { isOrdering, region } = this.state
    const { navigation, activeFilter } = this.props

    return (
      <View style={{
        flex: 1,
      }}>
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            width: '100%',
            height: '100%',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}
        >
          <MapView
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
            }}
            ref={ref => { this.map = ref }}
            initialRegion={defaultRegion}
            showsUserLocation={true}
            userLocationAnnotationTitle={''}
            showsMyLocationButton={false}
            followsUserLocation={false}
          >
            {!isOrdering &&
              this.items.map((item, index) => (
                <Marker
                  coordinate={{
                    latitude: item.location.lat,
                    longitude: item.location.lon
                  }}
                  title={''}
                  key={item.id}
                  onPress={e => {this._handleMarkerPress(e, index)}}
                  pinColor={index === this.state.activeIndex ? Layout.colors.black : Layout.colors.grey}
                  style={index === this.state.activeIndex ? { zIndex: 1 } : null}
                >
                  <Callout tooltip={true} />
                </Marker>
              ))
            }
          </MapView>
          {!isOrdering &&
            <MapCarousel
              items={this.items}
              navigation={navigation}
              collection={activeFilter.collection}
              onSlideChange={this._handleSlideChange}
              ref={'mapCarousel'}
            />
          }
        </View>
      </View>
    )
  }
}
