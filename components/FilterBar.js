import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import {
  View,
  Text,
  ScrollView,
  FlatList
} from 'react-native'
import Layout from '../constants/Layout'
import FilterButton from '../components/FilterButton'

class FilterBar extends Component {
  _handleFilterPress = (id) => {
    this.props.updateActiveFilter(id)
  }

  render() {
    const { filters, activeId, uid } = this.props

    return (
      <View style={{
        backgroundColor: Layout.colors.lightGrey,
        borderBottomColor: Layout.colors.mediumGrey,
        borderBottomWidth: 1
      }}>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            minWidth: '100%',
          }}
        >
          <FlatList
            data={filters}
            keyExtractor={(item) => item.filterId}
            contentContainerStyle={{
              padding: Layout.marginBasic,
              justifyContent: 'center',
              minWidth: '100%',
            }}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            renderItem={({item, index}) => {
              if (item.requiresUid && !uid) {
                return null
              }
              const isActive = item.filterId === activeId ? true : false
              return (<FilterButton filter={item} handlePress={this._handleFilterPress} isActive={isActive} />)
            }}
          />
        </ScrollView>
      </View>
    )
  }
}

export default compose(
  connect((state) => ({ uid: state.firebase.auth.uid })),
)(FilterBar)
