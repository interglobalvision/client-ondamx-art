import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import {
  View,
  FlatList,
  Text,
} from 'react-native'
import Layout from '../constants/Layout'
import { Translate } from 'react-localize-redux'

class BookmarkList extends Component {

  _handleItemPress = (id) => {
    const { navigation, navTitle } = this.props
    navigation.push(navTitle, { id })
  }

  render() {
    const { itemList, ItemComponent, emptyTranslateId } = this.props

    if (!isLoaded(itemList)) {
      return null
    }

    else if (isEmpty(itemList)) {
      return (
        <View style={{
          paddingLeft: Layout.marginBasic,
          paddingRight: Layout.marginBasic,
          justifyContent: 'center',
          alignItems: 'center',
          flex: 1,
          height: '100%'
        }}>
          <Text style={{
            color: Layout.colors.grey,
            fontSize: Layout.fontSizes.mid,
            textAlign: 'center',
          }}><Translate id={emptyTranslateId} /></Text>
        </View>
      )
    }

    return (
      <View>
        <View style={{
          paddingLeft: Layout.marginBasic,
          paddingRight: Layout.marginBasic,
        }}>
          <FlatList
            data={itemList}
            keyExtractor={(item) => item.id}
            renderItem={({item, index}) => {
              return (
                <ItemComponent item={item} handlePress={this._handleItemPress} />
              )
            }}
          />
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    itemList: state.firestore.ordered['bookmarks-' + props.collection],
  }
}

export default compose(
  connect((state) => ({ uid: state.firebase.auth.uid })),
  firestoreConnect((props) => {
    const { uid } = props
    return [{
      collection: props.collection,
      where: [
        ['bookmarks','array-contains',uid],
        ['status', '==', 'published'],
      ],
      storeAs: 'bookmarks-' + props.collection,
    }]
  }),
  connect(mapStateToProps)
)(BookmarkList)
