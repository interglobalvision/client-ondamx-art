import React, { Component } from 'react'
import {
  View,
  Text,
} from 'react-native'
import Layout from '../constants/Layout'
import { Translate } from 'react-localize-redux'
import { hasNotch } from '../lib/utils'

export default class TabBarLabel extends Component {
  render() {
    return (
      <View style={{
        paddingTop: hasNotch() ? Layout.marginBasic * 3 : Layout.marginBasic,
        paddingBottom: Layout.marginBasic,
      }}>
        <Text style={{
          color: Layout.colors.ondaBlue,
        }}><Translate id={this.props.translateId} /></Text>
      </View>
    )
  }
}
