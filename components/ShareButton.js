import React from 'react';
import { View, Image, Platform, TouchableOpacity, Share } from 'react-native';
import { constructUrl } from '../lib/utils'

export default class ShareButton extends React.Component {
  handleShare = async () => {
    const { title, slug, type, itemId } = this.props
    const url = constructUrl(title, slug, type, itemId)

    const shareData = Platform.OS === 'ios' ? {
      title,
      url
    } : {
      title,
      message: url
    }

    try {
      const result = await Share.share(shareData)

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  }

  render() {
    const source = Platform.OS !== 'android' ? require('../assets/images/share-ios.png') : require('../assets/images/share-android.png')

    return (
      <View>
        <TouchableOpacity onPress={this.handleShare}>
          <Image source={source} style={{
            height: 22,
            width: Platform.OS !== 'android' ? 16 : 24
          }} />
        </TouchableOpacity>
      </View>
    )
  }
}
