import React, { Component } from 'react'
import { connect, dispatch } from 'react-redux'
import {
  View,
  Text,
  ScrollView,
  FlatList
} from 'react-native'
import Layout from '../constants/Layout'
import { Translate } from 'react-localize-redux'
import SectionHeaderSmall from '../components/SectionHeaderSmall'
import FilterButton from '../components/FilterButton'
import { calendarFilters } from '../lib/filters'

export default class EventsFilterBar extends Component {
  _handleFilterPress = (id) => {
    const { navigation, updateActiveFilter } = this.props

    if (navigation.state.routeName === 'Explore') {
      navigation.push('Filter', { filterId: id })
    } else {
      updateActiveFilter(id)
    }
  }

  render() {
    const { translateId, navigation, activeId } = this.props

    return (
      <View>
        {translateId &&
          <View style={{
            paddingLeft: Layout.marginBasic,
            paddingRight: Layout.marginBasic,
          }}>
            <SectionHeaderSmall translateId={translateId} />
          </View>
        }
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            paddingLeft: Layout.marginBasic,
            paddingBottom: activeId ? 0 : Layout.marginBasic * 2,
            paddingTop: !activeId ? 0 : Layout.marginBasic,
          }}
        >
          <FlatList
            data={calendarFilters}
            keyExtractor={(item) => item.filterId}
            contentContainerStyle={{
              alignSelf: 'flex-start',
            }}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            renderItem={({item, index}) => {
              const isActive = item.filterId === activeId ? true : false
              return (<FilterButton filter={item} handlePress={this._handleFilterPress} isActive={isActive} />)
            }}
          />
        </ScrollView>
      </View>
    )
  }
}
