import React from 'react'
import { Image } from 'react-native'
import Layout from '../constants/Layout'

export default class TabBarIcon extends React.Component {
  render() {
    const { imageSource, width, height } = this.props
    return (
      <Image
        source={imageSource}
        width={width}
        height={height}
      />
    )
  }
}
