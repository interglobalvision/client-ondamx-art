const ondaBlue = 'rgb(15,25,123)',
  brown = 'rgb(132,64,15)',
  lightBlue = 'rgb(212,225,244)',
  yellow = 'rgb(252,220,0)',
  grey = 'rgb(155,155,155)',
  mediumGrey = 'rgb(216,216,216)',
  lightGrey = 'rgb(248,248,248)',
  black = 'rgb(25,25,25)',
  white = 'rgb(255,255,255)'


export default {
  ondaBlue,
  lightBlue,
  brown,
  yellow,
  grey,
  mediumGrey,
  lightGrey,
  black,
  white,
};
