import Colors from './Colors'

/* font families
*
*  'heading'
*  'medium'
*  'regular'
*  'italic'
*/

const fontSizes = {
  large: 32,
  big: 22,
  mid: 16,
  regular: 14,
  small: 12,
  smaller: 10,
  smallest: 9,
}

const fontStyles = {
  title: {
    fontFamily: 'medium'
  },
  type: {
    textTransform: 'uppercase',
    color: Colors.grey,
    fontSize: fontSizes.small,
  },
  date: {
    color: Colors.grey,
  }
}

const eventStyles = {
  dateEnding: {
    color: Colors.black,
  },
  featured: {
    name: {
      ...fontStyles.title,
      fontSize: fontSizes.big,
    },
    space: {
      fontSize: fontSizes.mid,
    },
    type: {
      ...fontStyles.type,
    },
    date: {
      ...fontStyles.date,
      fontSize: fontSizes.small,
    },
  },
  card: {
    name: {
      ...fontStyles.title,
      fontSize: fontSizes.mid,
    },
    space: {
      fontSize: fontSizes.small,
    },
    type: {
      ...fontStyles.type,
      fontSize: fontSizes.smaller,
    },
    date: {
      ...fontStyles.date,
      fontSize: fontSizes.small,
    },
  },
  carousel: {
    name: {
      ...fontStyles.title,
      fontSize: fontSizes.regular,
    },
    space: {
      fontSize: fontSizes.small,
    },
    type: {
      ...fontStyles.type,
      fontSize: fontSizes.smaller,
    },
    date: {
      ...fontStyles.date,
      fontSize: fontSizes.smaller,
    },
  },
  single: {
    name: {
      ...fontStyles.title,
      fontSize: fontSizes.large,
    },
    title: {
      fontSize: fontSizes.big,
    },
    type: {
      ...fontStyles.type,
    },
    date: {
      ...fontStyles.date,
      fontSize: fontSizes.mid,
    },
    space: {
      fontSize: fontSizes.mid,
      fontFamily: 'medium',
    },
    spaceLinked: {
      fontSize: fontSizes.mid,
      fontFamily: 'medium',
      color: Colors.brown
    },
    buyTickets: {
      fontFamily: 'medium',
      color: Colors.brown
    }
  },
}

const spaceStyles = {
  card: {
    name: {
      ...fontStyles.title,
    },
    type: {
      ...fontStyles.type,
      fontSize: fontSizes.smaller,
    },
    neighborhood: {
      fontSize: fontSizes.smaller,
      color: Colors.grey,
    },
  },
  single: {
    name: {
      ...fontStyles.title,
      fontSize: fontSizes.large,
    },
    type: {
      ...fontStyles.type,
    },
  },
}

const articleStyles = {
  featured: {
    title: {
      ...fontStyles.title,
      fontSize: fontSizes.mid,
    },
    type: {
      ...fontStyles.type,
      color: Colors.black,
    },
    author: {
      fontSize: fontSizes.small,
    },
  },
  card: {
    title: {
      ...fontStyles.title,
      fontSize: fontSizes.regular,
    },
    type: {
      ...fontStyles.type,
      color: Colors.black,
    },
    author: {
      fontSize: fontSizes.small,
    },
  },
  single: {
    title: {
      ...fontStyles.title,
      fontSize: fontSizes.large,
    },
    subtitle: {
      ...fontStyles.title,
      fontSize: fontSizes.big,
    },
    type: {
      ...fontStyles.type,
      color: Colors.black,
    },
    author: {
      fontSize: fontSizes.small,
    },
    date: {
      fontSize: fontSizes.small,
    },
  },
}

export default {
  fontSizes,
  fontStyles: {
    ...fontStyles,
    defaultTextStyle: {
      fontFamily: 'regular',
      fontSize: fontSizes.regular,
      color: Colors.black
    },
    optionButton: {
      fontFamily: 'medium',
    },
    buttonText: {
      fontFamily: 'medium',
      color: Colors.brown,
    },
    linkText: {
      color: Colors.brown,
    },
    finePrint: {
      color: Colors.mediumGrey,
      fontSizes: fontSizes.smallest,
    },
    submitButtonText: {
      color: Colors.white,
      textTransform: 'uppercase',
      fontSize: fontSizes.small
    },
    sectionHeader: {
      textTransform: 'uppercase',
      fontFamily: 'heading',
      fontSize: fontSizes.big
    },
    sectionHeaderSmall: {
      textTransform: 'uppercase',
      fontFamily: 'medium',
      fontSize: fontSizes.small,
    },
    caption: {
      textAlign: 'center',
      fontSize: fontSizes.smallest,
    },
    event: eventStyles,
    space: spaceStyles,
    article: articleStyles,
  },
}
