import { Dimensions } from 'react-native';
import Colors from './Colors'
import Typography from './Typography'

const windowDimensions = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height
}

const container = {
  flex: 1,
  backgroundColor: Colors.lightGrey
}

const shadow = {
  shadowColor: Colors.black,
  shadowOffset: { width: 0, height: 5 },
  shadowOpacity: 0.1,
  shadowRadius: 3
}

const borderRadius = 5
const borderWidth = 1
const marginBasic = 10

const outerCard = {
  ...shadow,
  borderRadius: borderRadius
}

const carouselOuterCard = {
  ...outerCard,
  marginLeft: marginBasic,
  marginRight: marginBasic,
}

const innerCard = {
  borderRadius: borderRadius,
  overflow: 'hidden',
  backgroundColor: Colors.white,
  width: '100%',
  height: '100%',
}

const slider = {
  wrapper: {
    paddingBottom: marginBasic * 2,
  },
  contentContainer: {
    padding: marginBasic,
  },
}

const cards = {
  featured: {
    width: windowDimensions.width - (marginBasic * 4),
    height: 420,
    imageHeight: 220,
  },
  carouselSmall: {
    width: 220,
    height: 150,
    imageHeight: 80,
  },
  carouselMid: {
    width: 260,
    height: 170,
    imageHeight: 103,
  },
  eventHalf: {
    height: 150,
  },
  eventFull: {
    height: 240,
    imageHeight: 127,
  },
  articleFeatured: {

  },
  articleItem: {

  },
  map: {
    width: 220,
    height: 140,
    imageHeight: 80,
  },
  relatedExternal: {
    width: 180,
    imageHeight: 96
  }
}

const responsiveImage = {
  width: '100%',
  height: '100%',
}

const imageBackgroundColor = Colors.mediumGrey

const imageHolder = {
  width: '100%',
  backgroundColor: imageBackgroundColor,
}

const imageHolderSquare = {
  backgroundColor: imageBackgroundColor,
  margin: marginBasic,
  borderRadius: borderRadius,
  overflow: 'hidden',
}

const events = {
  featured: {
    slider: {
      wrapper: {
        paddingTop: marginBasic,
        paddingBottom: marginBasic * 3,
      },
      contentContainer: {
        padding: marginBasic,
        paddingRight: marginBasic * 2,
      },
      snapToInterval: cards.featured.width + marginBasic,
    },
    outerCard: {
      ...outerCard,
      width: cards.featured.width,
      height: cards.featured.height,
      marginLeft: marginBasic,
    },
    wrapper: {
      flex: 1,
      flexDirection: 'column',
    },
    imageHolder: {
      ...imageHolder,
      height: cards.featured.imageHeight,
    },
    coverImageHolder: {
      ...imageHolder,
      height: cards.featured.height,
    },
  },

  carouselSmall: {
    slider: {
      ...slider,
      snapToInterval: cards.carouselSmall.width + (marginBasic * 2),
    },
    outerCard: {
      ...carouselOuterCard,
      width: cards.carouselSmall.width,
      height: cards.carouselSmall.height,
    },
    imageHolder: {
      ...imageHolder,
      height: cards.carouselSmall.imageHeight,
    },
  },

  carouselMid: {
    slider: {
      ...slider,
      snapToInterval: cards.carouselMid.width + (marginBasic * 2),
    },
    outerCard: {
      ...carouselOuterCard,
      width: cards.carouselMid.width,
      height: cards.carouselMid.height,
    },
    imageHolder: {
      ...imageHolder,
      height: cards.carouselMid.imageHeight,
    },
  },

  half: {
    outerCard: {
      ...outerCard,
      height: cards.eventHalf.height,
      padding: marginBasic,
    },
  },

  full: {
    outerCard: {
      ...outerCard,
      height: cards.eventFull.height,
      padding: marginBasic,
    },
    imageHolder: {
      ...imageHolder,
      height: cards.eventFull.imageHeight,
    }
  }
}

const articles = {
  featured: {
    imageHolder: {
      ...imageHolderSquare,
      width: 156,
      height: 120,
    },
  },
  item: {
    imageHolder: {
      ...imageHolderSquare,
      width: 108,
      height: 108,
    },
  },
}

const map = {
  slider: {
    wrapper: {
      position: 'absolute',
      bottom: marginBasic * 2,
      left: 0,
      right: 0,
      width: '100%',
    },
    contentContainer: {
      paddingLeft: (windowDimensions.width - cards.map.width) / 2,
      paddingRight: ((windowDimensions.width - cards.map.width) / 2) - (marginBasic * 2)
    },
    snapToInterval: cards.map.width + (marginBasic * 2),
  },
  outerCard: {
    ...outerCard,
    width: cards.map.width,
    height: cards.map.height,
    marginRight: marginBasic * 2
  },
  imageHolder: {
    ...imageHolder,
    height: cards.map.imageHeight,
  },
}

const relatedExternal = {
  slider: {
    ...slider,
    snapToInterval: cards.relatedExternal.width + (marginBasic * 2),
  },
  item: {
    width: cards.relatedExternal.width,
    marginHorizontal: marginBasic
  },
  imageHolder: {
    ...imageHolder,
    height: cards.relatedExternal.imageHeight,
    overflow: 'hidden',
    borderRadius: borderRadius,
  }
}

const textInput = {
  padding: marginBasic,
  borderColor: Colors.mediumGrey,
  borderWidth: borderWidth,
  borderRadius: borderRadius,
  marginBottom: marginBasic,
  backgroundColor: Colors.white,
}

const socialLoginButton = {
  padding: marginBasic,
  borderColor: Colors.white,
  borderWidth:  borderWidth,
  borderRadius: borderRadius,
  width: '100%',
  flexDirection: 'row',
}

const submitButton = {
  padding: marginBasic,
  backgroundColor: Colors.ondaBlue,
  borderRadius: borderRadius * 3,
  alignItems: 'center',
  width: 200
}

const search = {
  outerCard: {
    ...outerCard,
    paddingVertical: marginBasic,
    marginHorizontal: marginBasic * 2,
    height: 60,
  },
  inputHolder: {
    paddingHorizontal: marginBasic,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: '100%',
    width: '100%',
  },
  textInput: {
    marginLeft: marginBasic,
    paddingVertical: marginBasic * 1.2,
    flex: 1,
  },
  resultsContainer: {
    ...container,
    borderTopColor: Colors.mediumGrey,
    borderTopWidth: borderWidth,
    marginTop: marginBasic
  },
  imageHolder: {
    ...imageHolder,
    borderRadius: 34,
    overflow: 'hidden',
    height: 34,
    width: 34,
    marginRight: marginBasic * 2,
  },
}

const notifications = {
}

const draftContent = {
  unstyled: {
    marginBottom: Typography.fontSizes.regular,
    color: Colors.black,
    fontSize: Typography.fontSizes.regular,
    paddingHorizontal: marginBasic * 2,
  },
  bold: {
    fontFamily: 'medium',
  },
  italic: {
    fontFamily: 'italic',
  },
  'header-two': {
    color: Colors.black,
    fontSize: Typography.fontSizes.big,
    fontFamily: 'medium',
    paddingHorizontal: marginBasic * 2,
  },
  link: {
    color: Colors.brown,
  },
  blockquote: {
    marginLeft: 0,
  },
  blockquoteContainer: {
    marginTop: 0,
    marginBottom: Typography.fontSizes.regular,
    marginHorizontal: marginBasic * 2,
    paddingLeft: marginBasic * 2,
    borderLeftWidth: 1,
    borderLeftColor: Colors.black,
  },
  orderedListItemContainer: {
    paddingRight: marginBasic * 4,
    marginBottom: Typography.fontSizes.regular,
  },
  orderedListItemNumber: {
    alignSelf: 'flex-start',
    fontSize: Typography.fontSizes.regular,
    marginRight: marginBasic,
  },
  unorderedListItemContainer: {
    paddingHorizontal: marginBasic * 2,
    marginBottom: Typography.fontSizes.regular,
  },
  unorderedListItemBullet: {
    alignSelf: 'flex-start',
    marginTop: marginBasic / 2,
    marginRight: marginBasic
  }
}

const captionHolder = {
  paddingHorizontal: marginBasic * 3,
  alignItems: 'center',
  minHeight: marginBasic * 4,
}

const dividerTop = {
  borderTopWidth: 1,
  borderTopColor: Colors.mediumGrey,
  marginTop: marginBasic,
  paddingTop: marginBasic,
}

const tabBar = {
  borderTopWidth: 5,
  borderTopColor: Colors.ondaBlue,
}

export default {
  window: windowDimensions,
  isSmallDevice: windowDimensions.width < 375,
  fontSizes: Typography.fontSizes,
  fontStyles: Typography.fontStyles,
  colors: Colors,
  cards,
  responsiveImage,
  imageBackgroundColor,
  events,
  articles,
  map,
  container,
  borderRadius,
  shadow,
  innerCard,
  outerCard,
  marginBasic,
  textInput,
  submitButton,
  search,
  socialLoginButton,
  relatedExternal,
  draftContent,
  captionHolder,
  dividerTop,
  tabBar,
  notifications,
}
